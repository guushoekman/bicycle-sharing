---
city: "Valencia"
country: "Spain"
view: "39.4741, -0.3759"
zoom: "13"
bbox: "-0.423775,39.437004,-0.319234,39.502864"
timezone: "Europe/Madrid"
---