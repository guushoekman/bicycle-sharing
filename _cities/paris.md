---
city: "Paris"
country: "France"
view: "48.8555, 2.3459"
zoom: "12"
bbox: "2.162,48.761,2.506,48.929"
timezone: "Europe/Paris"
---