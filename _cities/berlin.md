---
city: "Berlin"
country: "Germany"
view: "52.5162, 13.3875"
zoom: "13"
bbox: "-0.423775,39.437004,-0.319234,39.502864"
timezone: "Europe/Berlin"
---