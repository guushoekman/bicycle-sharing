# Bicycle Sharing

### Get list of stations

```
curl 'http://api.citybik.es/v2/networks/network_name' | jq '.network.stations[]' > temp.json
jq '{type: "Feature", properties: {uid: .extra.uid, address: .name, bikes: [], slots: []}, geometry: {type: "Point", coordinates: [.longitude, .latitude]}}' temp.json > stations.geojson
rm temp.json
```

### Create a bounding box

[Reference](https://tools.ietf.org/html/rfc7946#section-5)

> The value of the bbox member MUST be an array ... with all axes of the most southwesterly point followed by all axes of the more northeasterly point

`[x1, y1, x2, y2]`

`x1` = lowest longitude
`y1` = lowest latitude
`x2` = highest longitude
`y2` = highest latitude

[Create rectange through UI](http://boundingbox.klokantech.com)

#### From stations coordinates

`curl http://api.citybik.es/v2/networks/network_name > temp.json`

`x1`: `jq '.network.stations[].longitude' temp.json | sort | head -1`
`y1`: `jq '.network.stations[].latitude' temp.json | sort | head -1`
`x2`: `jq '.network.stations[].longitude' temp.json | sort | tail -1`
`y2`: `jq '.network.stations[].latitude' temp.json | sort | tail -1`

`rm temp.json`

jq '.network.stations[].longitude' temp.json | sort | head -1 && jq '.network.stations[].latitude' temp.json | sort | head -1 && jq '.network.stations[].longitude' temp.json | sort | tail -1 && jq '.network.stations[].latitude' temp.json | sort | tail -1