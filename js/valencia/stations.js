var stations = [
  {
    "address": "Fontanars dels Aforins - Vall d'Uix\\u00f3",
    "uid": 198,
    "latitude": 39.460848056694466,
    "longitude": -0.3954071515343
  },
  {
    "address": "Plaza del Musico L\\u00f3pez Chavarri",
    "uid": 3,
    "latitude": 39.47680311538018,
    "longitude": -0.38037911504075
  },
  {
    "address": "San Juan Bosco - Santiago Rusi\\u00f1ol",
    "uid": 233,
    "latitude": 39.497128187401,
    "longitude": -0.369528093405798
  },
  {
    "address": "Fray Jun\\u00edpero Serra - Vall d'Uix\\u00f3",
    "uid": 211,
    "latitude": 39.45869404863428,
    "longitude": -0.397848157697943
  },
  {
    "address": "Justo y Pastor - Duque de Gaeta",
    "uid": 98,
    "latitude": 39.46964810153237,
    "longitude": -0.348533015740284
  },
  {
    "address": "Campoamor - M\\u00fasico Gin\\u00e9s",
    "uid": 76,
    "latitude": 39.4700511045018,
    "longitude": -0.343552001034738
  },
  {
    "address": "Doctor Vicente Zaragoz\\u00e1 - Ram\\u00f3n Asensio",
    "uid": 116,
    "latitude": 39.483221144341215,
    "longitude": -0.357652050365347
  },
  {
    "address": "Juan Llorens - Literato Gabriel Mir\\u00f3",
    "uid": 21,
    "latitude": 39.4723390971218,
    "longitude": -0.390305142409828
  },
  {
    "address": "Serrer\\u00eda, 67",
    "uid": 84,
    "latitude": 39.46726209778759,
    "longitude": -0.335255974679776
  },
  {
    "address": "Uruguay - Carteros",
    "uid": 193,
    "latitude": 39.4551140401216,
    "longitude": -0.38680912265075
  },
  {
    "address": "Plaza Luis Cano, 5",
    "uid": 268,
    "latitude": 39.50141318615774,
    "longitude": -0.418584242795808
  },
  {
    "address": "Tres Cruces - M\\u00fasico Ayll\\u00f3n",
    "uid": 247,
    "latitude": 39.46737807542721,
    "longitude": -0.405664185821979
  },
  {
    "address": "Salamanca - Conde Altea",
    "uid": 31,
    "latitude": 39.46733308836422,
    "longitude": -0.365119064198398
  },
  {
    "address": "Blasco Iba\\u00f1ez - Pintor Jos\\u00e9 Mongrell",
    "uid": 99,
    "latitude": 39.471312109714646,
    "longitude": -0.34057499279659
  },
  {
    "address": "Tomas de Villarroya - San Vicente",
    "uid": 255,
    "latitude": 39.44767101522146,
    "longitude": -0.386120116565009
  },
  {
    "address": "Tres Cruces - Hospital General",
    "uid": 246,
    "latitude": 39.46905808079698,
    "longitude": -0.406600189533111
  },
  {
    "address": "General Urrutia - Granada",
    "uid": 41,
    "latitude": 39.458880059935595,
    "longitude": -0.364867058884755
  },
  {
    "address": "San Jos\\u00e9 de Calasanz - Salas Quiroga",
    "uid": 26,
    "latitude": 39.46616307762487,
    "longitude": -0.386143126606345
  },
  {
    "address": "Calle Salv\\u00e1 - Calle Poeta Querol",
    "uid": 12,
    "latitude": 39.471935,
    "longitude": -0.374101
  },
  {
    "address": "Francisco Cubells - San Jos\\u00e9 de la Vega",
    "uid": 159,
    "latitude": 39.46341708497314,
    "longitude": -0.334812971276553
  },
  {
    "address": "Maximiliano Thous - Luz Casanova",
    "uid": 134,
    "latitude": 39.48762415410862,
    "longitude": -0.373430100012801
  },
  {
    "address": "Av. de la Plata - Zapadores",
    "uid": 179,
    "latitude": 39.455440047796785,
    "longitude": -0.366517061973888
  },
  {
    "address": "Conde Torrefiel - Cecilio Pl\\u00e1",
    "uid": 235,
    "latitude": 39.49801318789519,
    "longitude": -0.377294117154526
  },
  {
    "address": "P\\u00edo XII - Campanar",
    "uid": 172,
    "latitude": 39.481430126787565,
    "longitude": -0.39343615667867
  },
  {
    "address": "San Francisco de Paula - Castell de Pop",
    "uid": 274,
    "latitude": 39.448038033633594,
    "longitude": -0.333278958357397
  },
  {
    "address": "Pintor Stolz - Nueve de Octubre",
    "uid": 217,
    "latitude": 39.47029608559258,
    "longitude": -0.404687184457659
  },
  {
    "address": "Micer Masc\\u00f3 - Rodriguez Fornos",
    "uid": 81,
    "latitude": 39.47509611584928,
    "longitude": -0.361069056238335
  },
  {
    "address": "Navarro Reverter - Grabador Esteve",
    "uid": 28,
    "latitude": 39.47155010170615,
    "longitude": -0.367842074631205
  },
  {
    "address": "Alboraya - Flora",
    "uid": 131,
    "latitude": 39.48062113119549,
    "longitude": -0.371254089734112
  },
  {
    "address": "C/GUILLEM DE CASTRO esquina con C/NA JORDANA",
    "uid": 1,
    "latitude": 39.48001,
    "longitude": -0.38302
  },
  {
    "address": "Antonio Ferrandis - General Urrutia",
    "uid": 48,
    "latitude": 39.450775142005085,
    "longitude": -0.358643429344464
  },
  {
    "address": "Manuel Broseta i Pont - Naranjos",
    "uid": 112,
    "latitude": 39.47854813352072,
    "longitude": -0.342366002058633
  },
  {
    "address": "M\\u00fasico Hip\\u00f3lito Mart\\u00ednez - Di\\u00f3genes L\\u00f3pez Mecho",
    "uid": 122,
    "latitude": 39.48445314961921,
    "longitude": -0.354157040555065
  },
  {
    "address": "Albacete - Maluquer",
    "uid": 243,
    "latitude": 39.46278806672407,
    "longitude": -0.384633120260506
  },
  {
    "address": "Guillem de Castro (Torres de Quart)",
    "uid": 82,
    "latitude": 39.47602411158443,
    "longitude": -0.384005125495213
  },
  {
    "address": "Pav\\u00eda - Espad\\u00e1n",
    "uid": 165,
    "latitude": 39.47137711508329,
    "longitude": -0.324660945171817
  },
  {
    "address": "Albalat dels Tarongers - Professor Ernest Lluch",
    "uid": 106,
    "latitude": 39.47671212762051,
    "longitude": -0.341477998410525
  },
  {
    "address": "Alcasser - Poeta Alberto Lista",
    "uid": 265,
    "latitude": 39.470941086635776,
    "longitude": -0.408208195369257
  },
  {
    "address": "San Jose Artesano - Francisco Morote Greus",
    "uid": 238,
    "latitude": 39.497417180383685,
    "longitude": -0.394474168343446
  },
  {
    "address": "Av Puerto 19 (Telef\\u00f3nica)",
    "uid": 63,
    "latitude": 39.467747091898616,
    "longitude": -0.358505044597536
  },
  {
    "address": "Autopista del Saler - Puente Monteolivete",
    "uid": 47,
    "latitude": 39.45648005487459,
    "longitude": -0.355509029538065
  },
  {
    "address": "Francia - Menorca",
    "uid": 56,
    "latitude": 39.45838906488428,
    "longitude": -0.344495997567289
  },
  {
    "address": "San Clemente - Hospital Arnau de Vilanova",
    "uid": 227,
    "latitude": 39.490216153353934,
    "longitude": -0.403022190134151
  },
  {
    "address": "San Vicente Paul - Santiago Rusi\\u00f1ol",
    "uid": 232,
    "latitude": 39.49461218010229,
    "longitude": -0.365844081019678
  },
  {
    "address": "Pav\\u00eda (Instituto Isabel de Villena)",
    "uid": 169,
    "latitude": 39.478753139905066,
    "longitude": -0.324735949373236
  },
  {
    "address": "Ricardo Mu\\u00f1oz Suay - Mar\\u00eda Jos\\u00e9 Victoria Fuster (C.C. El Saler)",
    "uid": 49,
    "latitude": 39.452954722176685,
    "longitude": -0.35720111550767
  },
  {
    "address": "Murta - Sant Eperit",
    "uid": 121,
    "latitude": 39.48615015382428,
    "longitude": -0.358862055563278
  },
  {
    "address": "Pio XII - Men\\u00e9ndez Puidal (Nuevo Centro)",
    "uid": 143,
    "latitude": 39.47950512106764,
    "longitude": -0.391027148421504
  },
  {
    "address": "Catalu\\u00f1a - Doctor Vicente Zaragoz\\u00e1",
    "uid": 115,
    "latitude": 39.481530140385686,
    "longitude": -0.352239033238509
  },
  {
    "address": "M\\u00fasico Ayll\\u00f3n - Francisco Dolz",
    "uid": 215,
    "latitude": 39.46563907240366,
    "longitude": -0.39683915840866
  },
  {
    "address": "Corts Valencianes - General Avil\\u00e9s",
    "uid": 204,
    "latitude": 39.48581114052686,
    "longitude": -0.396678168748875
  },
  {
    "address": "Plaza Xuquer - Vinalop\\u00f3",
    "uid": 261,
    "latitude": 39.47664412448885,
    "longitude": -0.350468025305313
  },
  {
    "address": "Ram\\u00f3n Llull - Serpis",
    "uid": 102,
    "latitude": 39.47580112282824,
    "longitude": -0.346811013895548
  },
  {
    "address": "Conde Altea - Almirante Cadarso",
    "uid": 32,
    "latitude": 39.4659740827401,
    "longitude": -0.368338073115823
  },
  {
    "address": "Arag\\u00f3n - Ernesto Ferrer",
    "uid": 79,
    "latitude": 39.47274710913562,
    "longitude": -0.357333043778256
  },
  {
    "address": "Plaza de la Reina - Mar",
    "uid": 8,
    "latitude": 39.47428810857441,
    "longitude": -0.375175098086736
  },
  {
    "address": "Cra. Malilla - Bulevar Sur",
    "uid": 190,
    "latitude": 39.446050011610645,
    "longitude": -0.380387098491455
  },
  {
    "address": "Luis Garc\\u00eda Berlanga Mart\\u00ed - Menorca",
    "uid": 52,
    "latitude": 39.45600205620843,
    "longitude": -0.346431002074561
  },
  {
    "address": "Plaza M\\u00fasico Esp\\u00ed",
    "uid": 234,
    "latitude": 39.49646718382403,
    "longitude": -0.373735105660714
  },
  {
    "address": "Salamanca - Reina Do\\u00f1a Germana",
    "uid": 155,
    "latitude": 39.46492108086036,
    "longitude": -0.363169057053421
  },
  {
    "address": "Reina Do\\u00f1a Mar\\u00eda - C\\u00e1diz",
    "uid": 152,
    "latitude": 39.460893064158775,
    "longitude": -0.372796083742631
  },
  {
    "address": "Levante U.D. - Ecuador",
    "uid": 237,
    "latitude": 39.4947261723789,
    "longitude": -0.391129156875508
  },
  {
    "address": "Lu\\u00eds Peix\\u00f3 20",
    "uid": 108,
    "latitude": 39.47365411982902,
    "longitude": -0.333702973476317
  },
  {
    "address": "Juan Verdeguer - Toneleros",
    "uid": 67,
    "latitude": 39.45882806883267,
    "longitude": -0.336888975014669
  },
  {
    "address": "Doctor Vicente Zaragoz\\u00e1 - Emilio Bar\\u00f3",
    "uid": 120,
    "latitude": 39.484848148381595,
    "longitude": -0.362137064678155
  },
  {
    "address": "Juan Llorens - Quart",
    "uid": 19,
    "latitude": 39.47433710342028,
    "longitude": -0.391681147609965
  },
  {
    "address": "Alberique, 18 (Abastos)",
    "uid": 25,
    "latitude": 39.46769708197544,
    "longitude": -0.388701135103807
  },
  {
    "address": "Reig Genov\\u00e9s - Ram\\u00f3n Contreras Mongrell",
    "uid": 178,
    "latitude": 39.492184170392264,
    "longitude": -0.37059709396399
  },
  {
    "address": "Benifair\\u00f3 de Valldigna - Joaqu\\u00edn Benlloch",
    "uid": 186,
    "latitude": 39.450359028192096,
    "longitude": -0.374106081983545
  },
  {
    "address": "Blasco Iba\\u00f1ez - Clariano",
    "uid": 94,
    "latitude": 39.47534611972441,
    "longitude": -0.351674028220146
  },
  {
    "address": "Peris y Valero - Cuba",
    "uid": 39,
    "latitude": 39.457822053401024,
    "longitude": -0.374022085762198
  },
  {
    "address": "Campanar - Nicasio Benlloch",
    "uid": 140,
    "latitude": 39.48797215005971,
    "longitude": -0.38969114895134
  },
  {
    "address": "Alfahuir - Pe\\u00f1iscola",
    "uid": 128,
    "latitude": 39.488734159713275,
    "longitude": -0.367635083238739
  },
  {
    "address": "Convento Carmelitas - Alboraya",
    "uid": 130,
    "latitude": 39.48320614021858,
    "longitude": -0.370301088264948
  },
  {
    "address": "Campillo de Altobuey (Poluideportivo)",
    "uid": 107,
    "latitude": 39.47521312423745,
    "longitude": -0.336314982139155
  },
  {
    "address": "Manuel de Falla - Hern\\u00e1ndez L\\u00e1zaro",
    "uid": 221,
    "latitude": 39.47523810283483,
    "longitude": -0.40294418188015
  },
  {
    "address": "Moraira - Alta del Mar",
    "uid": 273,
    "latitude": 39.450273041140704,
    "longitude": -0.33336295982062
  },
  {
    "address": "X\\u00e1tiva - Bail\\u00e9n (Estaci\\u00f3n del Norte)",
    "uid": 17,
    "latitude": 39.467436084759534,
    "longitude": -0.377350100922523
  },
  {
    "address": "Pie de la Cruz - Rejas",
    "uid": 147,
    "latitude": 39.473169103148095,
    "longitude": -0.38029811284559
  },
  {
    "address": "Col\\u00f3n 20-22",
    "uid": 18,
    "latitude": 39.46815908853249,
    "longitude": -0.373220088928677
  },
  {
    "address": "Tres Forques - Tur\\u00eds",
    "uid": 256,
    "latitude": 39.4639310680958,
    "longitude": -0.392326143951058
  },
  {
    "address": "Jaime Roig - Bachiller",
    "uid": 119,
    "latitude": 39.48248814004604,
    "longitude": -0.363317066947724
  },
  {
    "address": "Pav\\u00eda - Columbretes",
    "uid": 164,
    "latitude": 39.468428105127856,
    "longitude": -0.324724943771547
  },
  {
    "address": "San Pancracio - Periodista Llorente",
    "uid": 138,
    "latitude": 39.48815815241352,
    "longitude": -0.384313132924836
  },
  {
    "address": "Regne de Valencia - Doctor Sumsi",
    "uid": 34,
    "latitude": 39.46409007583113,
    "longitude": -0.370052077239064
  },
  {
    "address": "Av. del Cuid - Burgos",
    "uid": 216,
    "latitude": 39.46913,
    "longitude": -0.399552
  },
  {
    "address": "San Vicente Martir - Doctor Vil\\u00e1 Barber\\u00e1",
    "uid": 27,
    "latitude": 39.46333006942278,
    "longitude": -0.381942112481819
  },
  {
    "address": "Gregorio Gea - Padre Ferris",
    "uid": 141,
    "latitude": 39.48323613551247,
    "longitude": -0.385247133088568
  },
  {
    "address": "Blasco Iba\\u00f1ez - Yecla",
    "uid": 96,
    "latitude": 39.47352211466433,
    "longitude": -0.348305017144382
  },
  {
    "address": "Jer\\u00f3nimo Monsoriu - Alcalde Cano Coloma",
    "uid": 151,
    "latitude": 39.46463708827678,
    "longitude": -0.337207979114133
  },
  {
    "address": "Ramiro de Maeztu - Peris Brell",
    "uid": 72,
    "latitude": 39.46721209400214,
    "longitude": -0.346399008039668
  },
  {
    "address": "Pintor Rafael Solves - Jose Soto Mico",
    "uid": 258,
    "latitude": 39.43979598762512,
    "longitude": -0.38922812163005
  },
  {
    "address": "Plaza Espa\\u00f1a",
    "uid": 58,
    "latitude": 39.46618607914484,
    "longitude": -0.381677113224894
  },
  {
    "address": "Col\\u00f3n, 44",
    "uid": 70,
    "latitude": 39.469181,
    "longitude": -0.371737
  },
  {
    "address": "Padre Esteban Pernet - Casa Misericordia",
    "uid": 263,
    "latitude": 39.46701107287583,
    "longitude": -0.409735197841415
  },
  {
    "address": "Giorgeta, 64",
    "uid": 207,
    "latitude": 39.455361042514035,
    "longitude": -0.381996108347568
  },
  {
    "address": "G\\u00f3mez Ferrer - \\u00c1lvaro de Baz\\u00e1n",
    "uid": 118,
    "latitude": 39.4804341340267,
    "longitude": -0.360507057423573
  },
  {
    "address": "Florista - T4 (Palau de Congressos)",
    "uid": 239,
    "latitude": 39.496953177032466,
    "longitude": -0.400055184833165
  },
  {
    "address": "UPV Inform\\u00e1tica",
    "uid": 114,
    "latitude": 39.481772142992675,
    "longitude": -0.346682016720988
  },
  {
    "address": "Archuiduque Carlos - Jos\\u00e9 Mar\\u00eda Mortes Lerma",
    "uid": 213,
    "latitude": 39.463133063872554,
    "longitude": -0.397063157732616
  },
  {
    "address": "UPV Galileo",
    "uid": 111,
    "latitude": 39.480184139586505,
    "longitude": -0.340651997804929
  },
  {
    "address": "Doctor Tom\\u00e1s Sala - Carteros",
    "uid": 254,
    "latitude": 39.44768201399717,
    "longitude": -0.39000812823506
  },
  {
    "address": "Blasco Iba\\u00f1ez - Mestre Ripoll",
    "uid": 100,
    "latitude": 39.471602111446835,
    "longitude": -0.33824098596173
  },
  {
    "address": "Jer\\u00f3nimo Mu\\u00f1oz - Gaspar Aguilar",
    "uid": 196,
    "latitude": 39.4595430546337,
    "longitude": -0.388157129082296
  },
  {
    "address": "Av. del Cuid - Marconi",
    "uid": 264,
    "latitude": 39.46912807848099,
    "longitude": -0.414523213347863
  },
  {
    "address": "Economista Gay - Luis Crumiere",
    "uid": 137,
    "latitude": 39.489001156645486,
    "longitude": -0.379987120406342
  },
  {
    "address": "Malvarrosa - R\\u00edo Tajo",
    "uid": 168,
    "latitude": 39.47683913241012,
    "longitude": -0.327977958049006
  },
  {
    "address": "Tres Cruces - Jose Maria Mortes Lerma",
    "uid": 248,
    "latitude": 39.46280806019345,
    "longitude": -0.40506018155276
  },
  {
    "address": "Perez Gald\\u00f3s - Nou Moles",
    "uid": 201,
    "latitude": 39.471680093784954,
    "longitude": -0.393759152416117
  },
  {
    "address": "Monduber - Peset Aleixandre",
    "uid": 174,
    "latitude": 39.48940615584413,
    "longitude": -0.386738140863954
  },
  {
    "address": "Molinell - Calder\\u00f3n de la Barca",
    "uid": 77,
    "latitude": 39.484941147529945,
    "longitude": -0.365758075579642
  },
  {
    "address": "Reina Violante - Escultor Garc\\u00eda Mas",
    "uid": 203,
    "latitude": 39.48457713488814,
    "longitude": -0.401273181871356
  },
  {
    "address": "Plaza San Felipe Neri (Mercado Algir\\u00f3s)",
    "uid": 74,
    "latitude": 39.469917100728054,
    "longitude": -0.353825031741888
  },
  {
    "address": "Gaspar Aguilar - M\\u00fasico Penella",
    "uid": 209,
    "latitude": 39.45205002773928,
    "longitude": -0.393106139888379
  },
  {
    "address": "Pav\\u00eda - Acequia de la Cadena",
    "uid": 167,
    "latitude": 39.4748761268572,
    "longitude": -0.324700947178956
  },
  {
    "address": "Plaza del Mercado - Taula de Canvis",
    "uid": 7,
    "latitude": 39.474840109114226,
    "longitude": -0.379276110679168
  },
  {
    "address": "Albalat dels Tarongers - Paseo Facultades",
    "uid": 104,
    "latitude": 39.47835413125328,
    "longitude": -0.34736601693197
  },
  {
    "address": "UPV Trinquet",
    "uid": 110,
    "latitude": 39.48070814260447,
    "longitude": -0.336770986463246
  },
  {
    "address": "Blasco Iba\\u00f1ez 121",
    "uid": 97,
    "latitude": 39.47303611466866,
    "longitude": -0.343223001657882
  },
  {
    "address": "Alca\\u00f1iz - Cambrils",
    "uid": 231,
    "latitude": 39.495040177417366,
    "longitude": -0.378712119815713
  },
  {
    "address": "Alboc\\u00e1cer - Vinaroz",
    "uid": 123,
    "latitude": 39.487639156890914,
    "longitude": -0.364932074551232
  },
  {
    "address": "Pintor Luis Arcas - Inst. Obrero Valenciano",
    "uid": 46,
    "latitude": 39.45547705039899,
    "longitude": -0.358878039093959
  },
  {
    "address": "Av. Puerto - Jos\\u00e9 Aguilar",
    "uid": 68,
    "latitude": 39.46277108052299,
    "longitude": -0.341823991929563
  },
  {
    "address": "Juan XXIII - Domingo G\\u00f3mez",
    "uid": 175,
    "latitude": 39.49255716773316,
    "longitude": -0.382803130751858
  },
  {
    "address": "Don Vicente Guillot - Progreso",
    "uid": 166,
    "latitude": 39.47331712024639,
    "longitude": -0.328901958917283
  },
  {
    "address": "Alameda - Pintor Maella",
    "uid": 53,
    "latitude": 39.456732058086,
    "longitude": -0.348230007860048
  },
  {
    "address": "Salvador Cerver\\u00f3 - Carlos Cortina",
    "uid": 271,
    "latitude": 39.499247187993085,
    "longitude": -0.389982155850781
  },
  {
    "address": "Av. de la Plata (Museo Fallero)",
    "uid": 42,
    "latitude": 39.45874906147631,
    "longitude": -0.358749040474832
  },
  {
    "address": "General Urrutia - Av. de la Plata",
    "uid": 44,
    "latitude": 39.456397052131265,
    "longitude": -0.363105052261891
  },
  {
    "address": "Col\\u00f3n, 60",
    "uid": 16,
    "latitude": 39.47006009581504,
    "longitude": -0.370524081869223
  },
  {
    "address": "Campos Crespo - Juan de Garay",
    "uid": 210,
    "latitude": 39.45552103825903,
    "longitude": -0.396807152864784
  },
  {
    "address": "Hermanos Maristas - General Urrutia",
    "uid": 45,
    "latitude": 39.45378704370325,
    "longitude": -0.361946047377204
  },
  {
    "address": "Masquefa, 42 - 44",
    "uid": 125,
    "latitude": 39.489661165707126,
    "longitude": -0.358709056987903
  },
  {
    "address": "Pechina - Teruel",
    "uid": 202,
    "latitude": 39.4761901091111,
    "longitude": -0.393425153835504
  },
  {
    "address": "Rio Segre - Rafael Company",
    "uid": 236,
    "latitude": 39.495034176224834,
    "longitude": -0.382369130775341
  },
  {
    "address": "Constituci\\u00f3n - Reus",
    "uid": 135,
    "latitude": 39.48567014586083,
    "longitude": -0.378592114439778
  },
  {
    "address": "Arquitecto Segura del Lago - Camino Nuevo de Pica\\u00f1a",
    "uid": 251,
    "latitude": 39.45579803670432,
    "longitude": -0.404504176110588
  },
  {
    "address": "P\\u00e9rez Gald\\u00f3s - Marqu\\u00e9s de Zenete",
    "uid": 157,
    "latitude": 39.464020069969,
    "longitude": -0.387457129393968
  },
  {
    "address": "\\u00c1ngel Villena - Ausias March",
    "uid": 187,
    "latitude": 39.447928021676226,
    "longitude": -0.368909065084874
  },
  {
    "address": "Naranjos - Ingeniero Fausto Elio",
    "uid": 109,
    "latitude": 39.47678713041781,
    "longitude": -0.333600974859012
  },
  {
    "address": "Av. Puerto - Doctor Manuel Candela",
    "uid": 65,
    "latitude": 39.46548408671991,
    "longitude": -0.350928020671138
  },
  {
    "address": "Nicasio Benlloch - Amics dels Corpus",
    "uid": 205,
    "latitude": 39.489255153267536,
    "longitude": -0.393181160103781
  },
  {
    "address": "Alfambra - Poeta Monmeneu",
    "uid": 133,
    "latitude": 39.48272413676827,
    "longitude": -0.375973105008805
  },
  {
    "address": "Fontanars dels Aforins - Jacinto Labaila",
    "uid": 197,
    "latitude": 39.458152048558595,
    "longitude": -0.392424141132886
  },
  {
    "address": "Blasco Iba\\u00f1ez, 28 (F. Geograf\\u00eda e Historia)",
    "uid": 88,
    "latitude": 39.47748012383395,
    "longitude": -0.361233058011854
  },
  {
    "address": "Puerto Rico - Cuba",
    "uid": 156,
    "latitude": 39.460932063112466,
    "longitude": -0.376436094678165
  },
  {
    "address": "Padre Barranco - Carlos Ruano Llopis (Pintor)",
    "uid": 225,
    "latitude": 39.48807514670377,
    "longitude": -0.401233183623003
  },
  {
    "address": "Blasco Iba\\u00f1ez - Arag\\u00f3n",
    "uid": 92,
    "latitude": 39.475828119933816,
    "longitude": -0.356059041618514
  },
  {
    "address": "German\\u00edas - Ruzafa",
    "uid": 33,
    "latitude": 39.464786076876194,
    "longitude": -0.37408108969372
  },
  {
    "address": "Blasco Iba\\u00f1ez - Jaime Roig",
    "uid": 85,
    "latitude": 39.47943212926333,
    "longitude": -0.364806069768998
  },
  {
    "address": "Armada Espa\\u00f1ola - Mariano Cuber",
    "uid": 162,
    "latitude": 39.46357208709311,
    "longitude": -0.32988595660341
  },
  {
    "address": "Alfonso el Magn\\u00e1nimo - Nave",
    "uid": 13,
    "latitude": 39.472030102316914,
    "longitude": -0.370965084251257
  },
  {
    "address": "Platero Su\\u00e1rez - Milagrosa",
    "uid": 132,
    "latitude": 39.48525414608695,
    "longitude": -0.373525099027208
  },
  {
    "address": "Estaci\\u00f3n AVE Joaqu\\u00edn Sorolla",
    "uid": 194,
    "latitude": 39.46098606179716,
    "longitude": -0.381065108588874
  },
  {
    "address": "Blasco Iba\\u00f1ez - Doctor G\\u00f3mez Ferrer (Cl\\u00ednico)",
    "uid": 87,
    "latitude": 39.47848612698768,
    "longitude": -0.361971060764256
  },
  {
    "address": "Periodista Gil Sumbiela - Poeta Serrano Clavero",
    "uid": 206,
    "latitude": 39.49268016612935,
    "longitude": -0.389098149691761
  },
  {
    "address": "Valle de la Ballestera - Hospital Nueve de Octubre",
    "uid": 223,
    "latitude": 39.47852611446744,
    "longitude": -0.40127817864509
  },
  {
    "address": "Santa Cruz de Tenerife, 21",
    "uid": 214,
    "latitude": 39.46490006867376,
    "longitude": -0.400667169496185
  },
  {
    "address": "Barcas, 11",
    "uid": 40,
    "latitude": 39.470434095658966,
    "longitude": -0.374915095234885
  },
  {
    "address": "Cast\\u00e1n Tobe\\u00f1as - Rincon de Ademuz",
    "uid": 219,
    "latitude": 39.472789,
    "longitude": -0.401684
  },
  {
    "address": "Plaza de Tetu\\u00e1n",
    "uid": 9,
    "latitude": 39.474323110354575,
    "longitude": -0.370021082654504
  },
  {
    "address": "Baleares - R\\u00edo Escalona",
    "uid": 59,
    "latitude": 39.46305607733176,
    "longitude": -0.354640030484729
  },
  {
    "address": "Cirilo Amor\\u00f3s - Jorge Juan (Mercado Col\\u00f3n)",
    "uid": 30,
    "latitude": 39.468569091411034,
    "longitude": -0.368590075268948
  },
  {
    "address": "Grabador Jordan - Escultor Pastor",
    "uid": 91,
    "latitude": 39.444093009140374,
    "longitude": -0.367664059275921
  },
  {
    "address": "Plaza de la Virgen - Bail\\u00eda",
    "uid": 4,
    "latitude": 39.476715116676885,
    "longitude": -0.375433100164395
  },
  {
    "address": "Gran Canaria - Ingeniero Manuel Maese",
    "uid": 171,
    "latitude": 39.48302815289556,
    "longitude": -0.329098964737767
  },
  {
    "address": "Gasc\\u00f3 Oliag - Primado Reig",
    "uid": 117,
    "latitude": 39.47921913101099,
    "longitude": -0.357156046729095
  },
  {
    "address": "Canal de Navarr\\u00e9s - Maestro Rodrigo",
    "uid": 266,
    "latitude": 39.490257152389816,
    "longitude": -0.406462200475517
  },
  {
    "address": "Naranjos (Magisterio)",
    "uid": 95,
    "latitude": 39.479803136509446,
    "longitude": -0.34621001424835
  },
  {
    "address": "Angel Guimer\\u00e1 - Juan Llorens",
    "uid": 22,
    "latitude": 39.469755089002845,
    "longitude": -0.388444135439494
  },
  {
    "address": "Francia - Pintor Maella",
    "uid": 55,
    "latitude": 39.45931106660426,
    "longitude": -0.348777010892994
  },
  {
    "address": "Huesca - Bar\\u00f3n de C\\u00e1rcer",
    "uid": 14,
    "latitude": 39.46893508902061,
    "longitude": -0.379815109120643
  },
  {
    "address": "Menorca - Baleares",
    "uid": 62,
    "latitude": 39.45977907030124,
    "longitude": -0.342242991568314
  },
  {
    "address": "Marqu\\u00e9s de San Juan - Diputat Llu\\u00eds Luc\\u00eda",
    "uid": 144,
    "latitude": 39.478747117048464,
    "longitude": -0.395571161644053
  },
  {
    "address": "Blasco Iba\\u00f1ez, 23 (F. Filosof\\u00eda y Psicolog\\u00eda)",
    "uid": 89,
    "latitude": 39.47777012539991,
    "longitude": -0.359409052701581
  },
  {
    "address": "Hospital Nueva Fe (administraci\\u00f3n)",
    "uid": 189,
    "latitude": 39.4446390082756,
    "longitude": -0.37599308454911
  },
  {
    "address": "Amadeo de Saboya (frente Ayuntamiento)",
    "uid": 80,
    "latitude": 39.47371011075012,
    "longitude": -0.362389059448826
  },
  {
    "address": "Almazora - Benimuslem",
    "uid": 129,
    "latitude": 39.485943149532034,
    "longitude": -0.37003608893869
  },
  {
    "address": "Zapadores, 23",
    "uid": 181,
    "latitude": 39.45796705574948,
    "longitude": -0.368282068629998
  },
  {
    "address": "General Elio - Llano del Real",
    "uid": 83,
    "latitude": 39.47755312220108,
    "longitude": -0.367061075518166
  },
  {
    "address": "Fray Juan\\u00edpero Serra - Torrente",
    "uid": 212,
    "latitude": 39.461452057092146,
    "longitude": -0.40048716710112
  },
  {
    "address": "Arag\\u00f3n - Vicente Sancho Tello",
    "uid": 78,
    "latitude": 39.469913099113434,
    "longitude": -0.358779046585651
  },
  {
    "address": "Peris y Valero - Salamanca",
    "uid": 149,
    "latitude": 39.462902074466406,
    "longitude": -0.361886052119026
  },
  {
    "address": "Cno. Moncada - Pedro Patricio Mey",
    "uid": 176,
    "latitude": 39.491985167042394,
    "longitude": -0.378944118876901
  },
  {
    "address": "Fernando el Cat\\u00f3lico - Erudito Orellana",
    "uid": 23,
    "latitude": 39.47159609591605,
    "longitude": -0.3862691299059
  },
  {
    "address": "Hospital - Horno del Hospital",
    "uid": 10,
    "latitude": 39.47078409432719,
    "longitude": -0.382701118769287
  },
  {
    "address": "Baleares - Leb\\u00f3n",
    "uid": 60,
    "latitude": 39.462199075340266,
    "longitude": -0.351867021712218
  },
  {
    "address": "Aitana - Florista",
    "uid": 229,
    "latitude": 39.49307216559276,
    "longitude": -0.394897167291038
  },
  {
    "address": "Reus - Alquer\\u00eda de la Estrella",
    "uid": 139,
    "latitude": 39.48567414446925,
    "longitude": -0.382964127549696
  },
  {
    "address": "Av. Puerto - Serrer\\u00eda",
    "uid": 69,
    "latitude": 39.46192007863096,
    "longitude": -0.338811982446624
  },
  {
    "address": "Nueve de Octubre - Cieza",
    "uid": 245,
    "latitude": 39.47202809113153,
    "longitude": -0.405642188252898
  },
  {
    "address": "Ninot - Regino Mas",
    "uid": 270,
    "latitude": 39.50004318971825,
    "longitude": -0.392980165265021
  },
  {
    "address": "Plaza Am\\u00e9rica - Cirilo Amor\\u00f3s - Sorn\\u00ed",
    "uid": 29,
    "latitude": 39.47008009754055,
    "longitude": -0.36539006649012
  },
  {
    "address": "Peris y Valero - Cabo Jubi",
    "uid": 38,
    "latitude": 39.459369828677254,
    "longitude": -0.370340076012569
  },
  {
    "address": "Dels Gremis - Campos Crespo",
    "uid": 252,
    "latitude": 39.450256018261534,
    "longitude": -0.4036651706049
  },
  {
    "address": "Cra. Malilla - Olt\\u00e1",
    "uid": 192,
    "latitude": 39.45260503424732,
    "longitude": -0.378805097289263
  },
  {
    "address": "Llano de la Zauid\\u00eda - Doctor Ol\\u00f3riz",
    "uid": 153,
    "latitude": 39.48313013675752,
    "longitude": -0.380263118087985
  },
  {
    "address": "Jorge Comin (Metge) - Terrateig",
    "uid": 224,
    "latitude": 39.480536120478945,
    "longitude": -0.403679186925212
  },
  {
    "address": "UPV Caminos",
    "uid": 113,
    "latitude": 39.48124414217441,
    "longitude": -0.343702007510602
  },
  {
    "address": "Carteros - Mossen Febrer",
    "uid": 208,
    "latitude": 39.451660027860314,
    "longitude": -0.38866912636635
  },
  {
    "address": "Autopista del Saler - Antonio Ferrandis (C.C. El Saler)",
    "uid": 50,
    "latitude": 39.453286044933414,
    "longitude": -0.352950020141597
  },
  {
    "address": "Gregorio Gea - Profesor Beltr\\u00e1n B\\u00e1guena",
    "uid": 142,
    "latitude": 39.48188812951767,
    "longitude": -0.389750145869399
  },
  {
    "address": "Campamento, 81",
    "uid": 269,
    "latitude": 39.499875178486775,
    "longitude": -0.426354265293528
  },
  {
    "address": "Rub\\u00e9n Dar\\u00edo - Plaza Fray Luis Colomer",
    "uid": 103,
    "latitude": 39.47842112983494,
    "longitude": -0.35246203223541
  },
  {
    "address": "Plaza Ayuntamiento - Cotanda",
    "uid": 11,
    "latitude": 39.471154097455006,
    "longitude": -0.376875101498657
  },
  {
    "address": "La Safor - Maestro Rodrigo",
    "uid": 242,
    "latitude": 39.48787514504915,
    "longitude": -0.404288192680299
  },
  {
    "address": "Tres Cruces - Pio XI",
    "uid": 250,
    "latitude": 39.45632,
    "longitude": -0.4008977
  },
  {
    "address": "Salvador Giner - C. Museo",
    "uid": 2,
    "latitude": 39.4798571258551,
    "longitude": -0.379839115061075
  },
  {
    "address": "Ausias March - Pianista Amparo Iturbi",
    "uid": 182,
    "latitude": 39.455400044894006,
    "longitude": -0.375056087555512
  },
  {
    "address": "Maestro Rodrigo - Manuel de Falla",
    "uid": 222,
    "latitude": 39.47686111010473,
    "longitude": -0.397373166037864
  },
  {
    "address": "Blasco Iba\\u00f1ez - Poeta Dur\\u00e1n Tortajada",
    "uid": 93,
    "latitude": 39.47225011180076,
    "longitude": -0.34390000326249
  },
  {
    "address": "Alameda - Pintor Monle\\u00f3n",
    "uid": 57,
    "latitude": 39.46444408081199,
    "longitude": -0.35834704234339
  },
  {
    "address": "Jos\\u00e9 Meli\\u00e1 Castell\\u00f3 - Campos Crespo",
    "uid": 253,
    "latitude": 39.452694027294896,
    "longitude": -0.401192164498328
  },
  {
    "address": "Salvador Ferrandis Luna - Juan Bautista Vives",
    "uid": 218,
    "latitude": 39.470708089307045,
    "longitude": -0.397475163041212
  },
  {
    "address": "Economista Gay - Constituci\\u00f3n",
    "uid": 136,
    "latitude": 39.4899091610835,
    "longitude": -0.375701108044157
  },
  {
    "address": "Vicente la Roda - Ingeniero Fausto Elio",
    "uid": 272,
    "latitude": 39.480591143659446,
    "longitude": -0.332280972954351
  },
  {
    "address": "Beato Nicol\\u00e1s Factor - Convento de Jes\\u00fas",
    "uid": 199,
    "latitude": 39.461556060298705,
    "longitude": -0.391649140641673
  },
  {
    "address": "Aularios Universuidad de Valencia",
    "uid": 105,
    "latitude": 39.477658129796865,
    "longitude": -0.344611008304436
  },
  {
    "address": "Alfahuir - Jos\\u00e9 Chabas Bordehore",
    "uid": 126,
    "latitude": 39.493066176705305,
    "longitude": -0.36019506326505
  },
  {
    "address": "Giorgeta - Roig de Corella",
    "uid": 195,
    "latitude": 39.45935205521343,
    "longitude": -0.384372117626406
  },
  {
    "address": "Plaza Badajoz",
    "uid": 145,
    "latitude": 39.481515125451246,
    "longitude": -0.398486171871458
  },
  {
    "address": "Moreras - Rona de Nazaret",
    "uid": 275,
    "latitude": 39.452273047299634,
    "longitude": -0.33515596627555
  },
  {
    "address": "Vicent Vuidal - Pintor Maella",
    "uid": 61,
    "latitude": 39.46126107345635,
    "longitude": -0.347921009381426
  },
  {
    "address": "Traginers - Pedrapiquers",
    "uid": 260,
    "latitude": 39.45952404837945,
    "longitude": -0.407308186532079
  },
  {
    "address": "Plaza de Europa",
    "uid": 54,
    "latitude": 39.45974606677287,
    "longitude": -0.352781023126671
  },
  {
    "address": "Hospital Nueva Fe (consultas externas)",
    "uid": 188,
    "latitude": 39.44446300836761,
    "longitude": -0.373882078122754
  },
  {
    "address": "Paseo Neptuno 32-34",
    "uid": 163,
    "latitude": 39.464405091973795,
    "longitude": -0.323491937905993
  },
  {
    "address": "Valle de la Ballestera - Pio Baroja",
    "uid": 244,
    "latitude": 39.47847411270088,
    "longitude": -0.406227193465013
  },
  {
    "address": "Rep\\u00fablica Argentina - Campoamor",
    "uid": 75,
    "latitude": 39.472081108680385,
    "longitude": -0.351792026815861
  },
  {
    "address": "Veles e Vents",
    "uid": 276,
    "latitude": 39.4619430835605,
    "longitude": -0.323855937664754
  },
  {
    "address": "Moreras (Oceanogr\\u00e1fico)",
    "uid": 51,
    "latitude": 39.45215104293519,
    "longitude": -0.347318002649104
  },
  {
    "address": "Peris y Valero - Luis Sant\\u00e1ngel",
    "uid": 37,
    "latitude": 39.46090558239442,
    "longitude": -0.366531327258963
  },
  {
    "address": "Azagador de Alboraya - Dolores Marqu\\u00e9s",
    "uid": 124,
    "latitude": 39.48846916061659,
    "longitude": -0.362046066347782
  },
  {
    "address": "Plaza Salvador Soria, 8",
    "uid": 257,
    "latitude": 39.44521900594571,
    "longitude": -0.389195124464717
  },
  {
    "address": "Doctor Lluch - Virgen del Sufragio",
    "uid": 158,
    "latitude": 39.46666609819163,
    "longitude": -0.327809952057852
  },
  {
    "address": "Escultor Jos\\u00e9 Capuz - Oriente",
    "uid": 43,
    "latitude": 39.45944506301512,
    "longitude": -0.361244048329219
  },
  {
    "address": "Camp del Turia - Corts Valencianes",
    "uid": 240,
    "latitude": 39.494972169842995,
    "longitude": -0.401638188523523
  },
  {
    "address": "Manuel Candela - Rodriguez de Cepeda",
    "uid": 150,
    "latitude": 39.467689094227495,
    "longitude": -0.350700021177111
  },
  {
    "address": "Cast\\u00e1n Tobe\\u00f1as - Patriques",
    "uid": 220,
    "latitude": 39.473823099518064,
    "longitude": -0.398412167524643
  },
  {
    "address": "Doctor Nicasio Benlloch - L'Horta Sud",
    "uid": 228,
    "latitude": 39.49366016633798,
    "longitude": -0.398762179196655
  },
  {
    "address": "Guillem de Castro - San Pedro Pascual",
    "uid": 6,
    "latitude": 39.47276610053889,
    "longitude": -0.384174124251856
  },
  {
    "address": "Doctor Waksman - Nieves",
    "uid": 180,
    "latitude": 39.45598904826899,
    "longitude": -0.370774075033782
  },
  {
    "address": "Alcudia de Crespins - Pedro Patricio Mey",
    "uid": 177,
    "latitude": 39.493513173915595,
    "longitude": -0.373579103612918
  },
  {
    "address": "Beniferri - Vicent Tom\\u00e1s Mart\\u00ed",
    "uid": 267,
    "latitude": 39.4941361656198,
    "longitude": -0.406020201221407
  },
  {
    "address": "Pescadores - Progreso",
    "uid": 154,
    "latitude": 39.46984410830488,
    "longitude": -0.329644959268791
  },
  {
    "address": "Pio IX - M\\u00fasico Cabanilles",
    "uid": 259,
    "latitude": 39.44542800538199,
    "longitude": -0.393106136312086
  },
  {
    "address": "Gaspar Aguilar - Vicente Parra",
    "uid": 86,
    "latitude": 39.45520803898995,
    "longitude": -0.391284136125771
  },
  {
    "address": "Jer\\u00f3nimo Monsoriu - Industria",
    "uid": 73,
    "latitude": 39.46622709180114,
    "longitude": -0.342969997228533
  },
  {
    "address": "Esparraguera - Cra. Malilla",
    "uid": 191,
    "latitude": 39.450297026408926,
    "longitude": -0.378957096498364
  },
  {
    "address": "Plaza Poeta Llorente",
    "uid": 5,
    "latitude": 39.47686311853029,
    "longitude": -0.371231087646942
  },
  {
    "address": "Ausias March - Av. de la Plata",
    "uid": 183,
    "latitude": 39.452588036275,
    "longitude": -0.372375077997377
  },
  {
    "address": "Marino Blas de Lezo (estaci\\u00f3n Caba\\u00f1al Adif)",
    "uid": 101,
    "latitude": 39.470036107395714,
    "longitude": -0.334451973768557
  },
  {
    "address": "Fernando el Cat\\u00f3lico - Cuenca",
    "uid": 24,
    "latitude": 39.469045088002915,
    "longitude": -0.384118122083548
  },
  {
    "address": "La Vall d'Albauida - Corts Valencianes",
    "uid": 241,
    "latitude": 39.49294616289816,
    "longitude": -0.401981188470323
  },
  {
    "address": "Alfahuir - Duque de Mandas",
    "uid": 127,
    "latitude": 39.49055916686075,
    "longitude": -0.36453707493258
  },
  {
    "address": "Av. Puerto 61-63",
    "uid": 64,
    "latitude": 39.46648608901148,
    "longitude": -0.354286031274125
  },
  {
    "address": "Blasco Iba\\u00f1ez, 32 (F. Filolog\\u00eda)",
    "uid": 90,
    "latitude": 39.47670212191253,
    "longitude": -0.35905805107551
  },
  {
    "address": "Av. Campanar (La Fe)",
    "uid": 146,
    "latitude": 39.48507813986351,
    "longitude": -0.391043151456223
  },
  {
    "address": "Mediterr\\u00e1neo - Plaza Cruz de Ca\\u00f1amelar",
    "uid": 161,
    "latitude": 39.467937101175984,
    "longitude": -0.331816964744236
  },
  {
    "address": "Jos\\u00e9 Mar\\u00eda de Haro - Justo y Pastor",
    "uid": 160,
    "latitude": 39.46730409669327,
    "longitude": -0.33907198613268
  },
  {
    "address": "Ebanista Caselles - Ausias March",
    "uid": 185,
    "latitude": 39.45001202816097,
    "longitude": -0.37059407126453
  },
  {
    "address": "Guillem de Anglesola - Av. Puerto",
    "uid": 66,
    "latitude": 39.46423908400446,
    "longitude": -0.346352006288128
  },
  {
    "address": "P\\u00edo XII - Monestir de Poblet",
    "uid": 173,
    "latitude": 39.48450913674717,
    "longitude": -0.394769162326153
  },
  {
    "address": "Poeta Serrano Clavero - General Llorens",
    "uid": 230,
    "latitude": 39.494718173715064,
    "longitude": -0.386875144115863
  },
  {
    "address": "Av. Puerto - Plaza Tribunal de les Aig\\u00fces",
    "uid": 71,
    "latitude": 39.460348075016185,
    "longitude": -0.333621966050435
  },
  {
    "address": "Av. del Cuid - Juli\\u00e1n Pe\\u00f1a",
    "uid": 200,
    "latitude": 39.4677050805168,
    "longitude": -0.393305148918046
  },
  {
    "address": "Bombero Ram\\u00f3n Duart - Hermanos Maristas",
    "uid": 184,
    "latitude": 39.452750038999675,
    "longitude": -0.365661057953976
  },
  {
    "address": "Tres Forques - Colonia Espa\\u00f1ola de Mexico",
    "uid": 262,
    "latitude": 39.46344006095387,
    "longitude": -0.409316194664879
  },
  {
    "address": "Ribera - Plaza Ayuntamiento",
    "uid": 15,
    "latitude": 39.469056090688554,
    "longitude": -0.375728096943875
  },
  {
    "address": "Tres Cruces - Segunda Rep\\u00fablica Espa\\u00f1ola",
    "uid": 249,
    "latitude": 39.459474049657025,
    "longitude": -0.402831173069948
  },
  {
    "address": "Regne de Valencia - Almirante Cadarso",
    "uid": 35,
    "latitude": 39.46371707553277,
    "longitude": -0.367085068143159
  },
  {
    "address": "Quart - Fernando el Cat\\u00f3lico",
    "uid": 20,
    "latitude": 39.47500210686962,
    "longitude": -0.387938136740957
  },
  {
    "address": "Plaza de los Fueros - Conde Trenor",
    "uid": 36,
    "latitude": 39.47904012448793,
    "longitude": -0.375527101694727
  },
  {
    "address": "Corts Valencianes - La Safor",
    "uid": 226,
    "latitude": 39.490832156737454,
    "longitude": -0.398950178249295
  },
  {
    "address": "Isabel de Villena - Mendiz\\u00e1bal",
    "uid": 170,
    "latitude": 39.48294115378812,
    "longitude": -0.325424953690914
  },
]