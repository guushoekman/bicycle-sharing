var stations = [
  {
    "address": "Flandre - Alphonse Karr",
    "uid": "19008",
    "latitude": 48.89465,
    "longitude": 2.381868
  },
  {
    "address": "Charenton - Place du Col Bourgoin",
    "uid": "12011",
    "latitude": 48.84486731955409,
    "longitude": 2.382578935054453
  },
  {
    "address": "Phalsbourg-Villiers",
    "uid": "17019",
    "latitude": 48.88242307900951,
    "longitude": 2.3090453445911407
  },
  {
    "address": "Claude Decaen - Cannebière",
    "uid": "12111",
    "latitude": 48.83761417037952,
    "longitude": 2.397253782195704
  },
  {
    "address": "Paradis - Hauteville",
    "uid": "10019",
    "latitude": 48.874999954666755,
    "longitude": 2.3522757401923404
  },
  {
    "address": "Legendre - Nollet",
    "uid": "17110",
    "latitude": 48.887742186754224,
    "longitude": 2.320293079881375
  },
  {
    "address": "Georges V - François 1er",
    "uid": "8049",
    "latitude": 48.87042247158609,
    "longitude": 2.301069796085358
  },
  {
    "address": "Charles de Gaulle - Beauté ",
    "uid": "41302",
    "latitude": 48.83649602312711,
    "longitude": 2.479562651149885
  },
  {
    "address": "Saint-Mandé - Docteur Arnold Netter",
    "uid": "12017",
    "latitude": 48.84462585769668,
    "longitude": 2.404946386814118
  },
  {
    "address": "Traversière - Ledru-Rollin",
    "uid": "12004",
    "latitude": 48.850735824128805,
    "longitude": 2.375870111706245
  },
  {
    "address": "Charonne - Avron",
    "uid": "20043",
    "latitude": 48.85160266522164,
    "longitude": 2.398403047037931
  },
  {
    "address": "Ménilmontant - Père Lachaise",
    "uid": "20030",
    "latitude": 48.86328729150227,
    "longitude": 2.3869940266013145
  },
  {
    "address": "Sablons - Georges Mandel",
    "uid": "16108",
    "latitude": 48.863875,
    "longitude": 2.28189
  },
  {
    "address": "Francoeur - Caulaincourt ",
    "uid": "18016",
    "latitude": 48.8899944409612,
    "longitude": 2.342529706656933
  },
  {
    "address": "Vignerons - Minimes",
    "uid": "43006",
    "latitude": 48.841486,
    "longitude": 2.430793
  },
  {
    "address": "Louis Lumière ",
    "uid": "20104",
    "latitude": 48.86216198070008,
    "longitude": 2.41112578185621
  },
  {
    "address": "Davout - Volga",
    "uid": "20044",
    "latitude": 48.852919679957296,
    "longitude": 2.410522909779111
  },
  {
    "address": "Porte de Charenton",
    "uid": "12040",
    "latitude": 48.83124010585643,
    "longitude": 2.3989107459783554
  },
  {
    "address": "Château de Vincennes",
    "uid": "12123",
    "latitude": 48.84423193255657,
    "longitude": 2.439209537709179
  },
  {
    "address": "Ménilmontant - Oberkampf",
    "uid": "11029",
    "latitude": 48.8666176586814,
    "longitude": 2.38301344041578
  },
  {
    "address": "Vaugirard - Pasteur",
    "uid": "15004",
    "latitude": 48.84040868803496,
    "longitude": 2.3154367133975033
  },
  {
    "address": "Tardieu - Chappe",
    "uid": "18005",
    "latitude": 48.88414675978186,
    "longitude": 2.3418454080820084
  },
  {
    "address": "Marché Saint-Quentin",
    "uid": "10021",
    "latitude": 48.877356057652264,
    "longitude": 2.354511445637388
  },
  {
    "address": "Javel - André Citroen",
    "uid": "15064",
    "latitude": 48.846276485475,
    "longitude": 2.2786368057131767
  },
  {
    "address": "Place de la Fraternité",
    "uid": "31709",
    "latitude": 48.857050394743816,
    "longitude": 2.422549956709779
  },
  {
    "address": "Boudreau - Auber",
    "uid": "9106",
    "latitude": 48.87219,
    "longitude": 2.329423
  },
  {
    "address": "Sibelle - Alésia",
    "uid": "14012",
    "latitude": 48.826701758539215,
    "longitude": 2.3384983465075493
  },
  {
    "address": "Jean Jaurès - Jean Baptiste clément ",
    "uid": "21003",
    "latitude": 48.845479089676,
    "longitude": 2.237240932881832
  },
  {
    "address": "Nationale - Marcel Duchamp",
    "uid": "13114",
    "latitude": 48.82463749337136,
    "longitude": 2.3678014799952507
  },
  {
    "address": "Edgar Quinet - Gaité",
    "uid": "14001",
    "latitude": 48.84116092896631,
    "longitude": 2.32459545135498
  },
  {
    "address": "Sadi Carnot - François Mitterrand ",
    "uid": "31703",
    "latitude": 48.87532687671517,
    "longitude": 2.4241617321968083
  },
  {
    "address": "Flandrin - Longchamp",
    "uid": "16012",
    "latitude": 48.86877849178477,
    "longitude": 2.274354539176829
  },
  {
    "address": "François Truffaut - Saint Emilion",
    "uid": "12033",
    "latitude": 48.83372354344807,
    "longitude": 2.386601903271428
  },
  {
    "address": "Place de l'Insurrection d'Août 1944 ",
    "uid": "42003",
    "latitude": 48.81477852751594,
    "longitude": 2.398074567317963
  },
  {
    "address": "Gay Lussac - Saint-Jacques",
    "uid": "5005",
    "latitude": 48.844730256132095,
    "longitude": 2.341923944866407
  },
  {
    "address": "Madeleine Michelis - Roule",
    "uid": "22012",
    "latitude": 48.88277209719355,
    "longitude": 2.277434505522251
  },
  {
    "address": "Mairie du 17ème",
    "uid": "17012",
    "latitude": 48.88416184871573,
    "longitude": 2.322079431103719
  },
  {
    "address": "Henry Farman",
    "uid": "15042",
    "latitude": 48.83372430280548,
    "longitude": 2.2713854536414146
  },
  {
    "address": "Ceinture du Lac Inferieure - Saint-Cloud",
    "uid": "16129",
    "latitude": 48.86218070933143,
    "longitude": 2.2617871686816216
  },
  {
    "address": "Paul Vaillant-Couturier - Victor Hugo",
    "uid": "23004",
    "latitude": 48.89819702263572,
    "longitude": 2.293848017974069
  },
  {
    "address": "Place Lafayette",
    "uid": "21306",
    "latitude": 48.82933141031328,
    "longitude": 2.2637230530381203
  },
  {
    "address": "Chateaubriand - Friedland",
    "uid": "8104",
    "latitude": 48.8740144756374,
    "longitude": 2.29991625786582
  },
  {
    "address": "Departement - Pajol",
    "uid": "18110",
    "latitude": 48.886674849730184,
    "longitude": 2.3613611608743668
  },
  {
    "address": "Malakoff  place de la porte maillot ",
    "uid": "16101",
    "latitude": 48.876733496637506,
    "longitude": 2.2834577411413193
  },
  {
    "address": "Dom-Pérignon - Gravelle",
    "uid": "12119",
    "latitude": 48.82548157443457,
    "longitude": 2.4098803102970128
  },
  {
    "address": "Manin - Secrétan",
    "uid": "19114",
    "latitude": 48.8791665,
    "longitude": 2.3787959
  },
  {
    "address": "Maisons-Alfort - Stade Auguste Delaune",
    "uid": "47004",
    "latitude": 48.8092658449553,
    "longitude": 2.4340999871492386
  },
  {
    "address": "Bercy",
    "uid": "12108",
    "latitude": 48.83996691499447,
    "longitude": 2.378862574696541
  },
  {
    "address": "Victoire - Laffite",
    "uid": "9111",
    "latitude": 48.87514373494672,
    "longitude": 2.338118505144376
  },
  {
    "address": "Jean Jaurès - Salvador Allende",
    "uid": "22402",
    "latitude": 48.82146007977343,
    "longitude": 2.302899459965933
  },
  {
    "address": "Pierre Joseph Desault",
    "uid": "13120",
    "latitude": 48.82110230800372,
    "longitude": 2.378775402903557
  },
  {
    "address": "Mairie  ",
    "uid": "31701",
    "latitude": 48.868399043864684,
    "longitude": 2.4179135262966156
  },
  {
    "address": "Saint-Mande - Picpus",
    "uid": "12016",
    "latitude": 48.845127728277696,
    "longitude": 2.4015686296481897
  },
  {
    "address": "Marcadet - Ramey",
    "uid": "18021",
    "latitude": 48.890875,
    "longitude": 2.3449019
  },
  {
    "address": "Assas - Rennes",
    "uid": "6025",
    "latitude": 48.8492084999386,
    "longitude": 2.32851454623001
  },
  {
    "address": "Gare de Bercy",
    "uid": "12024",
    "latitude": 48.839794573905685,
    "longitude": 2.382606603205204
  },
  {
    "address": "Naples - Rome",
    "uid": "8019",
    "latitude": 48.879960700846226,
    "longitude": 2.321358695626259
  },
  {
    "address": "Mairie",
    "uid": "42704",
    "latitude": 48.81191981537702,
    "longitude": 2.3570189997553825
  },
  {
    "address": "Nationale - Vincent Auriol",
    "uid": "13017",
    "latitude": 48.83252708397293,
    "longitude": 2.362354410392119
  },
  {
    "address": "Saint-Honoré - 29 juillet",
    "uid": "1017",
    "latitude": 48.86557114282421,
    "longitude": 2.330567417577153
  },
  {
    "address": "Marché aux fleurs",
    "uid": "4002",
    "latitude": 48.85522061347032,
    "longitude": 2.347293309867382
  },
  {
    "address": "Enghien - Faubourg Poissonnière",
    "uid": "10042",
    "latitude": 48.87242006305313,
    "longitude": 2.348395236282807
  },
  {
    "address": "Argenson-Château",
    "uid": "22002",
    "latitude": 48.888440490538756,
    "longitude": 2.2641297429800034
  },
  {
    "address": "Cité Vaneau",
    "uid": "7012",
    "latitude": 48.8542210666207,
    "longitude": 2.319614924490452
  },
  {
    "address": "Place Tristan Bernard ",
    "uid": "17036",
    "latitude": 48.8791964,
    "longitude": 2.2915403
  },
  {
    "address": "Square Louis XVI",
    "uid": "8016",
    "latitude": 48.87320440102894,
    "longitude": 2.323658830743138
  },
  {
    "address": "Claude Vellefaux - Hôpital Saint-Louis",
    "uid": "10032",
    "latitude": 48.87285913900304,
    "longitude": 2.370071706985146
  },
  {
    "address": "Reuilly -Daumesnil ",
    "uid": "12037",
    "latitude": 48.8395385,
    "longitude": 2.3972516
  },
  {
    "address": "Louis Blanc - La Chapelle",
    "uid": "10034",
    "latitude": 48.88400896451429,
    "longitude": 2.3599335551261906
  },
  {
    "address": "Cité des Sciences et de l'Industrie",
    "uid": "19009",
    "latitude": 48.89659571178903,
    "longitude": 2.384599149227142
  },
  {
    "address": "Place Monge",
    "uid": "5024",
    "latitude": 48.842764450905456,
    "longitude": 2.352406606078148
  },
  {
    "address": "Manin - Simon Bolivar",
    "uid": "19023",
    "latitude": 48.87649965412676,
    "longitude": 2.379407362599104
  },
  {
    "address": "Commandant Rivière",
    "uid": "8033",
    "latitude": 48.872933,
    "longitude": 2.309796
  },
  {
    "address": "Bruant - Vincent Auriol",
    "uid": "13018",
    "latitude": 48.83484845292942,
    "longitude": 2.366826499627087
  },
  {
    "address": "Square des Saint-Simoniens",
    "uid": "20119",
    "latitude": 48.8701796,
    "longitude": 2.3967273
  },
  {
    "address": "Pajol - Riquet ",
    "uid": "18109",
    "latitude": 48.88960028134536,
    "longitude": 2.3628534749150276
  },
  {
    "address": "Cloître Saint-Merri - Renard",
    "uid": "4019",
    "latitude": 48.85883544889818,
    "longitude": 2.351827919483185
  },
  {
    "address": "Porte d'Aubervilliers",
    "uid": "18049",
    "latitude": 48.89859661942376,
    "longitude": 2.3689722642302513
  },
  {
    "address": "Claude Debussy - Marc Bloch",
    "uid": "21112",
    "latitude": 48.91034917741103,
    "longitude": 2.3112118989229202
  },
  {
    "address": "Mairie du 12ème",
    "uid": "12109",
    "latitude": 48.84086149032458,
    "longitude": 2.3876759782433505
  },
  {
    "address": "Longchamp - Place d'Iéna",
    "uid": "16007",
    "latitude": 48.86491723343388,
    "longitude": 2.2926748171448708
  },
  {
    "address": "Pernety - Raymond Losserand",
    "uid": "14030",
    "latitude": 48.83425462795304,
    "longitude": 2.317609339386963
  },
  {
    "address": "Boyer-Barret - Raymond Losserand",
    "uid": "14104",
    "latitude": 48.83329880233322,
    "longitude": 2.3173383995890617
  },
  {
    "address": "Fragonard - Porte de Clichy",
    "uid": "17011",
    "latitude": 48.89415563341899,
    "longitude": 2.314389310777188
  },
  {
    "address": "Ségur - d'Estrées",
    "uid": "7018",
    "latitude": 48.85141997898257,
    "longitude": 2.3097044974565506
  },
  {
    "address": "Lavoisier - Missak Manouchian",
    "uid": "31105",
    "latitude": 48.86286468896063,
    "longitude": 2.495676465332508
  },
  {
    "address": "Dominique Pado - Croix Nivert",
    "uid": "15050",
    "latitude": 48.83656,
    "longitude": 2.28976
  },
  {
    "address": "Parc Georges Brassens - Brancion",
    "uid": "15046",
    "latitude": 48.83227829269761,
    "longitude": 2.3022570088505745
  },
  {
    "address": "Gambetta - Saint Fargeau",
    "uid": "20037",
    "latitude": 48.871244610447015,
    "longitude": 2.4039056897163396
  },
  {
    "address": "Sorbonne - Ecoles",
    "uid": "5030",
    "latitude": 48.84968656619092,
    "longitude": 2.3434996604919434
  },
  {
    "address": "Porte de Pantin - Petits Ponts",
    "uid": "19034",
    "latitude": 48.89034195521685,
    "longitude": 2.397813642189543
  },
  {
    "address": "Saint-Denis - Réaumur",
    "uid": "2003",
    "latitude": 48.8661116315789,
    "longitude": 2.3507790675507594
  },
  {
    "address": "Temple - Haudriettes",
    "uid": "3009",
    "latitude": 48.861636880211584,
    "longitude": 2.356696020645806
  },
  {
    "address": "Bréguet - Sabin",
    "uid": "11033",
    "latitude": 48.856931678257446,
    "longitude": 2.3706749462105408
  },
  {
    "address": "Cujas Saint-Michel",
    "uid": "5106",
    "latitude": 48.84819134858432,
    "longitude": 2.34183165485398
  },
  {
    "address": "Square Claude Bernard",
    "uid": "19110",
    "latitude": 48.89876016219984,
    "longitude": 2.375325746834278
  },
  {
    "address": "Lecourbe - Jean Maridor",
    "uid": "15054",
    "latitude": 48.83796366147599,
    "longitude": 2.2877392172813416
  },
  {
    "address": "Port Royal - Hôpital Cochin",
    "uid": "14004",
    "latitude": 48.838373233813286,
    "longitude": 2.340717874467373
  },
  {
    "address": "Laffitte - Italiens",
    "uid": "9023",
    "latitude": 48.871815737396304,
    "longitude": 2.337201547845077
  },
  {
    "address": "Vivaldi - Antoine-Julien Hénard",
    "uid": "12028",
    "latitude": 48.841923663276546,
    "longitude": 2.389728979467376
  },
  {
    "address": "Porte de la Chapelle",
    "uid": "18048",
    "latitude": 48.89845996601992,
    "longitude": 2.3607194423675537
  },
  {
    "address": "Grenelle - Violet",
    "uid": "15106",
    "latitude": 48.84993035512154,
    "longitude": 2.294612042605877
  },
  {
    "address": "Charentonneau - Maisons Alfort",
    "uid": "47003",
    "latitude": 48.809932980484355,
    "longitude": 2.443288456496946
  },
  {
    "address": "Pierre Brossolette  Adolphe Pinard",
    "uid": "22403",
    "latitude": 48.8218109,
    "longitude": 2.3131814
  },
  {
    "address": "Petites Ecuries - Faubourg Saint-Denis",
    "uid": "10006",
    "latitude": 48.87295368099433,
    "longitude": 2.3539475351572037
  },
  {
    "address": "Bienfaisance - Place de Narvik",
    "uid": "8035",
    "latitude": 48.87660163459077,
    "longitude": 2.3129771277308464
  },
  {
    "address": "Kléber - Gouverneur Général Eboué",
    "uid": "21310",
    "latitude": 48.82692976736927,
    "longitude": 2.272250431956518
  },
  {
    "address": "Boulets - Faubourg Saint-Antoine",
    "uid": "11010",
    "latitude": 48.84925926211519,
    "longitude": 2.391754614194301
  },
  {
    "address": "Belliard - Damrémont",
    "uid": "18033",
    "latitude": 48.89669238777841,
    "longitude": 2.338023450388167
  },
  {
    "address": "Jemmapes - Ecluses Saint-Martin ",
    "uid": "10025",
    "latitude": 48.87748519186726,
    "longitude": 2.3660067468881607
  },
  {
    "address": "Lagny - Auguste Blanqui ",
    "uid": "31001",
    "latitude": 48.8493253,
    "longitude": 2.4215047
  },
  {
    "address": "Ecoles - Carmes",
    "uid": "5007",
    "latitude": 48.848904447989405,
    "longitude": 2.3473486304283147
  },
  {
    "address": "Lallier - Trudaine ",
    "uid": "9017",
    "latitude": 48.88109246617963,
    "longitude": 2.3408224806189537
  },
  {
    "address": "Place de la vache noire",
    "uid": "41103",
    "latitude": 48.8103714,
    "longitude": 2.32661
  },
  {
    "address": "Santé - Arago",
    "uid": "14113",
    "latitude": 48.83507657562064,
    "longitude": 2.3415737262044938
  },
  {
    "address": "Buisson Saint-Louis - Saint-Maur",
    "uid": "10024",
    "latitude": 48.87174684930295,
    "longitude": 2.3722528328789854
  },
  {
    "address": "Ménilmontant - Pelleport",
    "uid": "20036",
    "latitude": 48.87068263384462,
    "longitude": 2.398959602331646
  },
  {
    "address": "Rome - Provence",
    "uid": "8007",
    "latitude": 48.874230055580306,
    "longitude": 2.325570111724113
  },
  {
    "address": "Jean Bologne - Passy",
    "uid": "16022",
    "latitude": 48.857566889784465,
    "longitude": 2.280027395306772
  },
  {
    "address": "Duméril - Jeanne d'Arc",
    "uid": "13006",
    "latitude": 48.83815160808658,
    "longitude": 2.357108449556404
  },
  {
    "address": "Place de l'Édit de Nantes",
    "uid": "19125",
    "latitude": 48.88879,
    "longitude": 2.3785
  },
  {
    "address": "Mairie",
    "uid": "21108",
    "latitude": 48.90360140312434,
    "longitude": 2.3060955852270126
  },
  {
    "address": "Porte de Bagnolet",
    "uid": "20022",
    "latitude": 48.864707139122984,
    "longitude": 2.408344637349607
  },
  {
    "address": "Place Falguière",
    "uid": "15017",
    "latitude": 48.83629973941847,
    "longitude": 2.3104464635252953
  },
  {
    "address": "Etienne Marcel - Montorgueil",
    "uid": "2002",
    "latitude": 48.86450567259199,
    "longitude": 2.3461959511041637
  },
  {
    "address": "Square Raymond Souplex",
    "uid": "18019",
    "latitude": 48.89183721478911,
    "longitude": 2.335426103165901
  },
  {
    "address": "François 1er - Montaigne",
    "uid": "8038",
    "latitude": 48.866887649978096,
    "longitude": 2.306675575720671
  },
  {
    "address": "Laumière - Petit",
    "uid": "19013",
    "latitude": 48.88467368538326,
    "longitude": 2.380039058625698
  },
  {
    "address": "Mairie du 18ème",
    "uid": "18025",
    "latitude": 48.892939296937605,
    "longitude": 2.3445510864257812
  },
  {
    "address": "Square-Montholon",
    "uid": "9009",
    "latitude": 48.876620377545024,
    "longitude": 2.3457641154527664
  },
  {
    "address": "Pierre Brossolette - 12 Février 1934",
    "uid": "22404",
    "latitude": 48.81773730394612,
    "longitude": 2.3076703771948814
  },
  {
    "address": "Dantzig - Lefebvre ",
    "uid": "15044",
    "latitude": 48.83013921586756,
    "longitude": 2.295934036374092
  },
  {
    "address": "Parc Monceau",
    "uid": "17018",
    "latitude": 48.88107791412864,
    "longitude": 2.3095492646098137
  },
  {
    "address": "Mairie",
    "uid": "32301",
    "latitude": 48.885109,
    "longitude": 2.435679
  },
  {
    "address": "Sahel - Général Michel Bizot",
    "uid": "12022",
    "latitude": 48.840872964793355,
    "longitude": 2.4044444784522057
  },
  {
    "address": "Porte de la Villette",
    "uid": "19115",
    "latitude": 48.89835196548483,
    "longitude": 2.386150136590004
  },
  {
    "address": "Gouthière - Poterne des Peupliers",
    "uid": "13028",
    "latitude": 48.820507,
    "longitude": 2.351342
  },
  {
    "address": "Exelmans - Versailles",
    "uid": "16039",
    "latitude": 48.84097491091973,
    "longitude": 2.264390252530575
  },
  {
    "address": "Place de la Nation - Taillebourg",
    "uid": "11014",
    "latitude": 48.8488408,
    "longitude": 2.3973043
  },
  {
    "address": "Porte de la Plaine",
    "uid": "15045",
    "latitude": 48.82805169605981,
    "longitude": 2.2924277186393742
  },
  {
    "address": "Berges de Seine - Concorde",
    "uid": "7112",
    "latitude": 48.863096900140626,
    "longitude": 2.316989621020396
  },
  {
    "address": "Montparnasse -  Square Ozanam",
    "uid": "6005",
    "latitude": 48.84331056782166,
    "longitude": 2.326524294912815
  },
  {
    "address": "Mairie du 16ème",
    "uid": "16013",
    "latitude": 48.863936848471994,
    "longitude": 2.2765953093767166
  },
  {
    "address": "Paul-Valery - Victor-Hugo",
    "uid": "16104",
    "latitude": 48.87137406080164,
    "longitude": 2.2887128591537476
  },
  {
    "address": "Jourdan - Stade Charléty",
    "uid": "14014",
    "latitude": 48.81942833336948,
    "longitude": 2.343335375189781
  },
  {
    "address": "Lobau - Hôtel de Ville",
    "uid": "4016",
    "latitude": 48.8562118,
    "longitude": 2.3532536
  },
  {
    "address": "Poissy - Saint-Germain",
    "uid": "5019",
    "latitude": 48.849696494270084,
    "longitude": 2.352968528866768
  },
  {
    "address": "Victor Massé - Jean-Baptiste Pigalle",
    "uid": "9019",
    "latitude": 48.88110437240003,
    "longitude": 2.3366955667734146
  },
  {
    "address": "Sabot - Rennes",
    "uid": "6032",
    "latitude": 48.852575112948415,
    "longitude": 2.331552766263485
  },
  {
    "address": "Jardin Marguerite Long",
    "uid": "17104",
    "latitude": 48.8816985773914,
    "longitude": 2.2836575657129288
  },
  {
    "address": "Jean de la Fontaine - Boulainvilliers",
    "uid": "16025",
    "latitude": 48.85264791461651,
    "longitude": 2.2758664190769196
  },
  {
    "address": "Danielle Casanova - Place Vendôme",
    "uid": "1022",
    "latitude": 48.86826649822326,
    "longitude": 2.3304839059710503
  },
  {
    "address": "Parc de Choisy",
    "uid": "13122",
    "latitude": 48.828156251574946,
    "longitude": 2.358513532903332
  },
  {
    "address": "Gare de Lyon - Van Gogh",
    "uid": "12006",
    "latitude": 48.8438397856433,
    "longitude": 2.370445024035681
  },
  {
    "address": "Jemmapes - Grange aux Belles",
    "uid": "10111",
    "latitude": 48.873462,
    "longitude": 2.364035
  },
  {
    "address": "Lerrede - Tolbiac",
    "uid": "13052",
    "latitude": 48.828876296639876,
    "longitude": 2.374438270926476
  },
  {
    "address": "Maine - Antoine Bourdelle",
    "uid": "15002",
    "latitude": 48.84308531746595,
    "longitude": 2.320263946784738
  },
  {
    "address": "Gare de Clichy-Levallois ",
    "uid": "23005",
    "latitude": 48.8966223,
    "longitude": 2.2985924
  },
  {
    "address": "Ledru-Rollin - Place du Père Chaillet",
    "uid": "11006",
    "latitude": 48.856433706943704,
    "longitude": 2.379020587761426
  },
  {
    "address": "Pontoise - La Tournelle",
    "uid": "5107",
    "latitude": 48.85045800918843,
    "longitude": 2.352454275492188
  },
  {
    "address": "Colisée Champs-Elysées ",
    "uid": "8039",
    "latitude": 48.87047539938743,
    "longitude": 2.3076190799474716
  },
  {
    "address": "Harpe - Saint-Germain ",
    "uid": "5001",
    "latitude": 48.85151881501689,
    "longitude": 2.343670316040516
  },
  {
    "address": "Duroc - Place de Breteuil",
    "uid": "7013",
    "latitude": 48.84746064114221,
    "longitude": 2.3126140236854558
  },
  {
    "address": "Charles Tillon - Cimetière Communal",
    "uid": "33015",
    "latitude": 48.918669589949445,
    "longitude": 2.3931993171572685
  },
  {
    "address": "Saint-François-Xavier",
    "uid": "7014",
    "latitude": 48.85167822904787,
    "longitude": 2.314633840627834
  },
  {
    "address": "Boulangers - Cardinal Lemoine",
    "uid": "5022",
    "latitude": 48.8462904,
    "longitude": 2.3521495
  },
  {
    "address": "Navier - Epinettes",
    "uid": "17007",
    "latitude": 48.89587780769155,
    "longitude": 2.3226776719093327
  },
  {
    "address": "Le Corbusier - Jean Jaurès",
    "uid": "21009",
    "latitude": 48.83781183639671,
    "longitude": 2.2409749031066895
  },
  {
    "address": "Gare de l'Est - Chateau Landon",
    "uid": "10026",
    "latitude": 48.879305,
    "longitude": 2.362424
  },
  {
    "address": "Gabriel Lamé",
    "uid": "12031",
    "latitude": 48.8351212784276,
    "longitude": 2.385534569621086
  },
  {
    "address": "Square d'Anvers",
    "uid": "9005",
    "latitude": 48.882633484310816,
    "longitude": 2.344884406212207
  },
  {
    "address": "Raymond Poincaré - Place Victor Hugo",
    "uid": "16005",
    "latitude": 48.870306124369975,
    "longitude": 2.285074541420228
  },
  {
    "address": "Boussingault - Tolbiac",
    "uid": "13021",
    "latitude": 48.82603516605852,
    "longitude": 2.342176808004326
  },
  {
    "address": "Voltaire - Anatole France",
    "uid": "23010",
    "latitude": 48.891962328964816,
    "longitude": 2.284204990113615
  },
  {
    "address": "Charonne - Philippe-Auguste",
    "uid": "11019",
    "latitude": 48.85560316202584,
    "longitude": 2.390546390619984
  },
  {
    "address": "Molitor - Michel-Ange",
    "uid": "16037",
    "latitude": 48.845182081538034,
    "longitude": 2.26211277356597
  },
  {
    "address": "Saint-Bon - Rivoli",
    "uid": "4018",
    "latitude": 48.857999643314926,
    "longitude": 2.350217280976349
  },
  {
    "address": "Saint-Amand - Labrouste",
    "uid": "15040",
    "latitude": 48.83395161744815,
    "longitude": 2.3087446019053455
  },
  {
    "address": "Saint-Germain - Ciseaux",
    "uid": "6012",
    "latitude": 48.85330527759475,
    "longitude": 2.334465461882703
  },
  {
    "address": "Docteurs Déjérine - Pte de Montreuil",
    "uid": "20009",
    "latitude": 48.85398448133697,
    "longitude": 2.41208288897222
  },
  {
    "address": "Batignolles - Beudant",
    "uid": "8020",
    "latitude": 48.882209874662706,
    "longitude": 2.319571673870087
  },
  {
    "address": "Stalingrad - Désiré Chevalier",
    "uid": "31010",
    "latitude": 48.85681112307105,
    "longitude": 2.444987425339264
  },
  {
    "address": "Aristide Briand - Place de la Résistance",
    "uid": "21302",
    "latitude": 48.8213157727266,
    "longitude": 2.250973843038082
  },
  {
    "address": "Quai aux Fleurs - Pont Saint-Louis",
    "uid": "4003",
    "latitude": 48.85291595620935,
    "longitude": 2.352195717394352
  },
  {
    "address": "Crimée - Ourcq",
    "uid": "19001",
    "latitude": 48.894104140316266,
    "longitude": 2.372946088467279
  },
  {
    "address": "Haxo - Surmelin",
    "uid": "20109",
    "latitude": 48.8694370761222,
    "longitude": 2.40539657537972
  },
  {
    "address": "Molitor - Chardon-Lagache",
    "uid": "16038",
    "latitude": 48.84520880183234,
    "longitude": 2.2656840831041336
  },
  {
    "address": "Croix Nivert - Square Saint-Lambert",
    "uid": "15036",
    "latitude": 48.8430515215905,
    "longitude": 2.295200452208519
  },
  {
    "address": "Gare Montparnasse - Vaugirard",
    "uid": "15003",
    "latitude": 48.8418071798797,
    "longitude": 2.31951002690117
  },
  {
    "address": "Melun - Jean Jaures",
    "uid": "19030",
    "latitude": 48.8837796723475,
    "longitude": 2.376477420330048
  },
  {
    "address": "Louis Ganne - Davout",
    "uid": "20045",
    "latitude": 48.86338720692607,
    "longitude": 2.409725748002529
  },
  {
    "address": "Reuilly - Place Felix Eboue",
    "uid": "12036",
    "latitude": 48.84030274499962,
    "longitude": 2.394630398715453
  },
  {
    "address": "Rochechouart - Martyrs",
    "uid": "18041",
    "latitude": 48.88219377957169,
    "longitude": 2.3405495658516884
  },
  {
    "address": "Place de Finlande",
    "uid": "7026",
    "latitude": 48.86240833596654,
    "longitude": 2.311362437903881
  },
  {
    "address": "Petit Palais",
    "uid": "8001",
    "latitude": 48.8667692,
    "longitude": 2.3157655
  },
  {
    "address": "Square du Carrefour de l'Insurrection",
    "uid": "21703",
    "latitude": 48.820864559770676,
    "longitude": 2.291113436222077
  },
  {
    "address": "Square Desnouettes",
    "uid": "15061",
    "latitude": 48.83476837934194,
    "longitude": 2.284142297394666
  },
  {
    "address": "Rampal - Belleville ",
    "uid": "19102",
    "latitude": 48.87302160155955,
    "longitude": 2.379910983145237
  },
  {
    "address": "Bobillot - Moulin des Prés",
    "uid": "13023",
    "latitude": 48.828657346203094,
    "longitude": 2.353020646316964
  },
  {
    "address": "Grenelle - Dr Finlay",
    "uid": "15028",
    "latitude": 48.851315406715976,
    "longitude": 2.2919502854347233
  },
  {
    "address": "Faustin Hélie - Desbordes-Valmore",
    "uid": "16111",
    "latitude": 48.859403,
    "longitude": 2.276522
  },
  {
    "address": "Liard - Amiral Mouchez",
    "uid": "14013",
    "latitude": 48.8214169,
    "longitude": 2.342167
  },
  {
    "address": "Filles Saint-Thomas - Place de la Bourse",
    "uid": "2009",
    "latitude": 48.86921107122796,
    "longitude": 2.3396865651011467
  },
  {
    "address": "Saint-Ambroise - Parmentier",
    "uid": "11027",
    "latitude": 48.86214784454763,
    "longitude": 2.3772348091006275
  },
  {
    "address": "Auguste Blanqui Corvisart",
    "uid": "13009",
    "latitude": 48.8300989,
    "longitude": 2.3502726
  },
  {
    "address": "Damrémont - Ordener",
    "uid": "18027",
    "latitude": 48.893400392981874,
    "longitude": 2.336266523912805
  },
  {
    "address": "Cimetière de Montmartre",
    "uid": "18003",
    "latitude": 48.886456815828815,
    "longitude": 2.332897827913839
  },
  {
    "address": "Place de l'Hôtel de Ville",
    "uid": "4017",
    "latitude": 48.8573314,
    "longitude": 2.351458
  },
  {
    "address": "Bir Hakeim",
    "uid": "15026",
    "latitude": 48.853844054333514,
    "longitude": 2.289705348222022
  },
  {
    "address": "Place Aristide Briand",
    "uid": "42205",
    "latitude": 48.82194490338474,
    "longitude": 2.412859573960304
  },
  {
    "address": "Geoffroy-Saint-Hilaire - Saint-Marcel",
    "uid": "5105",
    "latitude": 48.83907826424974,
    "longitude": 2.356877340669728
  },
  {
    "address": "Pereire - Place de Wagram",
    "uid": "17022",
    "latitude": 48.887070607581215,
    "longitude": 2.304111124531457
  },
  {
    "address": "Place Saint-André des Arts",
    "uid": "6020",
    "latitude": 48.85288617388103,
    "longitude": 2.3426544293761253
  },
  {
    "address": "La Motte-Picquet - Grenelle",
    "uid": "15023",
    "latitude": 48.84858321339195,
    "longitude": 2.299347817897797
  },
  {
    "address": "Edouard Pailleron Simon Bolivar ",
    "uid": "19039",
    "latitude": 48.87979886049996,
    "longitude": 2.374839261174202
  },
  {
    "address": "Parmentier - République ",
    "uid": "11105",
    "latitude": 48.8644954,
    "longitude": 2.3750124
  },
  {
    "address": "St-Ouen - Lamarck",
    "uid": "18047",
    "latitude": 48.891083040295555,
    "longitude": 2.326735694839291
  },
  {
    "address": "Gabrielle Josserand - Edouard Vaillant ",
    "uid": "35003",
    "latitude": 48.90278883206414,
    "longitude": 2.3932532966136932
  },
  {
    "address": "Charles de Gaulle-Louis-Philippe",
    "uid": "22010",
    "latitude": 48.88162131114395,
    "longitude": 2.2712830090406526
  },
  {
    "address": "Général De Gaulle - Vivien",
    "uid": "41605",
    "latitude": 48.83608867952291,
    "longitude": 2.418907594723682
  },
  {
    "address": "Général Leclerc - Bièvre",
    "uid": "23041",
    "latitude": 48.782561,
    "longitude": 2.31684
  },
  {
    "address": "Square Edmond Pépin",
    "uid": "33104",
    "latitude": 48.88367504404228,
    "longitude": 2.403361093042902
  },
  {
    "address": "Michel-Ange - Parent de Rosan",
    "uid": "16118",
    "latitude": 48.840579923690115,
    "longitude": 2.2584119439125065
  },
  {
    "address": "Daumesnil - Hébrard",
    "uid": "12043",
    "latitude": 48.8429,
    "longitude": 2.38474
  },
  {
    "address": "Sommerard - Saint-Jacques",
    "uid": "5002",
    "latitude": 48.850310432924466,
    "longitude": 2.345015835298588
  },
  {
    "address": "Madagascar - Meuniers",
    "uid": "12035",
    "latitude": 48.83460464474706,
    "longitude": 2.3970650508999825
  },
  {
    "address": "Guillaume Bertrand - République",
    "uid": "11028",
    "latitude": 48.86382517434147,
    "longitude": 2.38090637715497
  },
  {
    "address": "Monceau - Malesherbes ",
    "uid": "8037",
    "latitude": 48.8795079,
    "longitude": 2.3145456
  },
  {
    "address": "Caire - Dussoubs",
    "uid": "2017",
    "latitude": 48.86785342111862,
    "longitude": 2.349364310503006
  },
  {
    "address": "Joseph Liouville - Croix Nivert",
    "uid": "15021",
    "latitude": 48.844835471244416,
    "longitude": 2.297508493065834
  },
  {
    "address": "Vistule - Choisy",
    "uid": "13113",
    "latitude": 48.82365543660611,
    "longitude": 2.3615723848342896
  },
  {
    "address": "Saint-Jacques - Soufflot",
    "uid": "5006",
    "latitude": 48.8465255,
    "longitude": 2.343099
  },
  {
    "address": "Thouin - Cardinal Lemoine",
    "uid": "5016",
    "latitude": 48.84504716661511,
    "longitude": 2.3494647851273465
  },
  {
    "address": "Verdun - Place Henri IV",
    "uid": "21502",
    "latitude": 48.870797375639214,
    "longitude": 2.2277003154158592
  },
  {
    "address": "Alexandre Dumas - Voltaire",
    "uid": "11102",
    "latitude": 48.85288867036487,
    "longitude": 2.389147640809553
  },
  {
    "address": "Belleville -  Pré Saint-Gervais",
    "uid": "19121",
    "latitude": 48.875565910073085,
    "longitude": 2.3930460959672923
  },
  {
    "address": "Legendre - Clichy",
    "uid": "17004",
    "latitude": 48.88896161855884,
    "longitude": 2.322528975094376
  },
  {
    "address": "M A Lagroua Weill-Hallé -  F Dolto",
    "uid": "13055",
    "latitude": 48.828595283857425,
    "longitude": 2.380220606266108
  },
  {
    "address": "Félix Faure - Lourmel ",
    "uid": "15032",
    "latitude": 48.83868901709515,
    "longitude": 2.2818081825971603
  },
  {
    "address": "Panthéon - Valette",
    "uid": "5032",
    "latitude": 48.84702057165222,
    "longitude": 2.3464982619671075
  },
  {
    "address": "Ernest Renan - Parc des Expositions",
    "uid": "15126",
    "latitude": 48.83089757010408,
    "longitude": 2.2853584215044975
  },
  {
    "address": "Assas - Luxembourg",
    "uid": "6008",
    "latitude": 48.84373446877937,
    "longitude": 2.333428381875887
  },
  {
    "address": "Gare Rer",
    "uid": "41202",
    "latitude": 48.8436279,
    "longitude": 2.4630465
  },
  {
    "address": "Saint-Marc - Feydeau",
    "uid": "2102",
    "latitude": 48.870314763994976,
    "longitude": 2.341878566630611
  },
  {
    "address": "Pascal - Claude Bernard",
    "uid": "5026",
    "latitude": 48.839061952159746,
    "longitude": 2.3499782010912895
  },
  {
    "address": "Guy Môquet - Compoint",
    "uid": "17006",
    "latitude": 48.89213559044868,
    "longitude": 2.32321947813034
  },
  {
    "address": "Turbigo - Française",
    "uid": "1007",
    "latitude": 48.86358858111446,
    "longitude": 2.3477794602513313
  },
  {
    "address": "Danielle Casanova - Opéra",
    "uid": "2020",
    "latitude": 48.867643242297284,
    "longitude": 2.3333894088864326
  },
  {
    "address": "Convention - Vaugirard",
    "uid": "15052",
    "latitude": 48.83767682233156,
    "longitude": 2.295586926020252
  },
  {
    "address": "Jean Moulin - Place Victor et Hélène Basch",
    "uid": "14010",
    "latitude": 48.82764910240992,
    "longitude": 2.326196730136872
  },
  {
    "address": "Commandant Charcot - Bretteville",
    "uid": "22007",
    "latitude": 48.87530570765423,
    "longitude": 2.2558460757136345
  },
  {
    "address": "Porte d'Ivry",
    "uid": "13041",
    "latitude": 48.82176874759649,
    "longitude": 2.3688706755638127
  },
  {
    "address": "Transvaal - Charles de Gaulle ",
    "uid": "21001",
    "latitude": 48.848280403628806,
    "longitude": 2.2373420120857195
  },
  {
    "address": "Square Cambronne",
    "uid": "15010",
    "latitude": 48.847566545259745,
    "longitude": 2.3025691509246826
  },
  {
    "address": "Versailles - Claude Terrasse",
    "uid": "16041",
    "latitude": 48.84021933807409,
    "longitude": 2.263834191152959
  },
  {
    "address": "Jussieu - Fossés Saint-Bernard",
    "uid": "5021",
    "latitude": 48.84718551002347,
    "longitude": 2.3534858599305153
  },
  {
    "address": "Mairie du 19ème",
    "uid": "19021",
    "latitude": 48.88255227996044,
    "longitude": 2.3811323940753937
  },
  {
    "address": "Matignon - Faubourg Saint-Honoré",
    "uid": "8032",
    "latitude": 48.87147859116726,
    "longitude": 2.313977926969528
  },
  {
    "address": "Marc Sangnier - Porte de Vanves",
    "uid": "14106",
    "latitude": 48.825218683556734,
    "longitude": 2.3107116669416428
  },
  {
    "address": "Mairie du 10ème",
    "uid": "10009",
    "latitude": 48.87208944990723,
    "longitude": 2.357582598924637
  },
  {
    "address": "Emile Reynaud - Porte de la Villette ",
    "uid": "19010",
    "latitude": 48.90098953630053,
    "longitude": 2.3887022584676743
  },
  {
    "address": "Alibert - Jemmapes",
    "uid": "10013",
    "latitude": 48.8712121,
    "longitude": 2.3661431
  },
  {
    "address": "Gare RER",
    "uid": "43005",
    "latitude": 48.84758123046334,
    "longitude": 2.433240153571221
  },
  {
    "address": "Choisy - Malmaisons",
    "uid": "13038",
    "latitude": 48.82210119146052,
    "longitude": 2.3633962869644165
  },
  {
    "address": "Les Halles Saint-Eustache",
    "uid": "1008",
    "latitude": 48.86362695901683,
    "longitude": 2.342623919248581
  },
  {
    "address": "Edgar Quinet - Raspail ",
    "uid": "14002",
    "latitude": 48.83940266673835,
    "longitude": 2.32971478253603
  },
  {
    "address": "Belidor - Gouvion-Saint-Cyr",
    "uid": "17043",
    "latitude": 48.88022154052015,
    "longitude": 2.2854680567979813
  },
  {
    "address": "Rond-Point Rhin et Danube",
    "uid": "21007",
    "latitude": 48.840477314766034,
    "longitude": 2.2280610725283623
  },
  {
    "address": "Vinaigriers - Magenta",
    "uid": "10012",
    "latitude": 48.8731167,
    "longitude": 2.3593052
  },
  {
    "address": "Bineau - Louise Michel",
    "uid": "23011",
    "latitude": 48.88668137249892,
    "longitude": 2.284023691218167
  },
  {
    "address": "Place des Droits de l'Enfant",
    "uid": "14011",
    "latitude": 48.8274804,
    "longitude": 2.3319217
  },
  {
    "address": "Place Jacques Madaule",
    "uid": "21305",
    "latitude": 48.8239337873086,
    "longitude": 2.2606918215751652
  },
  {
    "address": "Goutte d'or - Barbès",
    "uid": "18007",
    "latitude": 48.88512432362698,
    "longitude": 2.349696233868599
  },
  {
    "address": "Las Cases - Bourgogne",
    "uid": "7011",
    "latitude": 48.85900507746749,
    "longitude": 2.3185520991683006
  },
  {
    "address": "Mairie du 20ème",
    "uid": "20106",
    "latitude": 48.86530232427545,
    "longitude": 2.3990669846534733
  },
  {
    "address": "Courcelles - Pierre Demours",
    "uid": "17047",
    "latitude": 48.883769733273496,
    "longitude": 2.29858466343129
  },
  {
    "address": "Keller - La Roquette",
    "uid": "11003",
    "latitude": 48.8555508,
    "longitude": 2.3752778
  },
  {
    "address": "Gare de Lyon - Place Louis Armand",
    "uid": "12151",
    "latitude": 48.8455150897939,
    "longitude": 2.3726498446286
  },
  {
    "address": "Square Boucicaut",
    "uid": "7003",
    "latitude": 48.851296433665276,
    "longitude": 2.325061820447445
  },
  {
    "address": "Champ de Mars - Cler",
    "uid": "7020",
    "latitude": 48.856691550115315,
    "longitude": 2.306646779179573
  },
  {
    "address": "Bercy - Traversiére",
    "uid": "12102",
    "latitude": 48.84619264257384,
    "longitude": 2.3703800886869435
  },
  {
    "address": "Place Edmond Michelet",
    "uid": "4020",
    "latitude": 48.860134890541865,
    "longitude": 2.350063696503639
  },
  {
    "address": "Benjamin Godard - Victor Hugo",
    "uid": "16107",
    "latitude": 48.865983,
    "longitude": 2.275725
  },
  {
    "address": "Cléry - Montmartre",
    "uid": "2021",
    "latitude": 48.867458,
    "longitude": 2.3445211
  },
  {
    "address": "Louis Lumière - Porte de Bagnolet",
    "uid": "20115",
    "latitude": 48.86431136000005,
    "longitude": 2.4105210229754443
  },
  {
    "address": "Le Brix et Mesmin - Jourdan",
    "uid": "14108",
    "latitude": 48.82234096593411,
    "longitude": 2.327861653302471
  },
  {
    "address": "Censier - Jardin des Plantes",
    "uid": "5028",
    "latitude": 48.84107178161383,
    "longitude": 2.355475388467312
  },
  {
    "address": "Gare Saint-Lazare - Stockholm",
    "uid": "8110",
    "latitude": 48.877127316667654,
    "longitude": 2.322417162358761
  },
  {
    "address": "Volontaires - Vaugirard",
    "uid": "15014",
    "latitude": 48.84132050184004,
    "longitude": 2.30808901725357
  },
  {
    "address": "Suffren - Pérignon",
    "uid": "15009",
    "latitude": 48.84717561492764,
    "longitude": 2.307052147596786
  },
  {
    "address": "Place Alphonse Deville",
    "uid": "6107",
    "latitude": 48.85018826266191,
    "longitude": 2.3275961726903915
  },
  {
    "address": "Saint-Benoît - Jacob",
    "uid": "6002",
    "latitude": 48.85545136013941,
    "longitude": 2.3334722220897675
  },
  {
    "address": "Place Du Général De Gaulle",
    "uid": "31009",
    "latitude": 48.8687591,
    "longitude": 2.4329931
  },
  {
    "address": "Rapp - Place du Général Gouraud",
    "uid": "7024",
    "latitude": 48.85810288510693,
    "longitude": 2.300407290458679
  },
  {
    "address": "Convention - Lourmel",
    "uid": "15062",
    "latitude": 48.84205217310643,
    "longitude": 2.2859314084053044
  },
  {
    "address": "Custine - Ramey",
    "uid": "18015",
    "latitude": 48.888735896947054,
    "longitude": 2.3469526693224907
  },
  {
    "address": "Cassini-Denfert-Rechereau ",
    "uid": "14111",
    "latitude": 48.83752583906732,
    "longitude": 2.336035408079624
  },
  {
    "address": "Didot - Place de Moro-Giafferi",
    "uid": "14031",
    "latitude": 48.8337,
    "longitude": 2.3215
  },
  {
    "address": "Musée d'Orsay",
    "uid": "7007",
    "latitude": 48.85972615790874,
    "longitude": 2.325860448181629
  },
  {
    "address": "Charenton - Wattignies",
    "uid": "12030",
    "latitude": 48.83667579640627,
    "longitude": 2.391795855555629
  },
  {
    "address": "Michelet - Assas",
    "uid": "6018",
    "latitude": 48.84274150283194,
    "longitude": 2.335021197795868
  },
  {
    "address": "Choron - Martyrs",
    "uid": "9016",
    "latitude": 48.877851443502536,
    "longitude": 2.3396701365709305
  },
  {
    "address": "Beaujon - Wagram",
    "uid": "8056",
    "latitude": 48.87535245598514,
    "longitude": 2.2964416444301605
  },
  {
    "address": "Verdun - Adrien Damalix",
    "uid": "44102",
    "latitude": 48.8222334178559,
    "longitude": 2.4215328320860863
  },
  {
    "address": "Belleville - Porte des Lilas",
    "uid": "19037",
    "latitude": 48.87660391892763,
    "longitude": 2.404769691129476
  },
  {
    "address": "Parc Roger Salengro",
    "uid": "21110",
    "latitude": 48.905303858641844,
    "longitude": 2.310559451580048
  },
  {
    "address": "Philippe-Auguste",
    "uid": "11016",
    "latitude": 48.851853488264595,
    "longitude": 2.393294535577297
  },
  {
    "address": "Place de la Montagne du Goulet",
    "uid": "15060",
    "latitude": 48.8443415,
    "longitude": 2.2773844
  },
  {
    "address": "Marcadet - Barbès",
    "uid": "18022",
    "latitude": 48.89045112096476,
    "longitude": 2.349057392968696
  },
  {
    "address": "Belles Feuilles - Place de Mexco",
    "uid": "16010",
    "latitude": 48.86580474647378,
    "longitude": 2.282920628786087
  },
  {
    "address": "Cotentin - Pasteur",
    "uid": "15114",
    "latitude": 48.8389284,
    "longitude": 2.316356
  },
  {
    "address": "René Binet - Porte de Montmartre",
    "uid": "18035",
    "latitude": 48.89905109950848,
    "longitude": 2.3365534096956253
  },
  {
    "address": "Porte de Ménilmontant",
    "uid": "20027",
    "latitude": 48.8691543609174,
    "longitude": 2.40932246879487
  },
  {
    "address": "Repos - Cimetière du Père Lachaise",
    "uid": "20131",
    "latitude": 48.859358523592554,
    "longitude": 2.389613136684456
  },
  {
    "address": "Place des Victoires",
    "uid": "2006",
    "latitude": 48.8660354,
    "longitude": 2.3420333
  },
  {
    "address": "Jean Lolive - Parc Stalingrad",
    "uid": "35009",
    "latitude": 48.8921284,
    "longitude": 2.4091689
  },
  {
    "address": "Exelmans - Michel-Ange",
    "uid": "16040",
    "latitude": 48.84309486651855,
    "longitude": 2.2597770800669053
  },
  {
    "address": "Grande Armée - Brunel",
    "uid": "17038",
    "latitude": 48.876116,
    "longitude": 2.288124
  },
  {
    "address": "D'artois Berry",
    "uid": "8103",
    "latitude": 48.87393521240818,
    "longitude": 2.3064258322119713
  },
  {
    "address": "Lecourbe - Convention",
    "uid": "15053",
    "latitude": 48.83916919806073,
    "longitude": 2.2915928810834885
  },
  {
    "address": "Flandre - Riquet",
    "uid": "19005",
    "latitude": 48.888013281448345,
    "longitude": 2.373759595522922
  },
  {
    "address": "Epée de Bois - Mouffetard",
    "uid": "5015",
    "latitude": 48.841508690388835,
    "longitude": 2.350066713988781
  },
  {
    "address": "Flandrin - Henri Martin",
    "uid": "16018",
    "latitude": 48.8643275,
    "longitude": 2.2724174
  },
  {
    "address": "Jean-Calvin - Tournefort",
    "uid": "5014",
    "latitude": 48.84158856925782,
    "longitude": 2.348489575088024
  },
  {
    "address": "Ivry - Baudricourt",
    "uid": "13037",
    "latitude": 48.824695995415006,
    "longitude": 2.363106073577468
  },
  {
    "address": "Fontenay - Libération",
    "uid": "43008",
    "latitude": 48.847763791081555,
    "longitude": 2.4448127299547195
  },
  {
    "address": "Place de l'Eglise",
    "uid": "35008",
    "latitude": 48.89271966633131,
    "longitude": 2.412529571472175
  },
  {
    "address": "Guersant - Gouvion-Saint-Cyr",
    "uid": "17041",
    "latitude": 48.88287775178599,
    "longitude": 2.287667370814871
  },
  {
    "address": "Cardinal Lavigerie",
    "uid": "12113",
    "latitude": 48.832934193699835,
    "longitude": 2.402636749399156
  },
  {
    "address": "Jardin Alexandra David Neel",
    "uid": "41603",
    "latitude": 48.845623830974496,
    "longitude": 2.4236098676919937
  },
  {
    "address": "Lamartine - Faubourg Montmartre",
    "uid": "9015",
    "latitude": 48.87661398289084,
    "longitude": 2.339765690267086
  },
  {
    "address": "Arago - Glacière ",
    "uid": "13002",
    "latitude": 48.83487785877968,
    "longitude": 2.344621494412422
  },
  {
    "address": "Gare du Nord - Dunkerque ",
    "uid": "10028",
    "latitude": 48.88042350842878,
    "longitude": 2.3527636751532555
  },
  {
    "address": "Place Pigalle",
    "uid": "18042",
    "latitude": 48.8827555,
    "longitude": 2.3361566
  },
  {
    "address": "Point du Jour - Jean Jaurès",
    "uid": "21016",
    "latitude": 48.828753141436465,
    "longitude": 2.245911467014621
  },
  {
    "address": "Abbé Carton - Plantes",
    "uid": "14110",
    "latitude": 48.82770803490614,
    "longitude": 2.3207511752843857
  },
  {
    "address": "Place de Passy",
    "uid": "16043",
    "latitude": 48.85780291711202,
    "longitude": 2.277500546942199
  },
  {
    "address": "Mairie du 4ème",
    "uid": "4015",
    "latitude": 48.85594991049653,
    "longitude": 2.3563823103904724
  },
  {
    "address": "Château - Bineau",
    "uid": "22001",
    "latitude": 48.89051191347665,
    "longitude": 2.270167225273174
  },
  {
    "address": "Raspail - Bac",
    "uid": "7005",
    "latitude": 48.855628721100935,
    "longitude": 2.3256602883338933
  },
  {
    "address": "Rougemont - Poissonnière",
    "uid": "9012",
    "latitude": 48.87138751286167,
    "longitude": 2.3457643037969467
  },
  {
    "address": "Cevennes-Bergers",
    "uid": "15109",
    "latitude": 48.84240765130395,
    "longitude": 2.281037718057633
  },
  {
    "address": "Vivienne - Montmartre",
    "uid": "2108",
    "latitude": 48.8713475973833,
    "longitude": 2.341458834707737
  },
  {
    "address": "Jacquard - Ternaux ",
    "uid": "11031",
    "latitude": 48.8646605,
    "longitude": 2.373162
  },
  {
    "address": "Paul Klee Place - Augusta holmes",
    "uid": "13020",
    "latitude": 48.838946417408025,
    "longitude": 2.3704235663952966
  },
  {
    "address": "Pyrénées - Charles Renouvier",
    "uid": "20020",
    "latitude": 48.86095933229224,
    "longitude": 2.400421852042113
  },
  {
    "address": "Réunion - Avron",
    "uid": "20132",
    "latitude": 48.852566752033766,
    "longitude": 2.403928068708116
  },
  {
    "address": "Marseille - Beaurepaire",
    "uid": "10014",
    "latitude": 48.871624306962985,
    "longitude": 2.3639265082738348
  },
  {
    "address": "Saint-Marcel - Hôpital",
    "uid": "13013",
    "latitude": 48.83950442458303,
    "longitude": 2.360989417022932
  },
  {
    "address": "Guillaume Tell - Parc Henri Barbusse",
    "uid": "35013",
    "latitude": 48.889488420014786,
    "longitude": 2.419848968734546
  },
  {
    "address": "Arthur Auger - Jean Jaurès",
    "uid": "21205",
    "latitude": 48.81372275707675,
    "longitude": 2.307245582342148
  },
  {
    "address": "Raspail - Albert Schweitzer",
    "uid": "42501",
    "latitude": 48.81031545,
    "longitude": 2.34395871
  },
  {
    "address": "Thionville - Ourcq",
    "uid": "19015",
    "latitude": 48.88917613743794,
    "longitude": 2.3833653330802917
  },
  {
    "address": "Parc de Belleville - Julien Lacroix",
    "uid": "20040",
    "latitude": 48.8703459,
    "longitude": 2.3842214
  },
  {
    "address": "Pelleport - Belgrand",
    "uid": "20023",
    "latitude": 48.86500081847653,
    "longitude": 2.403980836377493
  },
  {
    "address": "Gambetta - Michelet",
    "uid": "31018",
    "latitude": 48.8526615,
    "longitude": 2.4298732
  },
  {
    "address": "Buzenval - Avron",
    "uid": "20007",
    "latitude": 48.851617204998654,
    "longitude": 2.401513597307666
  },
  {
    "address": "Diderot - Pierre Bourdan",
    "uid": "12107",
    "latitude": 48.84764862079675,
    "longitude": 2.3902582749724393
  },
  {
    "address": "Place Madeleine Daniélou",
    "uid": "17105",
    "latitude": 48.883498,
    "longitude": 2.282015
  },
  {
    "address": "Arsène Houssaye - Champs-Elysées",
    "uid": "8028",
    "latitude": 48.87340927626732,
    "longitude": 2.2977904602885246
  },
  {
    "address": "Aqueduc - Villette",
    "uid": "10036",
    "latitude": 48.883843,
    "longitude": 2.36711
  },
  {
    "address": "Bois de Vincennes - Gare",
    "uid": "41301",
    "latitude": 48.836022242886884,
    "longitude": 2.4708339950830287
  },
  {
    "address": "Le Brun - Gobelins",
    "uid": "13007",
    "latitude": 48.835092787823875,
    "longitude": 2.353468135133752
  },
  {
    "address": "Square Denise Buisson",
    "uid": "31005",
    "latitude": 48.856284,
    "longitude": 2.426434
  },
  {
    "address": "Place Albert Kahn",
    "uid": "18031",
    "latitude": 48.8957969797804,
    "longitude": 2.345585584134864
  },
  {
    "address": "Jean-Baptiste Pigalle - La Bruyere",
    "uid": "9026",
    "latitude": 48.87939826447722,
    "longitude": 2.333709173337285
  },
  {
    "address": "Butte aux Cailles - Esperance",
    "uid": "13022",
    "latitude": 48.82758774190948,
    "longitude": 2.3492151126265526
  },
  {
    "address": "Passy - Place du Costa Rica",
    "uid": "16023",
    "latitude": 48.8584893,
    "longitude": 2.2839655
  },
  {
    "address": "Mairie d'Issy",
    "uid": "21309",
    "latitude": 48.8236748616062,
    "longitude": 2.272416092455387
  },
  {
    "address": "Paul Doumer - La Tour",
    "uid": "16016",
    "latitude": 48.860116109626574,
    "longitude": 2.280840481911055
  },
  {
    "address": "Champeaux - Gallieni",
    "uid": "31705",
    "latitude": 48.86450831929632,
    "longitude": 2.416152656078339
  },
  {
    "address": "Bayen - Pereire",
    "uid": "17037",
    "latitude": 48.881804409079656,
    "longitude": 2.2921088710427284
  },
  {
    "address": "Pierre Ginier - Clichy",
    "uid": "18045",
    "latitude": 48.8867667,
    "longitude": 2.326228
  },
  {
    "address": "Quai Voltaire",
    "uid": "7006",
    "latitude": 48.85894298617901,
    "longitude": 2.331418204714572
  },
  {
    "address": "Siam - La Pompe",
    "uid": "16017",
    "latitude": 48.861720329027264,
    "longitude": 2.275346568555312
  },
  {
    "address": "Aboukir - Alexandrie",
    "uid": "2016",
    "latitude": 48.868767787961765,
    "longitude": 2.3502444103360176
  },
  {
    "address": "Vaneau - Sevres",
    "uid": "7002",
    "latitude": 48.84856323305935,
    "longitude": 2.3204218259345537
  },
  {
    "address": "Jean-Pierre Timbaud - Vaucouleurs",
    "uid": "11032",
    "latitude": 48.867937,
    "longitude": 2.377885
  },
  {
    "address": "Clapeyron - Moscou",
    "uid": "8011",
    "latitude": 48.88078819514121,
    "longitude": 2.324815057218075
  },
  {
    "address": "Maison de Radio - France",
    "uid": "16029",
    "latitude": 48.85132886434057,
    "longitude": 2.277785539627075
  },
  {
    "address": "Victor Hugo - La Pompe",
    "uid": "16011",
    "latitude": 48.86795222470859,
    "longitude": 2.28146318346262
  },
  {
    "address": "Pereire - Rennequin",
    "uid": "17030",
    "latitude": 48.88364782887555,
    "longitude": 2.2952447086572647
  },
  {
    "address": "Gérando - Rochechouart ",
    "uid": "9004",
    "latitude": 48.88294340844958,
    "longitude": 2.346484623849392
  },
  {
    "address": "Orteaux - Mouraud",
    "uid": "20118",
    "latitude": 48.85558642847872,
    "longitude": 2.408762907764669
  },
  {
    "address": "Pyrénées - Emmery",
    "uid": "20042",
    "latitude": 48.872010281270974,
    "longitude": 2.392039597034455
  },
  {
    "address": "Bellefond-Poissonnière",
    "uid": "9001",
    "latitude": 48.87759897050706,
    "longitude": 2.348463758826256
  },
  {
    "address": "Saint-Jacques Val de Grâce",
    "uid": "5004",
    "latitude": 48.8417514,
    "longitude": 2.3413893
  },
  {
    "address": "Dunois- Clisson",
    "uid": "13043",
    "latitude": 48.832294,
    "longitude": 2.3674523
  },
  {
    "address": "Place des Marseillais",
    "uid": "42207",
    "latitude": 48.826614571984805,
    "longitude": 2.4060115590691566
  },
  {
    "address": "Sarrette - Général Leclerc",
    "uid": "14019",
    "latitude": 48.82503085292528,
    "longitude": 2.326412869398186
  },
  {
    "address": "Alexandre Dumas - Place de la Réunion",
    "uid": "20013",
    "latitude": 48.85556563008463,
    "longitude": 2.400427535176277
  },
  {
    "address": "Ouest - Château ",
    "uid": "14034",
    "latitude": 48.83617286014242,
    "longitude": 2.319392143923804
  },
  {
    "address": "Gare d'Austerlitz - Quai Saint-Bernard",
    "uid": "13104",
    "latitude": 48.84369030863996,
    "longitude": 2.3657163977622986
  },
  {
    "address": "Pelée Alphonse Baudin",
    "uid": "11103",
    "latitude": 48.8600265,
    "longitude": 2.3710541
  },
  {
    "address": "Volontaire - Lecourbe",
    "uid": "15013",
    "latitude": 48.8435137879902,
    "longitude": 2.3065522313117985
  },
  {
    "address": "Honoré d'Estienne d'Ovres - Grilles",
    "uid": "35011",
    "latitude": 48.89056394743836,
    "longitude": 2.4064831843792427
  },
  {
    "address": "Mathurins - Auber",
    "uid": "9032",
    "latitude": 48.872988840743595,
    "longitude": 2.3294984212276404
  },
  {
    "address": "Indre - Prairies",
    "uid": "20021",
    "latitude": 48.86254398506782,
    "longitude": 2.403290458023548
  },
  {
    "address": "Montgallet - Charenton",
    "uid": "12027",
    "latitude": 48.84264777870052,
    "longitude": 2.386164927813738
  },
  {
    "address": "Charles Delescluze",
    "uid": "11101",
    "latitude": 48.85242200848887,
    "longitude": 2.380632497370243
  },
  {
    "address": "Mazagran - Bonne Nouvelle",
    "uid": "10105",
    "latitude": 48.870278,
    "longitude": 2.351281
  },
  {
    "address": "Place de la Gare",
    "uid": "47001",
    "latitude": 48.8025212,
    "longitude": 2.4275517
  },
  {
    "address": "Condorcet-Turgot",
    "uid": "9007",
    "latitude": 48.87985155789093,
    "longitude": 2.345232367515564
  },
  {
    "address": "Gergovie - Alésia",
    "uid": "14027",
    "latitude": 48.83088717455243,
    "longitude": 2.318882101207347
  },
  {
    "address": "Chernoviz - Raynouard",
    "uid": "16112",
    "latitude": 48.8569864146289,
    "longitude": 2.282173218142987
  },
  {
    "address": "Evangile - Aubervilliers",
    "uid": "18108",
    "latitude": 48.89503954187076,
    "longitude": 2.3687969148159027
  },
  {
    "address": "Place du Colonel Fabien ",
    "uid": "10038 ",
    "latitude": 48.8775683208602,
    "longitude": 2.369879186153412
  },
  {
    "address": "Bobillot - Tolbiac",
    "uid": "13024",
    "latitude": 48.8255961335041,
    "longitude": 2.350204177200794
  },
  {
    "address": "David Weill - Parc Montsouris",
    "uid": "14124",
    "latitude": 48.821092374256104,
    "longitude": 2.333725690841675
  },
  {
    "address": "Colonel Monteil - Brune",
    "uid": "14122",
    "latitude": 48.8265029331101,
    "longitude": 2.30927479982717
  },
  {
    "address": "Montreuil - République",
    "uid": "43004",
    "latitude": 48.85036498040637,
    "longitude": 2.4345435574650764
  },
  {
    "address": "Jardins d'Eole",
    "uid": "18039",
    "latitude": 48.8869703,
    "longitude": 2.3668488
  },
  {
    "address": "Cherche-Midi - Hôpital Necker",
    "uid": "15006",
    "latitude": 48.84461063309956,
    "longitude": 2.317914068698883
  },
  {
    "address": "Cambronne - Villa Croix Nivert",
    "uid": "15011",
    "latitude": 48.84584756136587,
    "longitude": 2.3017574474215508
  },
  {
    "address": "Place Carnot",
    "uid": "31012",
    "latitude": 48.8558809,
    "longitude": 2.4417193
  },
  {
    "address": "Cordelières - Arago",
    "uid": "13005",
    "latitude": 48.83548071896613,
    "longitude": 2.348572461702221
  },
  {
    "address": "Hector Malot - Daumesnil",
    "uid": "12008",
    "latitude": 48.84704783357938,
    "longitude": 2.377372272312641
  },
  {
    "address": "Noisy le Sec - Jean-Baptiste Clément",
    "uid": "31708",
    "latitude": 48.874543394980556,
    "longitude": 2.415250763297081
  },
  {
    "address": "George-Sand-Jean de La Fontaine",
    "uid": "16116",
    "latitude": 48.84986918764119,
    "longitude": 2.268257387525699
  },
  {
    "address": "Eugène Oudiné Dessous Des Berges",
    "uid": "13046",
    "latitude": 48.8256423,
    "longitude": 2.3750226
  },
  {
    "address": "Mouton Duvernet - Général Leclerc",
    "uid": "14008",
    "latitude": 48.831632518292565,
    "longitude": 2.3293121159076686
  },
  {
    "address": "Jean Macé - Faidherbe",
    "uid": "11007",
    "latitude": 48.85337548425958,
    "longitude": 2.3828620836138725
  },
  {
    "address": "Belleville - Télégraphe",
    "uid": "19040",
    "latitude": 48.8756045,
    "longitude": 2.3995549
  },
  {
    "address": "Goubet - Darius Milhaud",
    "uid": "19020",
    "latitude": 48.88466206560529,
    "longitude": 2.390209687445294
  },
  {
    "address": "Gare de Pont-Cardinet",
    "uid": "17017",
    "latitude": 48.88713901475306,
    "longitude": 2.314494143994255
  },
  {
    "address": "Place Marcel Cachin",
    "uid": "42014",
    "latitude": 48.81396053918452,
    "longitude": 2.390623725950718
  },
  {
    "address": "République - Henri Barbusse",
    "uid": "33005",
    "latitude": 48.906442322747715,
    "longitude": 2.3892849683761592
  },
  {
    "address": "Pierre Charron - Champs-Elysées",
    "uid": "8040",
    "latitude": 48.870103359864174,
    "longitude": 2.3043501377105713
  },
  {
    "address": "Jean-François Lépine - Stephenson",
    "uid": "18009",
    "latitude": 48.88611466796807,
    "longitude": 2.356855627529275
  },
  {
    "address": "Porte de Saint Cloud - Parc des Princes",
    "uid": "16044",
    "latitude": 48.838739771734616,
    "longitude": 2.252298519015312
  },
  {
    "address": "Vasco de Gama",
    "uid": "15055",
    "latitude": 48.83671783466264,
    "longitude": 2.283664085650799
  },
  {
    "address": "Victoria - Place du Chatelet",
    "uid": "1002",
    "latitude": 48.858018744217624,
    "longitude": 2.3469213030948755
  },
  {
    "address": "Hôtel de Ville",
    "uid": "22604",
    "latitude": 48.790844603480856,
    "longitude": 2.2871455930319455
  },
  {
    "address": "Saint-Séverin - Saint-Michel",
    "uid": "5033",
    "latitude": 48.85273130548806,
    "longitude": 2.3440927639603615
  },
  {
    "address": "Place Jean-Baptiste Clément",
    "uid": "42707",
    "latitude": 48.81251837448261,
    "longitude": 2.3614590615034103
  },
  {
    "address": "Boétie - Ponthieu",
    "uid": "8050",
    "latitude": 48.871417284354834,
    "longitude": 2.307678759098053
  },
  {
    "address": "Cambrai - Benjamin Constant",
    "uid": "19033",
    "latitude": 48.89594736851349,
    "longitude": 2.381133802246315
  },
  {
    "address": "Jouffroy d'Abbans - Wagram",
    "uid": "17026",
    "latitude": 48.881973298351625,
    "longitude": 2.301132157444954
  },
  {
    "address": "Théâtre - Lourmel",
    "uid": "15031",
    "latitude": 48.8482070397636,
    "longitude": 2.2898658737540245
  },
  {
    "address": "Mézières - Rennes ",
    "uid": "6103",
    "latitude": 48.85023459321703,
    "longitude": 2.3303819820284843
  },
  {
    "address": "Marcel Yol - Julien",
    "uid": "21708",
    "latitude": 48.825378,
    "longitude": 2.287004
  },
  {
    "address": "Constantine - Université",
    "uid": "7010",
    "latitude": 48.860680473738334,
    "longitude": 2.314848195584221
  },
  {
    "address": "Arcole - Notre-Dame",
    "uid": "4001",
    "latitude": 48.85388354132773,
    "longitude": 2.3493797332048416
  },
  {
    "address": "Université Paris Dauphine ",
    "uid": "16004",
    "latitude": 48.870707839985386,
    "longitude": 2.2748656198382378
  },
  {
    "address": "Charenton - Diderot ",
    "uid": "12009",
    "latitude": 48.84617146118711,
    "longitude": 2.3793068155646324
  },
  {
    "address": "Gambetta - Père Lachaise",
    "uid": "20024",
    "latitude": 48.864295696165875,
    "longitude": 2.398228667282044
  },
  {
    "address": "Place Marcel Sembat",
    "uid": "21017",
    "latitude": 48.83353185698138,
    "longitude": 2.2441908717155457
  },
  {
    "address": "Toqueville - Cardinet",
    "uid": "17119",
    "latitude": 48.8849609,
    "longitude": 2.3109867000000577
  },
  {
    "address": "Saint-Jacques - Tombe Issoire",
    "uid": "14006",
    "latitude": 48.833206330538864,
    "longitude": 2.3369108140468597
  },
  {
    "address": "Jules Ferry - Faubourg du Temple Paris",
    "uid": "11036",
    "latitude": 48.868397,
    "longitude": 2.3678933
  },
  {
    "address": "Sèvres - Lecourbe",
    "uid": "15008",
    "latitude": 48.8447677,
    "longitude": 2.3110086
  },
  {
    "address": "Euryale Dehaynin - Loire",
    "uid": "19117",
    "latitude": 48.886248917304805,
    "longitude": 2.3774591088294987
  },
  {
    "address": "Pyrénées - Cours de Vincennes",
    "uid": "20003",
    "latitude": 48.84786417875742,
    "longitude": 2.4062398821115494
  },
  {
    "address": "Rossini - Laffitte",
    "uid": "9022",
    "latitude": 48.87334627510507,
    "longitude": 2.337980867701675
  },
  {
    "address": "Parmentier - Hôpital Saint-Louis",
    "uid": "10113",
    "latitude": 48.87137987425661,
    "longitude": 2.369896383666685
  },
  {
    "address": "Richard Lenoir - Place de la Bastille",
    "uid": "11001",
    "latitude": 48.8538327,
    "longitude": 2.3697222
  },
  {
    "address": "Madame de Sanzillon - Mozart",
    "uid": "21104",
    "latitude": 48.90379820774301,
    "longitude": 2.3187401890754704
  },
  {
    "address": "Stalingrad - Rigollots",
    "uid": "41203",
    "latitude": 48.850122737946485,
    "longitude": 2.455398067831993
  },
  {
    "address": "Simon Bolivar - Henri Turot",
    "uid": "19118",
    "latitude": 48.876887,
    "longitude": 2.374454
  },
  {
    "address": "Didot - Mariniers",
    "uid": "14022",
    "latitude": 48.82650543613106,
    "longitude": 2.313136198271666
  },
  {
    "address": "Chartres - La Charbonnière",
    "uid": "18107",
    "latitude": 48.8847601,
    "longitude": 2.3534657
  },
  {
    "address": "Pont de Lodi - Dauphine ",
    "uid": "6014",
    "latitude": 48.855296898603285,
    "longitude": 2.339947307565249
  },
  {
    "address": "Sambre et Meuse - Villette",
    "uid": "10039",
    "latitude": 48.87440566621717,
    "longitude": 2.373807237995119
  },
  {
    "address": "Froment - Bréguet",
    "uid": "11025",
    "latitude": 48.8570414504784,
    "longitude": 2.37289470306807
  },
  {
    "address": "François Ponsard - La Muette ",
    "uid": "16021",
    "latitude": 48.8583666,
    "longitude": 2.2744515
  },
  {
    "address": "Chazelles - Courcelles ",
    "uid": "17025",
    "latitude": 48.879406604954,
    "longitude": 2.3034455627202988
  },
  {
    "address": "Ventadour - Opéra",
    "uid": "1116",
    "latitude": 48.866810653323,
    "longitude": 2.334388448251871
  },
  {
    "address": "Tour d'Auvergne - Rodier",
    "uid": "9008",
    "latitude": 48.879161,
    "longitude": 2.343698
  },
  {
    "address": "Eglise d'Auteuil",
    "uid": "16032",
    "latitude": 48.84749841771691,
    "longitude": 2.26848387135299
  },
  {
    "address": "Daunou - Louis le Grand",
    "uid": "2014",
    "latitude": 48.869113,
    "longitude": 2.332515
  },
  {
    "address": "Stephenson - Doudeauville",
    "uid": "18012",
    "latitude": 48.888537048944826,
    "longitude": 2.3561610281467438
  },
  {
    "address": "Picpus - Louis Braille",
    "uid": "12038",
    "latitude": 48.839988788911874,
    "longitude": 2.400525668552064
  },
  {
    "address": "Dunkerque - Rocroy",
    "uid": "10029",
    "latitude": 48.880726,
    "longitude": 2.351464
  },
  {
    "address": "Assemblée Nationale",
    "uid": "7009",
    "latitude": 48.86141101465194,
    "longitude": 2.320121418520769
  },
  {
    "address": "Petites Ecuries - Faubourg Poissonnière",
    "uid": "10005",
    "latitude": 48.87397217237368,
    "longitude": 2.348388757504776
  },
  {
    "address": "Larmeroux - Baudelaire",
    "uid": "21701",
    "latitude": 48.816939911291605,
    "longitude": 2.280989480484553
  },
  {
    "address": "Gare du Nord - Hôpital Lariboisière",
    "uid": "10107",
    "latitude": 48.881949,
    "longitude": 2.352339
  },
  {
    "address": "Place de la Nation - Picpus",
    "uid": "12015",
    "latitude": 48.84682168154762,
    "longitude": 2.400113046169281
  },
  {
    "address": "Place Charles Digeon",
    "uid": "41602",
    "latitude": 48.8436239,
    "longitude": 2.4183574
  },
  {
    "address": "Botzaris - Parc des Buttes Chaumont",
    "uid": "19024",
    "latitude": 48.877951,
    "longitude": 2.381464
  },
  {
    "address": "Damrémont - Caulaincourt",
    "uid": "18001",
    "latitude": 48.88714509078738,
    "longitude": 2.3325880989432335
  },
  {
    "address": "Lakanal - Commerce",
    "uid": "15034",
    "latitude": 48.84442462786544,
    "longitude": 2.294059172272682
  },
  {
    "address": "Hôtel de Ville",
    "uid": "21005",
    "latitude": 48.83503918235941,
    "longitude": 2.2417017817497253
  },
  {
    "address": "Etienne Jodelle - Saint Ouen",
    "uid": "18046",
    "latitude": 48.88794016394694,
    "longitude": 2.326124420314341
  },
  {
    "address": "Gambetta - Hôpital Tenon",
    "uid": "20026",
    "latitude": 48.867925154026736,
    "longitude": 2.400976623546542
  },
  {
    "address": "Charles Frérot - Albert Guilpin",
    "uid": "42505",
    "latitude": 48.8157121,
    "longitude": 2.3504868
  },
  {
    "address": "Dunkerque - Alsace",
    "uid": "10027",
    "latitude": 48.879355,
    "longitude": 2.358379
  },
  {
    "address": "Cadet - La Fayette",
    "uid": "9101",
    "latitude": 48.87560163267239,
    "longitude": 2.343570403754711
  },
  {
    "address": "Edouard Vaillant - Paul-Adolphe Souriau",
    "uid": "21019",
    "latitude": 48.83487177639868,
    "longitude": 2.2479324450397535
  },
  {
    "address": "Pierre-et-Marie-Curie-Julien-Grimau",
    "uid": "31702",
    "latitude": 48.870658,
    "longitude": 2.424046
  },
  {
    "address": "Reims - Porte d'Asnières",
    "uid": "17116",
    "latitude": 48.891834448210986,
    "longitude": 2.300144659954865
  },
  {
    "address": "Assas - Vaugirard",
    "uid": "6030",
    "latitude": 48.848274111397586,
    "longitude": 2.3292413726449013
  },
  {
    "address": "Estienne d'Orves - Henri Ginoux",
    "uid": "21204",
    "latitude": 48.815388220397274,
    "longitude": 2.320886790760933
  },
  {
    "address": "Regard - Rennes",
    "uid": "6004",
    "latitude": 48.847385404955965,
    "longitude": 2.3268555477261543
  },
  {
    "address": "Mairie ",
    "uid": "42010",
    "latitude": 48.81057120969284,
    "longitude": 2.3842826485633855
  },
  {
    "address": "Richard Lenoir - Voltaire",
    "uid": "11011",
    "latitude": 48.86164273877979,
    "longitude": 2.3727491497993474
  },
  {
    "address": "André Karman  République",
    "uid": "33006",
    "latitude": 48.9102674,
    "longitude": 2.3850127
  },
  {
    "address": "Censier - Santeuil",
    "uid": "5034",
    "latitude": 48.840658921385234,
    "longitude": 2.3537524044513702
  },
  {
    "address": "Carpeaux - Place Jacques Froment",
    "uid": "18018",
    "latitude": 48.8906542,
    "longitude": 2.3309428
  },
  {
    "address": "Brochant - Clichy",
    "uid": "17005",
    "latitude": 48.89032998912038,
    "longitude": 2.319730559929781
  },
  {
    "address": "Mairie du 7ème",
    "uid": "7102",
    "latitude": 48.8577967,
    "longitude": 2.3192664
  },
  {
    "address": "Beaumarchais - Pas de la Mule",
    "uid": "4107",
    "latitude": 48.855580507049055,
    "longitude": 2.368433367343057
  },
  {
    "address": "Blaise-Desgoffe - Vaugirard",
    "uid": "6027",
    "latitude": 48.84612887774711,
    "longitude": 2.324267216026783
  },
  {
    "address": "Victor Hugo - Bineau",
    "uid": "22014",
    "latitude": 48.88780106859287,
    "longitude": 2.278072424368198
  },
  {
    "address": "Liège",
    "uid": "8010",
    "latitude": 48.8795431,
    "longitude": 2.3264628
  },
  {
    "address": "Charonne - Pierre Bayle",
    "uid": "11021",
    "latitude": 48.858445054261914,
    "longitude": 2.39039812238911
  },
  {
    "address": "Place Pasdeloup",
    "uid": "11043",
    "latitude": 48.86265421563133,
    "longitude": 2.3670325062844237
  },
  {
    "address": "Mozart - Jasmin",
    "uid": "16027",
    "latitude": 48.85286124871689,
    "longitude": 2.268483570651513
  },
  {
    "address": "Saint-Romain - Cherche-Midi",
    "uid": "6108",
    "latitude": 48.84708159081946,
    "longitude": 2.321374788880348
  },
  {
    "address": "Mairie du 14ème",
    "uid": "14032",
    "latitude": 48.83254578122993,
    "longitude": 2.325492314994335
  },
  {
    "address": "Pereire - Saussure",
    "uid": "17117",
    "latitude": 48.8882568,
    "longitude": 2.3102882
  },
  {
    "address": "Saint-Didier - Raymond Poincaré",
    "uid": "16009",
    "latitude": 48.86642312020962,
    "longitude": 2.285863497192411
  },
  {
    "address": "Jean Jaurès - Reine",
    "uid": "21004",
    "latitude": 48.840108017817705,
    "longitude": 2.2399072756283056
  },
  {
    "address": "Léon - Doudeauville",
    "uid": "18014",
    "latitude": 48.88832421845225,
    "longitude": 2.353573372638597
  },
  {
    "address": "Porte Dorée",
    "uid": "12032",
    "latitude": 48.8354337,
    "longitude": 2.4077132
  },
  {
    "address": "Bourdon - Pont Morland",
    "uid": "4006",
    "latitude": 48.8478103,
    "longitude": 2.3659356
  },
  {
    "address": "Emeriau - Place de Brazzaville",
    "uid": "15029",
    "latitude": 48.850785261674964,
    "longitude": 2.2871585190296173
  },
  {
    "address": "Thérèse - Opéra",
    "uid": "1016",
    "latitude": 48.86648351450895,
    "longitude": 2.334433421223788
  },
  {
    "address": "Balard - Montagne de la Fage",
    "uid": "15070",
    "latitude": 48.83863387153602,
    "longitude": 2.278274193061826
  },
  {
    "address": "Place de la Porte de Champerret",
    "uid": "17031",
    "latitude": 48.88561741628322,
    "longitude": 2.290781411279206
  },
  {
    "address": "Place des Abbesses",
    "uid": "18004",
    "latitude": 48.884542726149725,
    "longitude": 2.3386867716908455
  },
  {
    "address": "Saint-Fiacre - Poissonière",
    "uid": "2022",
    "latitude": 48.87082891155797,
    "longitude": 2.3459213599562645
  },
  {
    "address": "Pirogues de Bercy - Libourne",
    "uid": "12034",
    "latitude": 48.832308,
    "longitude": 2.38637
  },
  {
    "address": "Temple - Charlot",
    "uid": "11038",
    "latitude": 48.86570934379584,
    "longitude": 2.365273725072355
  },
  {
    "address": "Charenton - Jardiniers",
    "uid": "12112",
    "latitude": 48.833821772922654,
    "longitude": 2.394692664106047
  },
  {
    "address": "Hôpital Robert Debré",
    "uid": "19036",
    "latitude": 48.87956876386225,
    "longitude": 2.4012206902213995
  },
  {
    "address": "Porte Molitor",
    "uid": "16036",
    "latitude": 48.84524763511433,
    "longitude": 2.2569015249609947
  },
  {
    "address": "Pré Saint-Gervais - Lilas",
    "uid": "19113",
    "latitude": 48.87787062698808,
    "longitude": 2.39590734243393
  },
  {
    "address": "Batignolles - Abel Truchet",
    "uid": "17050",
    "latitude": 48.88310390458221,
    "longitude": 2.323834749088381
  },
  {
    "address": "Alfred de Vigny - Place du Général Brocard",
    "uid": "8044",
    "latitude": 48.87836138361425,
    "longitude": 2.305254448220567
  },
  {
    "address": "Félix Faure - Sadi Carnot",
    "uid": "33009",
    "latitude": 48.907885757266726,
    "longitude": 2.378838474064149
  },
  {
    "address": "Charonne - Faubourg Saint-Antoine",
    "uid": "11004",
    "latitude": 48.85219190969573,
    "longitude": 2.3740932717919354
  },
  {
    "address": "Place du Canada",
    "uid": "8029",
    "latitude": 48.865346269362675,
    "longitude": 2.310193787964762
  },
  {
    "address": "Mairie du 9ème",
    "uid": "9013",
    "latitude": 48.87326713758289,
    "longitude": 2.340753448052758
  },
  {
    "address": "Mairie du 2ème",
    "uid": "2008",
    "latitude": 48.8672190355135,
    "longitude": 2.3404625437747044
  },
  {
    "address": "Porte de Vincennes ",
    "uid": "12115",
    "latitude": 48.846454,
    "longitude": 2.415514
  },
  {
    "address": "Crevaux - Bugeaud",
    "uid": "16105",
    "latitude": 48.870809428669546,
    "longitude": 2.281149007691853
  },
  {
    "address": "Verdier - République",
    "uid": "21207",
    "latitude": 48.81546447937147,
    "longitude": 2.317167073488236
  },
  {
    "address": "Olympiades",
    "uid": "13036",
    "latitude": 48.82676466519947,
    "longitude": 2.365086078643799
  },
  {
    "address": "Pau Casals - Neuve Tolbiac",
    "uid": "13054",
    "latitude": 48.8311602,
    "longitude": 2.3774395
  },
  {
    "address": "Marcelin Berthelot",
    "uid": "21006",
    "latitude": 48.8415473058801,
    "longitude": 2.233161628246308
  },
  {
    "address": "Hôtel de Ville - Pont-Marie",
    "uid": "4011",
    "latitude": 48.85389964552738,
    "longitude": 2.356947250664234
  },
  {
    "address": "Manin - Carrières d'Amérique",
    "uid": "19103",
    "latitude": 48.88467214209463,
    "longitude": 2.3919698596000676
  },
  {
    "address": "Pierre Demours - Laugier",
    "uid": "17029",
    "latitude": 48.8813072,
    "longitude": 2.294846
  },
  {
    "address": "Wurtz - Tolbiac",
    "uid": "13048",
    "latitude": 48.82631195621006,
    "longitude": 2.344585955142975
  },
  {
    "address": "Jouffroy d'Abbans - Villiers",
    "uid": "17021",
    "latitude": 48.88421554703737,
    "longitude": 2.3056533560156827
  },
  {
    "address": "Gare de Reuilly - Picpus",
    "uid": "12019",
    "latitude": 48.84267354116789,
    "longitude": 2.3972943797707558
  },
  {
    "address": "Place de la Nation - Charonne",
    "uid": "20001",
    "latitude": 48.848364667776785,
    "longitude": 2.399762385981272
  },
  {
    "address": "Beaumarchais-Saint-Gilles",
    "uid": "3001",
    "latitude": 48.8580378,
    "longitude": 2.3679352
  },
  {
    "address": "Bassano -  Iéna",
    "uid": "16015",
    "latitude": 48.868144510482075,
    "longitude": 2.296149600985935
  },
  {
    "address": "Montmartre - Etienne Marcel",
    "uid": "2005",
    "latitude": 48.865021945810085,
    "longitude": 2.345068440144759
  },
  {
    "address": "Eylau - Trocadéro",
    "uid": "16014",
    "latitude": 48.863354122370744,
    "longitude": 2.2864209115505223
  },
  {
    "address": "Gare SNCF Vanves-Malakoff",
    "uid": "21702",
    "latitude": 48.81884477114737,
    "longitude": 2.291871645129975
  },
  {
    "address": "Méchain - Faubourg Saint-Jacques",
    "uid": "14112",
    "latitude": 48.83586772099485,
    "longitude": 2.33798034198275
  },
  {
    "address": "Saint-Lazare - Place d'Estienne d'Orves",
    "uid": "9029",
    "latitude": 48.8768345,
    "longitude": 2.3329067
  },
  {
    "address": "Place André Malraux",
    "uid": "1015",
    "latitude": 48.86392515872666,
    "longitude": 2.3356226831674576
  },
  {
    "address": "Saint-Maur - République",
    "uid": "11018",
    "latitude": 48.86418586145509,
    "longitude": 2.378317415714264
  },
  {
    "address": "René Boulanger - Lancry",
    "uid": "10001",
    "latitude": 48.8685433,
    "longitude": 2.3600032
  },
  {
    "address": "Gare de Lyon - Roland Barthes",
    "uid": "12106",
    "latitude": 48.84395729494058,
    "longitude": 2.3790882155299187
  },
  {
    "address": "Berri - Haussmann",
    "uid": "8034",
    "latitude": 48.874746047357135,
    "longitude": 2.3082953318953514
  },
  {
    "address": "Montreuil - Voltaire",
    "uid": "11012",
    "latitude": 48.85050701184191,
    "longitude": 2.393075558274471
  },
  {
    "address": "Gobelins - Port-Royal",
    "uid": "5027",
    "latitude": 48.83713590026284,
    "longitude": 2.3515201359987254
  },
  {
    "address": "Place du Maréchal de Lattre de Tassigny",
    "uid": "21704",
    "latitude": 48.8217026998931,
    "longitude": 2.285395624828388
  },
  {
    "address": "Villiers - Batignolles",
    "uid": "17024",
    "latitude": 48.88125253846116,
    "longitude": 2.31653843075037
  },
  {
    "address": "Dareau - Broussais",
    "uid": "14125",
    "latitude": 48.8307758,
    "longitude": 2.3361804
  },
  {
    "address": "Marceau - Président Wilson",
    "uid": "8046",
    "latitude": 48.8652359,
    "longitude": 2.3002513
  },
  {
    "address": "Malesherbes - Place de la Madeleine ",
    "uid": "8004",
    "latitude": 48.87040602848252,
    "longitude": 2.323243509807989
  },
  {
    "address": "Odessa",
    "uid": "14127",
    "latitude": 48.842885590080826,
    "longitude": 2.3244281485676765
  },
  {
    "address": "Buzenval - Vignoles",
    "uid": "20012",
    "latitude": 48.85385817168762,
    "longitude": 2.400004081428051
  },
  {
    "address": "Rochechouart lamartine",
    "uid": "9010",
    "latitude": 48.876719895109,
    "longitude": 2.344393021853951
  },
  {
    "address": "Miromesnil - La Boétie",
    "uid": "8025",
    "latitude": 48.87372241291653,
    "longitude": 2.315848097205162
  },
  {
    "address": "Place de Rungis",
    "uid": "13025",
    "latitude": 48.82245594764965,
    "longitude": 2.347382729482953
  },
  {
    "address": "Jouffroy d'Abbans - Malesherbes",
    "uid": "17020",
    "latitude": 48.8850134,
    "longitude": 2.3070717
  },
  {
    "address": "Fernand Braudel - Vincent Auriol ",
    "uid": "13019",
    "latitude": 48.83637852318147,
    "longitude": 2.3723404482007027
  },
  {
    "address": "Jardin de l'Hospice Debrousse",
    "uid": "20108",
    "latitude": 48.861451061938,
    "longitude": 2.405686005949974
  },
  {
    "address": "Provence - Chaussée d'Antin",
    "uid": "9031",
    "latitude": 48.87428447542072,
    "longitude": 2.332976511564311
  },
  {
    "address": "Belleville - Pyrénées",
    "uid": "19041",
    "latitude": 48.8741908,
    "longitude": 2.3860039
  },
  {
    "address": "Porte Maillot - Pereire",
    "uid": "17042",
    "latitude": 48.877775,
    "longitude": 2.284444
  },
  {
    "address": "Jacquier - Didot",
    "uid": "14025",
    "latitude": 48.82953336292279,
    "longitude": 2.318179272115231
  },
  {
    "address": "Douai - Bruxelles",
    "uid": "9038",
    "latitude": 48.88320784122797,
    "longitude": 2.3308449949028507
  },
  {
    "address": "Place du Palais Royal",
    "uid": "1013",
    "latitude": 48.86243023476397,
    "longitude": 2.338520131151779
  },
  {
    "address": "Nationale - Place Jules Guesde",
    "uid": "21015",
    "latitude": 48.827600424128306,
    "longitude": 2.241835191324587
  },
  {
    "address": "Godot de Mauroy - Madeleine",
    "uid": "9034",
    "latitude": 48.869789,
    "longitude": 2.326347
  },
  {
    "address": "Jeanne Hornet - Girardot",
    "uid": "31704",
    "latitude": 48.8738386258081,
    "longitude": 2.427748180925846
  },
  {
    "address": "Galilée - Vernet",
    "uid": "8003",
    "latitude": 48.87171587869726,
    "longitude": 2.2985444962978363
  },
  {
    "address": "Riquet - Marx Dormoy",
    "uid": "18010",
    "latitude": 48.89021003702031,
    "longitude": 2.3601756244897842
  },
  {
    "address": "Cambronne - Mademoiselle",
    "uid": "15012",
    "latitude": 48.843277330035214,
    "longitude": 2.30230766886348
  },
  {
    "address": "Archives - Rivoli",
    "uid": "4103",
    "latitude": 48.857346707443455,
    "longitude": 2.3539767041802406
  },
  {
    "address": "Uzès - Montmartre",
    "uid": "2010",
    "latitude": 48.870790980240706,
    "longitude": 2.3431010171771054
  },
  {
    "address": "Charles de Gaulle - Plateau de Vanves",
    "uid": "22401",
    "latitude": 48.82233088973263,
    "longitude": 2.298571803412886
  },
  {
    "address": "Gambetta - Tourelles",
    "uid": "20110",
    "latitude": 48.87541242186098,
    "longitude": 2.4059603311549
  },
  {
    "address": "Parc André Citroën",
    "uid": "15059",
    "latitude": 48.84071436444365,
    "longitude": 2.278018571567725
  },
  {
    "address": "Victor Hugo - Galliéni",
    "uid": "21008",
    "latitude": 48.83790187225507,
    "longitude": 2.246063724160194
  },
  {
    "address": "Lourmel - Marie Skobtsov",
    "uid": "15108",
    "latitude": 48.844974,
    "longitude": 2.287283
  },
  {
    "address": "Villersexel - Saint-Germain",
    "uid": "7008",
    "latitude": 48.85890669737897,
    "longitude": 2.323183603584766
  },
  {
    "address": "Marceau - Chaillot",
    "uid": "8048",
    "latitude": 48.86883394989261,
    "longitude": 2.2989967837929726
  },
  {
    "address": "Leibniz - Jean Dollfus",
    "uid": "18034",
    "latitude": 48.896328348515986,
    "longitude": 2.333390423834684
  },
  {
    "address": "Place Adolphe Cherioux - Vaugirard",
    "uid": "15038",
    "latitude": 48.83959442800459,
    "longitude": 2.3010657727718353
  },
  {
    "address": "Lisbonne - Monceau",
    "uid": "8036",
    "latitude": 48.87745567681383,
    "longitude": 2.309694221419506
  },
  {
    "address": "Anatole France - Jean Lolive",
    "uid": "35019",
    "latitude": 48.8949922,
    "longitude": 2.4253764
  },
  {
    "address": "Porte de Clignancourt",
    "uid": "18032",
    "latitude": 48.897488,
    "longitude": 2.343996
  },
  {
    "address": "Paul Bodin - Clichy",
    "uid": "17009",
    "latitude": 48.89239614618928,
    "longitude": 2.317142598330975
  },
  {
    "address": "Arthur Ranc - Hôpital Bichat",
    "uid": "18112",
    "latitude": 48.8978712500682,
    "longitude": 2.333521842956543
  },
  {
    "address": "Quai Marcel Boyer-Victor Hugo",
    "uid": "42011",
    "latitude": 48.82248948133584,
    "longitude": 2.392205223441124
  },
  {
    "address": "Colonel Pierre Avia",
    "uid": "15120",
    "latitude": 48.8296843300915,
    "longitude": 2.275342586850291
  },
  {
    "address": "Alfred Roll - Berthier",
    "uid": "17102",
    "latitude": 48.8879035,
    "longitude": 2.3000183
  },
  {
    "address": "Cours de Vincennes - Davout",
    "uid": "20047",
    "latitude": 48.8473310531154,
    "longitude": 2.4103900081973
  },
  {
    "address": "Marché d'Auteuil",
    "uid": "16033",
    "latitude": 48.84842761367387,
    "longitude": 2.265253821160005
  },
  {
    "address": "Faubourg Montmartre",
    "uid": "9014",
    "latitude": 48.87525454905335,
    "longitude": 2.340853661298752
  },
  {
    "address": "Charenton - Prague",
    "uid": "12101",
    "latitude": 48.848175129981016,
    "longitude": 2.376358449431232
  },
  {
    "address": "Bourdonnais - Tour Eiffel",
    "uid": "7023",
    "latitude": 48.860897797941,
    "longitude": 2.29559959844703
  },
  {
    "address": "Victor Hugo - Jeanne Jugan",
    "uid": "12114",
    "latitude": 48.843949064031825,
    "longitude": 2.415081394834419
  },
  {
    "address": "Jacques Kellner - Saint-Ouen",
    "uid": "17003",
    "latitude": 48.895809485802886,
    "longitude": 2.327897409164145
  },
  {
    "address": "Lacépède - Linné",
    "uid": "5031",
    "latitude": 48.8437035476636,
    "longitude": 2.354651279747486
  },
  {
    "address": "Belleville - Couronnes",
    "uid": "20039",
    "latitude": 48.86883108287741,
    "longitude": 2.380986213684082
  },
  {
    "address": "Chaussée d'Antin - Haussmann",
    "uid": "9039",
    "latitude": 48.87243,
    "longitude": 2.33359
  },
  {
    "address": "Metz - Marne",
    "uid": "19123",
    "latitude": 48.891163,
    "longitude": 2.3865705
  },
  {
    "address": "Place du Moulin de Javel",
    "uid": "15058",
    "latitude": 48.84021670922037,
    "longitude": 2.2716060653328896
  },
  {
    "address": "Récollets-Square-Villemin",
    "uid": "10016",
    "latitude": 48.875034655883944,
    "longitude": 2.359801238597928
  },
  {
    "address": "Alesia - Suisses",
    "uid": "14028",
    "latitude": 48.831600736972284,
    "longitude": 2.31497872620821
  },
  {
    "address": "Herschel - Observatoire ",
    "uid": "6104",
    "latitude": 48.84342376221923,
    "longitude": 2.3376645147800446
  },
  {
    "address": "Porte de Versailles ",
    "uid": "15049",
    "latitude": 48.83215381734912,
    "longitude": 2.2871491312980647
  },
  {
    "address": "Le Goff - Gay-Lussac",
    "uid": "5003",
    "latitude": 48.8461024009786,
    "longitude": 2.3412207886576653
  },
  {
    "address": "Le Vau - Maurice Bertaux",
    "uid": "20103",
    "latitude": 48.86803439043193,
    "longitude": 2.411030612453627
  },
  {
    "address": "Blancs-Manteaux - Archives ",
    "uid": "4014",
    "latitude": 48.859418007030406,
    "longitude": 2.355947121977806
  },
  {
    "address": "Cambon - Rivoli",
    "uid": "1020",
    "latitude": 48.866143390992,
    "longitude": 2.3251800647136633
  },
  {
    "address": "Danton - Serpente",
    "uid": "6016",
    "latitude": 48.85229736258167,
    "longitude": 2.3414132371544834
  },
  {
    "address": "Estrapade - Ulm",
    "uid": "5012",
    "latitude": 48.84512772828039,
    "longitude": 2.3454568170361734
  },
  {
    "address": "Courcelles - Place des Ternes",
    "uid": "8055",
    "latitude": 48.878199928923124,
    "longitude": 2.2992860188665043
  },
  {
    "address": "Franquet - Labrouste",
    "uid": "15041",
    "latitude": 48.83287705011431,
    "longitude": 2.306762784719467
  },
  {
    "address": "Institut de France",
    "uid": "6001",
    "latitude": 48.85761567856468,
    "longitude": 2.335831353037739
  },
  {
    "address": "Beaux-Arts - Bonaparte",
    "uid": "6021",
    "latitude": 48.8566039745635,
    "longitude": 2.334742918610573
  },
  {
    "address": "Boulard - Daguerre",
    "uid": "14036",
    "latitude": 48.83442367825087,
    "longitude": 2.3294589668512344
  },
  {
    "address": "Raffet-Montmorency",
    "uid": "16115",
    "latitude": 48.85258670812,
    "longitude": 2.2629252456151954
  },
  {
    "address": "Porte d'Auteuil",
    "uid": "16034",
    "latitude": 48.8479394142243,
    "longitude": 2.2608396783471107
  },
  {
    "address": "Sorbier - Ménilmontant",
    "uid": "20034",
    "latitude": 48.86854239566069,
    "longitude": 2.3897721245884895
  },
  {
    "address": "Manin - Crimée",
    "uid": "19031",
    "latitude": 48.8831217743719,
    "longitude": 2.3863670602440834
  },
  {
    "address": "Benoît Frachon - République",
    "uid": "20105",
    "latitude": 48.85249392793307,
    "longitude": 2.415516637265682
  },
  {
    "address": "Bois de Vincennes - Polygone",
    "uid": "12122",
    "latitude": 48.83544491617421,
    "longitude": 2.431421081436563
  },
  {
    "address": "Porte de la Muette",
    "uid": "16019",
    "latitude": 48.86302317841182,
    "longitude": 2.2687188170157406
  },
  {
    "address": "Hôpital Pitié-Salpêtrière",
    "uid": "13103",
    "latitude": 48.8377016,
    "longitude": 2.3604946
  },
  {
    "address": "Gare Saint-Lazare - Place de Budapest",
    "uid": "9035",
    "latitude": 48.877753,
    "longitude": 2.327339
  },
  {
    "address": "Square Etienne Jarousse",
    "uid": "21707",
    "latitude": 48.82485182597129,
    "longitude": 2.2925933450460434
  },
  {
    "address": "Dutot - Place Alleray",
    "uid": "15018",
    "latitude": 48.83682609176588,
    "longitude": 2.307170109557766
  },
  {
    "address": "Notre-Dame-des-Champs - Stanislas",
    "uid": "6006",
    "latitude": 48.84479928858768,
    "longitude": 2.329337755887181
  },
  {
    "address": "Sénat - Condé",
    "uid": "6017",
    "latitude": 48.84954227788459,
    "longitude": 2.337939441204071
  },
  {
    "address": "Pyrénées - Ménilmontant",
    "uid": "20035",
    "latitude": 48.8693268535238,
    "longitude": 2.395084574818611
  },
  {
    "address": "Guynemer - Jardin du Luxembourg",
    "uid": "6009",
    "latitude": 48.8466127,
    "longitude": 2.3325478
  },
  {
    "address": "Octave Feuillet - Albéric Magnard",
    "uid": "16110",
    "latitude": 48.86106902949534,
    "longitude": 2.2730283066630363
  },
  {
    "address": "Louis Blanc - Aqueduc ",
    "uid": "10031",
    "latitude": 48.88203018259946,
    "longitude": 2.3635615780949593
  },
  {
    "address": "Raspail - Varenne",
    "uid": "7004",
    "latitude": 48.85314781654488,
    "longitude": 2.3263905197381973
  },
  {
    "address": "Square Saint-Ambroise",
    "uid": "11041",
    "latitude": 48.860934,
    "longitude": 2.375841
  },
  {
    "address": "Place Saint-Ferdinand",
    "uid": "17039",
    "latitude": 48.87815529789739,
    "longitude": 2.288518047864942
  },
  {
    "address": "Fontainebleau - Edmond Michelet",
    "uid": "42703",
    "latitude": 48.80926893618393,
    "longitude": 2.3627317696809764
  },
  {
    "address": "Calmette",
    "uid": "32603",
    "latitude": 48.88115155628259,
    "longitude": 2.4251672253012657
  },
  {
    "address": "Seine - Flandre",
    "uid": "19029",
    "latitude": 48.886785741479684,
    "longitude": 2.3746193200349808
  },
  {
    "address": "Geoffroy-Marie",
    "uid": "9011",
    "latitude": 48.873781940593325,
    "longitude": 2.344513876337893
  },
  {
    "address": "Square Pierre-Lazareff",
    "uid": "2004",
    "latitude": 48.867141720745806,
    "longitude": 2.348393350839615
  },
  {
    "address": "Beffroy-Place-du-General-Gouraud",
    "uid": "22003",
    "latitude": 48.886704983104785,
    "longitude": 2.261263518031739
  },
  {
    "address": "Pierre Grenier - Pont de Billancourt",
    "uid": "21013",
    "latitude": 48.825813993670934,
    "longitude": 2.248637303709984
  },
  {
    "address": "Jules Vallès - Charonne",
    "uid": "11112",
    "latitude": 48.85428437993525,
    "longitude": 2.384767457842827
  },
  {
    "address": "Fille du Calvaire-Turenne",
    "uid": "3003",
    "latitude": 48.86210505412563,
    "longitude": 2.3649600148200993
  },
  {
    "address": "Arsonval Falguière",
    "uid": "15113",
    "latitude": 48.84040040365445,
    "longitude": 2.313117867297474
  },
  {
    "address": "Alger - Rivoli",
    "uid": "1018",
    "latitude": 48.864793,
    "longitude": 2.329431
  },
  {
    "address": "Maine - Jean Zay  ",
    "uid": "14035",
    "latitude": 48.837925263897674,
    "longitude": 2.3225428909063344
  },
  {
    "address": "Reynaldo Hahn - Lagny",
    "uid": "20005",
    "latitude": 48.849266043601794,
    "longitude": 2.412330328844772
  },
  {
    "address": "Porte d'Orléans",
    "uid": "14018",
    "latitude": 48.8227639,
    "longitude": 2.3249313
  },
  {
    "address": "Parmentier - Fontaine au Roi",
    "uid": "11034",
    "latitude": 48.86787670551736,
    "longitude": 2.372705978671471
  },
  {
    "address": "Alexander Fleming - Belvédère",
    "uid": "19124",
    "latitude": 48.881769793858254,
    "longitude": 2.403179367969562
  },
  {
    "address": "Pierre Sarrazin - Saint-Michel",
    "uid": "6031",
    "latitude": 48.85090814639774,
    "longitude": 2.3421307280659676
  },
  {
    "address": "Place de la Reine Astrid",
    "uid": "8045",
    "latitude": 48.86492098253841,
    "longitude": 2.302549182385926
  },
  {
    "address": "Edouard Pailleron - Bouret",
    "uid": "19119",
    "latitude": 48.88058755163207,
    "longitude": 2.3767821863293648
  },
  {
    "address": "Centquatre",
    "uid": "19002",
    "latitude": 48.88933353845543,
    "longitude": 2.3707854375243187
  },
  {
    "address": "Botzaris - Crimée",
    "uid": "19025",
    "latitude": 48.87959698690176,
    "longitude": 2.3887913337477067
  },
  {
    "address": "Marignan - Champs-Elysées",
    "uid": "8013",
    "latitude": 48.869495661232406,
    "longitude": 2.306613813900307
  },
  {
    "address": "Lemercier - Cardinet",
    "uid": "17111",
    "latitude": 48.890097168851746,
    "longitude": 2.317377626895905
  },
  {
    "address": "Bercy - Villot",
    "uid": "12105",
    "latitude": 48.842067398481305,
    "longitude": 2.3763077706098557
  },
  {
    "address": "Pierre Levée - Fontaine Timbaud",
    "uid": "11109",
    "latitude": 48.86632723374305,
    "longitude": 2.3711163550615306
  },
  {
    "address": "Jean-Jacques Rousseau - Deux Ecus",
    "uid": "1012",
    "latitude": 48.86297805972131,
    "longitude": 2.3414635285735126
  },
  {
    "address": "Hameau Fleuri - Marché",
    "uid": "21014",
    "latitude": 48.83145948641608,
    "longitude": 2.2412618994712834
  },
  {
    "address": "Square de la rue Pixérécourt",
    "uid": "20002",
    "latitude": 48.8736824,
    "longitude": 2.3960278
  },
  {
    "address": "Mac-Mahon - Brey",
    "uid": "17046",
    "latitude": 48.877532084130166,
    "longitude": 2.294512536052928
  },
  {
    "address": "Mairie du 11ème",
    "uid": "11024",
    "latitude": 48.858925,
    "longitude": 2.3789759
  },
  {
    "address": "Lacépède - Monge",
    "uid": "5110",
    "latitude": 48.84389286531899,
    "longitude": 2.3519663885235786
  },
  {
    "address": "Félix Faure - Convention",
    "uid": "15069",
    "latitude": 48.841249336041045,
    "longitude": 2.288386600849503
  },
  {
    "address": "Jacques Callot - Mazarine",
    "uid": "6013",
    "latitude": 48.85528017533187,
    "longitude": 2.337574325501919
  },
  {
    "address": "Brancion - Lefebvre",
    "uid": "15043",
    "latitude": 48.82900222785569,
    "longitude": 2.301431038163409
  },
  {
    "address": "Lamennais - Washington ",
    "uid": "8102",
    "latitude": 48.87352990026496,
    "longitude": 2.303444892168045
  },
  {
    "address": "Molière - République",
    "uid": "21209",
    "latitude": 48.81111581390707,
    "longitude": 2.3145738624448158
  },
  {
    "address": "Lagny - Victor Basch",
    "uid": "43002",
    "latitude": 48.84883296464461,
    "longitude": 2.4260084331035614
  },
  {
    "address": "Saint-Vincent - Verdun",
    "uid": "21301",
    "latitude": 48.819211549234495,
    "longitude": 2.254007756710053
  },
  {
    "address": "Place Robert Guillemard",
    "uid": "15110",
    "latitude": 48.836191728066666,
    "longitude": 2.2811267160086417
  },
  {
    "address": "Dunkerque - Trudaine",
    "uid": "9006",
    "latitude": 48.88215166773376,
    "longitude": 2.3463253676891327
  },
  {
    "address": "Pyrénées - Maraîchers",
    "uid": "20016",
    "latitude": 48.85714001395299,
    "longitude": 2.4043509364128113
  },
  {
    "address": "Quai de Valmy",
    "uid": "10037",
    "latitude": 48.88126951579429,
    "longitude": 2.368186041712761
  },
  {
    "address": "Grands Moulins de Pantin",
    "uid": "19044",
    "latitude": 48.89738369720229,
    "longitude": 2.3959985375404362
  },
  {
    "address": "Louis le Grand-Italiens",
    "uid": "2015",
    "latitude": 48.870508937203915,
    "longitude": 2.334054461939329
  },
  {
    "address": "Colmar - Victor Hugo",
    "uid": "25001",
    "latitude": 48.88652251096166,
    "longitude": 2.173960916697979
  },
  {
    "address": "Murat - Porte de Saint-Cloud",
    "uid": "16042",
    "latitude": 48.83750862621299,
    "longitude": 2.2577651962637906
  },
  {
    "address": "Douai - Pierre Fontaine",
    "uid": "9027",
    "latitude": 48.88229101148092,
    "longitude": 2.33324121683836
  },
  {
    "address": "Oberkampf - République",
    "uid": "11030",
    "latitude": 48.8654533,
    "longitude": 2.3760261
  },
  {
    "address": "François 1er - Lincoln",
    "uid": "8105",
    "latitude": 48.86951797934417,
    "longitude": 2.302426218841562
  },
  {
    "address": "Quai Panhard et Levassor",
    "uid": "13050",
    "latitude": 48.82837482809675,
    "longitude": 2.384273745805574
  },
  {
    "address": "Pergolèse - Malakoff",
    "uid": "16002",
    "latitude": 48.875133047166585,
    "longitude": 2.2842496633529663
  },
  {
    "address": "Port de Solférino",
    "uid": "7110",
    "latitude": 48.86121174057001,
    "longitude": 2.3242460936307903
  },
  {
    "address": "Parme - Clichy",
    "uid": "9037",
    "latitude": 48.88126642422664,
    "longitude": 2.328197093450571
  },
  {
    "address": " Jean Bleuzen - Square du 11 Novembre",
    "uid": "21706",
    "latitude": 48.8232579,
    "longitude": 2.2965213
  },
  {
    "address": "Pajol - Place Herbert",
    "uid": "18011",
    "latitude": 48.8927825,
    "longitude": 2.3635598
  },
  {
    "address": "Bagnolet - Place Saint Blaise",
    "uid": "20048",
    "latitude": 48.85973,
    "longitude": 2.40334
  },
  {
    "address": "Clichy - Place Blanche",
    "uid": "18043",
    "latitude": 48.88364716745257,
    "longitude": 2.333388403058052
  },
  {
    "address": "Roquette - Auguste Laurent",
    "uid": "11022",
    "latitude": 48.858001906023354,
    "longitude": 2.381797702650783
  },
  {
    "address": "Schoelcher - Raspail",
    "uid": "14003",
    "latitude": 48.83681733526843,
    "longitude": 2.3312794080411474
  },
  {
    "address": "Favart - Italiens",
    "uid": "2013",
    "latitude": 48.87144635392104,
    "longitude": 2.338290105850787
  },
  {
    "address": "Palais des Sports",
    "uid": "15107",
    "latitude": 48.83346851742578,
    "longitude": 2.2858720645308495
  },
  {
    "address": "Mutualité",
    "uid": "5018",
    "latitude": 48.8482255727241,
    "longitude": 2.350287325680256
  },
  {
    "address": "Institut du Monde Arabe - Saint-Germain",
    "uid": "5020",
    "latitude": 48.84906329950134,
    "longitude": 2.3558086529374123
  },
  {
    "address": "Sainte-Elisabeth-Turbigo",
    "uid": "3005",
    "latitude": 48.86610337431042,
    "longitude": 2.359649240970612
  },
  {
    "address": "Alexandre Dumas ",
    "uid": "11017",
    "latitude": 48.854176,
    "longitude": 2.396093
  },
  {
    "address": "Lorraine - Jean Jaurès ",
    "uid": "19014",
    "latitude": 48.8863865,
    "longitude": 2.3825772
  },
  {
    "address": "Quai de la Loire - Jean Jaurès",
    "uid": "19004",
    "latitude": 48.88339733854529,
    "longitude": 2.371074522980134
  },
  {
    "address": "Hauteville - Bonne Nouvelle",
    "uid": "10003",
    "latitude": 48.87077436024043,
    "longitude": 2.349649636832728
  },
  {
    "address": "Victor Cresson - Telles de la Poterie",
    "uid": "21307",
    "latitude": 48.82269884297945,
    "longitude": 2.2686498274963633
  },
  {
    "address": "Louis Pasteur - Albert Petit",
    "uid": "22206",
    "latitude": 48.796261,
    "longitude": 2.317289
  },
  {
    "address": "Boyer - Ménilmontant",
    "uid": "20121",
    "latitude": 48.86889516267913,
    "longitude": 2.3919824145634565
  },
  {
    "address": "Marché Secrétan",
    "uid": "19022",
    "latitude": 48.88125496379482,
    "longitude": 2.373463287949562
  },
  {
    "address": "Bréa - Raspail",
    "uid": "6007",
    "latitude": 48.842738478257814,
    "longitude": 2.329794221763396
  },
  {
    "address": "Vignes - Boulainvilliers",
    "uid": "16031",
    "latitude": 48.856399483950504,
    "longitude": 2.275240458548069
  },
  {
    "address": "Saint-Fargeau - Mortier",
    "uid": "20117",
    "latitude": 48.87266656122256,
    "longitude": 2.407912909984589
  },
  {
    "address": "Evariste Galois",
    "uid": "20120",
    "latitude": 48.87308753734472,
    "longitude": 2.4133380129933357
  },
  {
    "address": "Jean Jaurès - Jules Jacquemin",
    "uid": "33103",
    "latitude": 48.88433053604843,
    "longitude": 2.4082902455152104
  },
  {
    "address": "Lacuée",
    "uid": "12002",
    "latitude": 48.849268481958404,
    "longitude": 2.370458543300628
  },
  {
    "address": "Louis Blanc - Jemmapes",
    "uid": "10110",
    "latitude": 48.879330755297346,
    "longitude": 2.368448562920093
  },
  {
    "address": "Belfort - Voltaire",
    "uid": "11020",
    "latitude": 48.8563358,
    "longitude": 2.3829901
  },
  {
    "address": "Oratoire - Rivoli",
    "uid": "1025",
    "latitude": 48.86135401009261,
    "longitude": 2.339942380785942
  },
  {
    "address": "Championnet - Vauvenargues",
    "uid": "18028",
    "latitude": 48.89401874744574,
    "longitude": 2.3319872841238976
  },
  {
    "address": "Balzac - Champ Elysées",
    "uid": "8052",
    "latitude": 48.87269963962128,
    "longitude": 2.3001953959465027
  },
  {
    "address": "Suffren - La Motte-Piquet",
    "uid": "15024",
    "latitude": 48.85091460817171,
    "longitude": 2.301293384670141
  },
  {
    "address": "Petit - Manin",
    "uid": "19019",
    "latitude": 48.88650687534334,
    "longitude": 2.3938155401308374
  },
  {
    "address": "Jourdain - Place des Grandes Rigoles",
    "uid": "20112",
    "latitude": 48.87403113727167,
    "longitude": 2.38959476351738
  },
  {
    "address": "Place Balard ",
    "uid": "15056",
    "latitude": 48.836395736424116,
    "longitude": 2.2784192115068436
  },
  {
    "address": "Gabriel Péri  Francisco Ferrer",
    "uid": "33102",
    "latitude": 48.8865458,
    "longitude": 2.4075062
  },
  {
    "address": "Haies - Réunion",
    "uid": "20116",
    "latitude": 48.85386896766491,
    "longitude": 2.402426162238911
  },
  {
    "address": "Saint-Placide - Cherche Midi",
    "uid": "6026",
    "latitude": 48.8490535919234,
    "longitude": 2.325317971408367
  },
  {
    "address": "De Toqueville - Terrasse",
    "uid": "17048",
    "latitude": 48.882428,
    "longitude": 2.314002
  },
  {
    "address": "Saint-Blaise - Mouraud",
    "uid": "20017",
    "latitude": 48.85696177597818,
    "longitude": 2.40869477391243
  },
  {
    "address": "Silly - Galliéni",
    "uid": "21010",
    "latitude": 48.8354885022041,
    "longitude": 2.232560813426972
  },
  {
    "address": "Bertrand Sincholle - Henri Barbusse",
    "uid": "21107",
    "latitude": 48.8992593831325,
    "longitude": 2.304164730012417
  },
  {
    "address": "Richard Lenoir - Jean-Pierre Timbaud",
    "uid": "11039",
    "latitude": 48.86580427668637,
    "longitude": 2.369337074966926
  },
  {
    "address": "Enfants du Paradis - Peupliers",
    "uid": "21021",
    "latitude": 48.83298254367628,
    "longitude": 2.2569766268134117
  },
  {
    "address": "Mairie du 8ème ",
    "uid": "8027",
    "latitude": 48.877932,
    "longitude": 2.318264
  },
  {
    "address": "Gare de Lyon - Chalon",
    "uid": "12007",
    "latitude": 48.84566619420225,
    "longitude": 2.3741452395915985
  },
  {
    "address": "Pyrénées - Dagorno",
    "uid": "20011",
    "latitude": 48.855425770695085,
    "longitude": 2.4051643162965775
  },
  {
    "address": "Commerce - Letellier",
    "uid": "15123",
    "latitude": 48.84797660373612,
    "longitude": 2.2966245196598805
  },
  {
    "address": "Piat - Parc de Belleville",
    "uid": "20113",
    "latitude": 48.87195626635528,
    "longitude": 2.384981390278095
  },
  {
    "address": "Duhamel - André Gide",
    "uid": "15027",
    "latitude": 48.8365662244334,
    "longitude": 2.312707121097412
  },
  {
    "address": "Maine - Liancourt",
    "uid": "14103",
    "latitude": 48.835064363954,
    "longitude": 2.323731449973991
  },
  {
    "address": "Tombe Issoire - René Coty",
    "uid": "14009",
    "latitude": 48.83051287670899,
    "longitude": 2.3343613743782043
  },
  {
    "address": "Flandre - Ourcq",
    "uid": "19007",
    "latitude": 48.89260159189023,
    "longitude": 2.3790915682911873
  },
  {
    "address": "Turbigo - Réaumur",
    "uid": "3011",
    "latitude": 48.86557404753054,
    "longitude": 2.3564198613166814
  },
  {
    "address": "Parmentier - Abel rabaud",
    "uid": "11035",
    "latitude": 48.869014792057236,
    "longitude": 2.371508963406086
  },
  {
    "address": "Chevaleret - Louise Weiss",
    "uid": "13015",
    "latitude": 48.832570718038,
    "longitude": 2.371231986681921
  },
  {
    "address": "Square des Amandiers",
    "uid": "20032",
    "latitude": 48.86615630668046,
    "longitude": 2.3891324177384377
  },
  {
    "address": "Square Simon Bolivar",
    "uid": "19101",
    "latitude": 48.87498199631513,
    "longitude": 2.382461093366146
  },
  {
    "address": "Serrurier ",
    "uid": "19027",
    "latitude": 48.8804934032465,
    "longitude": 2.39806417375803
  },
  {
    "address": "Lauriston - Copernic",
    "uid": "16006",
    "latitude": 48.86926,
    "longitude": 2.289918
  },
  {
    "address": "Parc Suzanne Lenglen",
    "uid": "15125",
    "latitude": 48.833250690537874,
    "longitude": 2.2765403240919113
  },
  {
    "address": "Batignolles - Place de Clichy ",
    "uid": "8012",
    "latitude": 48.88347453575669,
    "longitude": 2.326493114233017
  },
  {
    "address": "Pergolèse - Marbeau",
    "uid": "16102",
    "latitude": 48.87356870686902,
    "longitude": 2.281512522529856
  },
  {
    "address": "Moncey - Blanche",
    "uid": "9028",
    "latitude": 48.88006587474148,
    "longitude": 2.3310810327529907
  },
  {
    "address": "Mairie du 15ème",
    "uid": "15020",
    "latitude": 48.8417762,
    "longitude": 2.2986539
  },
  {
    "address": "Ruisseau - Ordener ",
    "uid": "18026",
    "latitude": 48.89299462568024,
    "longitude": 2.340144887566567
  },
  {
    "address": "Legendre - Dulong",
    "uid": "17013",
    "latitude": 48.885325169402904,
    "longitude": 2.316423766314984
  },
  {
    "address": "Messine - Place Du Pérou ",
    "uid": "8026",
    "latitude": 48.875448033960744,
    "longitude": 2.315508019010038
  },
  {
    "address": "Gare du Nord-Saint-Vincent de Paul",
    "uid": "10033",
    "latitude": 48.88095574164779,
    "longitude": 2.352430514838522
  },
  {
    "address": "Jean Monnet - Iles",
    "uid": "21303",
    "latitude": 48.823393984057304,
    "longitude": 2.2497073194874866
  },
  {
    "address": "Jean Mermoz - Champs-Elysées",
    "uid": "8031",
    "latitude": 48.86968985685174,
    "longitude": 2.310732454061508
  },
  {
    "address": "Dupleix - du Guesclin",
    "uid": "15025",
    "latitude": 48.85147910413369,
    "longitude": 2.296709530055523
  },
  {
    "address": "Saint-Gilles - Turenne",
    "uid": "3002",
    "latitude": 48.85815895677863,
    "longitude": 2.364714992099186
  },
  {
    "address": "Ambroise Rendu - Porte Brunet",
    "uid": "19105",
    "latitude": 48.883667,
    "longitude": 2.395733
  },
  {
    "address": "Gare Montparnasse - René Mouchotte",
    "uid": "14117",
    "latitude": 48.83920384329242,
    "longitude": 2.320883944630623
  },
  {
    "address": "Square Léon",
    "uid": "18008",
    "latitude": 48.88667396788626,
    "longitude": 2.353205569088459
  },
  {
    "address": "Championnet",
    "uid": "18101",
    "latitude": 48.8953643,
    "longitude": 2.3498166
  },
  {
    "address": "Place Denfert-Rochereau",
    "uid": "21002",
    "latitude": 48.84323245691861,
    "longitude": 2.2461049631237984
  },
  {
    "address": "Francoeur - Marcadet",
    "uid": "18020",
    "latitude": 48.89104123261663,
    "longitude": 2.340014585442034
  },
  {
    "address": "Benjamin Delessert - Jean Lolive",
    "uid": "35007",
    "latitude": 48.89390809216606,
    "longitude": 2.417946197846042
  },
  {
    "address": "Bertie Albrecht - Hoche",
    "uid": "8054",
    "latitude": 48.87614039826388,
    "longitude": 2.301303138307601
  },
  {
    "address": "Hôtel de Ville ",
    "uid": "43007",
    "latitude": 48.84837074734389,
    "longitude": 2.4398892000317574
  },
  {
    "address": "Guy Môquet - Etienne Dolet ",
    "uid": "22406",
    "latitude": 48.81527009991819,
    "longitude": 2.298078276995073
  },
  {
    "address": "Place Etienne Pernet",
    "uid": "15035",
    "latitude": 48.84243457133779,
    "longitude": 2.2921819612383847
  },
  {
    "address": "Coquillière - Louvre",
    "uid": "1024",
    "latitude": 48.863542580328534,
    "longitude": 2.3426036943935906
  },
  {
    "address": "Landy - Heurtault",
    "uid": "33012",
    "latitude": 48.915031976133456,
    "longitude": 2.3762789368629456
  },
  {
    "address": "Plantes - Moulin Vert",
    "uid": "14026",
    "latitude": 48.83005755305626,
    "longitude": 2.3232302069664006
  },
  {
    "address": "Saint-Augustin",
    "uid": "8018",
    "latitude": 48.8762009747008,
    "longitude": 2.319786250591278
  },
  {
    "address": "Lagny - Joffre",
    "uid": "41604",
    "latitude": 48.849311,
    "longitude": 2.417909
  },
  {
    "address": "Abbeville - Faubourg Poissonnière",
    "uid": "9002",
    "latitude": 48.879223,
    "longitude": 2.349147
  },
  {
    "address": "Surcouf - Université",
    "uid": "7016",
    "latitude": 48.861273016278126,
    "longitude": 2.309453550914185
  },
  {
    "address": "Mathis - Flandre",
    "uid": "19006",
    "latitude": 48.8906429898976,
    "longitude": 2.3762554675340652
  },
  {
    "address": "Chevreuse - Montparnasse",
    "uid": "6010",
    "latitude": 48.841739161230905,
    "longitude": 2.331552216736739
  },
  {
    "address": "Moulin de la Pointe",
    "uid": "13110",
    "latitude": 48.82092946051445,
    "longitude": 2.3563179373741145
  },
  {
    "address": "Crozatier - Faubourg Saint-Antoine",
    "uid": "12005",
    "latitude": 48.8504400134045,
    "longitude": 2.37890136621104
  },
  {
    "address": "Hautpoul - Jean Jaurès",
    "uid": "19016",
    "latitude": 48.88639367943585,
    "longitude": 2.386541454435452
  },
  {
    "address": "Gare de l'Est - Fidélité",
    "uid": "10017",
    "latitude": 48.874575,
    "longitude": 2.356796
  },
  {
    "address": "Jonquière - Docteur Paul Brousse",
    "uid": "17010",
    "latitude": 48.894799816683985,
    "longitude": 2.318705212173216
  },
  {
    "address": "Romain Rolland",
    "uid": "14116",
    "latitude": 48.820215229662765,
    "longitude": 2.3234144234418346
  },
  {
    "address": "Gare du Nord - Faubourg Saint-Denis",
    "uid": "10151",
    "latitude": 48.879612765503616,
    "longitude": 2.3568402975797653
  },
  {
    "address": "René Coty - Parc Montsouris",
    "uid": "14016",
    "latitude": 48.82479134498032,
    "longitude": 2.336124926805496
  },
  {
    "address": "Saint-Sulpice",
    "uid": "6003",
    "latitude": 48.85165383178419,
    "longitude": 2.3308077827095985
  },
  {
    "address": "Rocher - Laborde",
    "uid": "8017",
    "latitude": 48.87590240712654,
    "longitude": 2.322616316378116
  },
  {
    "address": "Général De Gaulle Allouette",
    "uid": "41601",
    "latitude": 48.839109,
    "longitude": 2.4175664
  },
  {
    "address": "Vieille du Temple - Francs Bourgeois",
    "uid": "4013",
    "latitude": 48.858256413365815,
    "longitude": 2.35821794718504
  },
  {
    "address": "Roquette - Folie Regnault",
    "uid": "11110",
    "latitude": 48.85961917660904,
    "longitude": 2.3864468559622765
  },
  {
    "address": "Place du Président Salvador Allende",
    "uid": "35004",
    "latitude": 48.897307310356055,
    "longitude": 2.40079153681279
  },
  {
    "address": "Centenaire - Sorins",
    "uid": "31013",
    "latitude": 48.858556189169065,
    "longitude": 2.4271198734641075
  },
  {
    "address": "Etienne Dolet - Place Maurice Chevalier",
    "uid": "20033",
    "latitude": 48.86798303359366,
    "longitude": 2.3851405311143026
  },
  {
    "address": "Varenne",
    "uid": "7015",
    "latitude": 48.8572026618033,
    "longitude": 2.315277121961117
  },
  {
    "address": "Général Michel Bizot - Claude Decaen",
    "uid": "12039",
    "latitude": 48.83481262982544,
    "longitude": 2.400927788127704
  },
  {
    "address": "Athènes - Clichy",
    "uid": "9036",
    "latitude": 48.878113787876,
    "longitude": 2.32956021945824
  },
  {
    "address": "Pierre et Marie Curie - Maurice Thorez",
    "uid": "42016",
    "latitude": 48.81580226360801,
    "longitude": 2.376804985105991
  },
  {
    "address": "Bastille - Biscornet",
    "uid": "12001",
    "latitude": 48.851355779578945,
    "longitude": 2.369220033288002
  },
  {
    "address": "Place Jean-Spire-Lemaître",
    "uid": "43009",
    "latitude": 48.8468799295832,
    "longitude": 2.452216297388077
  },
  {
    "address": "Frères Flavien - Porte des Lilas",
    "uid": "20029",
    "latitude": 48.8780992764359,
    "longitude": 2.41104629853209
  },
  {
    "address": "Brochant - Square des Batignolles ",
    "uid": "17016",
    "latitude": 48.88818698581028,
    "longitude": 2.316861143151153
  },
  {
    "address": "Convention - Saint-Charles",
    "uid": "15063",
    "latitude": 48.84315611111599,
    "longitude": 2.2831090539693832
  },
  {
    "address": "Grande Truanderie - Saint-Denis",
    "uid": "1006",
    "latitude": 48.8626302,
    "longitude": 2.3498075
  },
  {
    "address": "Abbé Groult - Convention",
    "uid": "15039",
    "latitude": 48.8359192,
    "longitude": 2.3024257
  },
  {
    "address": "Place Georges Guillaumin",
    "uid": "8053",
    "latitude": 48.874750203956566,
    "longitude": 2.301771435325988
  },
  {
    "address": "Chabrol - d'Hauteville",
    "uid": "10020",
    "latitude": 48.87701838803332,
    "longitude": 2.3513179644942284
  },
  {
    "address": "Londres - Place d'Estienne d'Orves",
    "uid": "9102",
    "latitude": 48.876693805756986,
    "longitude": 2.3305362090468407
  },
  {
    "address": "Granges aux Belles",
    "uid": "10115",
    "latitude": 48.8761373390584,
    "longitude": 2.3680844979417
  },
  {
    "address": "Roquépine - Malesherbes",
    "uid": "8015",
    "latitude": 48.87345073390807,
    "longitude": 2.320404499769211
  },
  {
    "address": "Joseph Sansboeuf - Saint Lazare",
    "uid": "8008",
    "latitude": 48.87531563065266,
    "longitude": 2.3229019716382027
  },
  {
    "address": "Ranelagh - Mozart",
    "uid": "16026",
    "latitude": 48.85545334552678,
    "longitude": 2.2701915353536606
  },
  {
    "address": "Granges aux Belles - Hôpital Saint-Louis",
    "uid": "10114",
    "latitude": 48.874794994841274,
    "longitude": 2.366669436984963
  },
  {
    "address": "Taitbout - Châteaudun",
    "uid": "9025",
    "latitude": 48.876383969379,
    "longitude": 2.335356677933531
  },
  {
    "address": "Port-Royal - Hôpital du Val-de-Grâce",
    "uid": "13001",
    "latitude": 48.8376682728114,
    "longitude": 2.344813834660711
  },
  {
    "address": "André Mazet - Saint-André des Arts",
    "uid": "6015",
    "latitude": 48.85375581057431,
    "longitude": 2.3390958085656166
  },
  {
    "address": "Gare Montparnasse - Edgar Quinet",
    "uid": "14101",
    "latitude": 48.841487851078895,
    "longitude": 2.3233232282800524
  },
  {
    "address": "Quatre Vents - Carrefour de l'Odéon",
    "uid": "6028",
    "latitude": 48.851751374458864,
    "longitude": 2.338161695134053
  },
  {
    "address": "Desaix - Edgar Faure",
    "uid": "15067",
    "latitude": 48.852804604940296,
    "longitude": 2.293077029857222
  },
  {
    "address": "Ordener - Poissonniers",
    "uid": "18023",
    "latitude": 48.891213890844654,
    "longitude": 2.351288666219873
  },
  {
    "address": "Emile Blemont - Poteau",
    "uid": "18029",
    "latitude": 48.8943782,
    "longitude": 2.3415122
  },
  {
    "address": "Londres - Amsterdam",
    "uid": "8101",
    "latitude": 48.878034461503965,
    "longitude": 2.326483540969424
  },
  {
    "address": "Pyrénées-Avron",
    "uid": "20008",
    "latitude": 48.853246898211985,
    "longitude": 2.405871044529538
  },
  {
    "address": "Carducci-Place Hannah Arendt",
    "uid": "19120",
    "latitude": 48.8775354,
    "longitude": 2.3859312
  },
  {
    "address": "Léon Frot - Charonne",
    "uid": "11111",
    "latitude": 48.854963800335234,
    "longitude": 2.387217085372007
  },
  {
    "address": "Galilée - Kléber",
    "uid": "16008",
    "latitude": 48.86741186571367,
    "longitude": 2.290781380114821
  },
  {
    "address": "Gare RER - Malleret-Joinville",
    "uid": "41403",
    "latitude": 48.802936,
    "longitude": 2.42531
  },
  {
    "address": "Gare Saint-Lazare - Isly",
    "uid": "8009",
    "latitude": 48.874777677621296,
    "longitude": 2.326515804784655
  },
  {
    "address": "Saint-Maur - Roquette",
    "uid": "11023",
    "latitude": 48.858688774062955,
    "longitude": 2.383553994571467
  },
  {
    "address": "Octave Gréard - Tour Eiffel",
    "uid": "7025",
    "latitude": 48.856502977695634,
    "longitude": 2.293178754328878
  },
  {
    "address": "Lassus-Delouvain",
    "uid": "19038",
    "latitude": 48.87550448459959,
    "longitude": 2.3890327325518528
  },
  {
    "address": "Roquette - Thiéré",
    "uid": "11002",
    "latitude": 48.85507930579506,
    "longitude": 2.3733686318721463
  },
  {
    "address": "Montgallet - Reuilly",
    "uid": "12013",
    "latitude": 48.844291,
    "longitude": 2.3896481
  },
  {
    "address": "Saint-Honoré - Musée du Louvre",
    "uid": "1023",
    "latitude": 48.86344313284792,
    "longitude": 2.3348781846405102
  },
  {
    "address": "Quai de l'Horloge - Pont Neuf",
    "uid": "1001",
    "latitude": 48.857058739111096,
    "longitude": 2.341798283943937
  },
  {
    "address": "Jean Jaurès - Presles",
    "uid": "35002",
    "latitude": 48.90713441498779,
    "longitude": 2.396005728053061
  },
  {
    "address": "François Debergue - Croix de Chavaux",
    "uid": "31008",
    "latitude": 48.857648156189306,
    "longitude": 2.4374029420503045
  },
  {
    "address": "Gare de Nogent-le-Perreux",
    "uid": "41303",
    "latitude": 48.83859390717572,
    "longitude": 2.4937737733125687
  },
  {
    "address": "Reims - Raymond Pitet",
    "uid": "17028",
    "latitude": 48.8888,
    "longitude": 2.296319
  },
  {
    "address": "Hoche - Tilsitt",
    "uid": "8057",
    "latitude": 48.87483645783703,
    "longitude": 2.297132313251496
  },
  {
    "address": "Square Emile - Chautemps",
    "uid": "3012",
    "latitude": 48.86743372423056,
    "longitude": 2.353635728359223
  },
  {
    "address": "Gare d'Austerlitz - Hôpital",
    "uid": "13127",
    "latitude": 48.841836,
    "longitude": 2.363468
  },
  {
    "address": "Porte Saint-Martin",
    "uid": "3101",
    "latitude": 48.86860172112996,
    "longitude": 2.35570102930069
  },
  {
    "address": "Rond-Point de la Chapelle",
    "uid": "18038",
    "latitude": 48.895126609314104,
    "longitude": 2.359971106052399
  },
  {
    "address": "Quai de la Seine",
    "uid": "19003",
    "latitude": 48.884492238407525,
    "longitude": 2.3703941702842717
  },
  {
    "address": "Paul Barruel - Vaugirard",
    "uid": "15019",
    "latitude": 48.84014918546239,
    "longitude": 2.3044332861900334
  },
  {
    "address": "Place de Rhin et Danube",
    "uid": "19026",
    "latitude": 48.88192765861766,
    "longitude": 2.392586767673493
  },
  {
    "address": "Temple - Jean-Pierre Timbaud",
    "uid": "11040",
    "latitude": 48.864465972010244,
    "longitude": 2.3660597205162053
  },
  {
    "address": "Fédération - Suffren",
    "uid": "15105",
    "latitude": 48.852828153442346,
    "longitude": 2.2978776320815086
  },
  {
    "address": "Gare d'Austerlitz",
    "uid": "13014",
    "latitude": 48.842524,
    "longitude": 2.364044
  },
  {
    "address": "Taitbout - La Fayette",
    "uid": "9024",
    "latitude": 48.87332988919973,
    "longitude": 2.3352625966072087
  },
  {
    "address": "Caumartin - Provence",
    "uid": "9104",
    "latitude": 48.874422773426545,
    "longitude": 2.3284685611724854
  },
  {
    "address": "Cité Riverin - Château d'Eau ",
    "uid": "10008",
    "latitude": 48.870702423554306,
    "longitude": 2.358739863900845
  },
  {
    "address": "Edouard Poisson - Victor Hugo",
    "uid": "33018",
    "latitude": 48.91113914952997,
    "longitude": 2.3792722821235657
  },
  {
    "address": "Hôtel de ville",
    "uid": "41210",
    "latitude": 48.84949749106274,
    "longitude": 2.4750164151191716
  },
  {
    "address": "Quai de l'Aisne - Général Leclerc",
    "uid": "35005",
    "latitude": 48.895713,
    "longitude": 2.400571
  },
  {
    "address": "Charles de Gaulle - Orléans ",
    "uid": "22009",
    "latitude": 48.88092225101522,
    "longitude": 2.271581590175629
  },
  {
    "address": "Charles de Gaulle - Jean Lolive ",
    "uid": "35014",
    "latitude": 48.8932644350857,
    "longitude": 2.4126765131950383
  },
  {
    "address": "Charles de Gaulle - Madrid",
    "uid": "22005",
    "latitude": 48.884432270367995,
    "longitude": 2.2608044743537907
  },
  {
    "address": "Vaugirard - Monsieur le Prince",
    "uid": "6029",
    "latitude": 48.848951441614176,
    "longitude": 2.3411329463124275
  },
  {
    "address": "Morice - Général Leclerc",
    "uid": "21105",
    "latitude": 48.902615193840354,
    "longitude": 2.313320636502019
  },
  {
    "address": "Place des Ternes",
    "uid": "17045",
    "latitude": 48.878438631226516,
    "longitude": 2.297727763652802
  },
  {
    "address": "Greneta - Sebastopol",
    "uid": "2001",
    "latitude": 48.8652054,
    "longitude": 2.3516722
  },
  {
    "address": "Place de la République",
    "uid": "31006",
    "latitude": 48.853759,
    "longitude": 2.4244458
  },
  {
    "address": "Villiers de l'Isle Adam - Pyrénées",
    "uid": "20111",
    "latitude": 48.867362,
    "longitude": 2.396222
  },
  {
    "address": "Gare de Laplace",
    "uid": "41102",
    "latitude": 48.80893199114179,
    "longitude": 2.3349460959434514
  },
  {
    "address": "Bleue - Trévise",
    "uid": "9113",
    "latitude": 48.87578906564573,
    "longitude": 2.347289621829987
  },
  {
    "address": "Village Saint-Paul",
    "uid": "4009",
    "latitude": 48.85193313001975,
    "longitude": 2.361596524715424
  },
  {
    "address": "Richard Wallace - Place du 8 Mai 1945",
    "uid": "28001",
    "latitude": 48.87866794788021,
    "longitude": 2.2420343756675725
  },
  {
    "address": "Chevaleret - Tolbiac",
    "uid": "13053",
    "latitude": 48.82941,
    "longitude": 2.375934
  },
  {
    "address": "Place-Violet",
    "uid": "15033",
    "latitude": 48.84477523502001,
    "longitude": 2.290646731853485
  },
  {
    "address": "Place Centrale ",
    "uid": "21951",
    "latitude": 48.7872696,
    "longitude": 2.2279648
  },
  {
    "address": "Cauchy - Cévennes",
    "uid": "15103",
    "latitude": 48.8433725710224,
    "longitude": 2.275132834911347
  },
  {
    "address": "Véron - Lepic",
    "uid": "18114",
    "latitude": 48.885257,
    "longitude": 2.334617
  },
  {
    "address": "Place Jules Ferry",
    "uid": "21203",
    "latitude": 48.812107928414775,
    "longitude": 2.325837314128876
  },
  {
    "address": "Gare RER",
    "uid": "42504",
    "latitude": 48.814280450380224,
    "longitude": 2.3410347104072575
  },
  {
    "address": "Hôpital Bégin",
    "uid": "43003",
    "latitude": 48.84569200927659,
    "longitude": 2.427535951137543
  },
  {
    "address": "Pont De Charenton",
    "uid": "44103",
    "latitude": 48.8179685,
    "longitude": 2.4199531
  },
  {
    "address": "Brillat-Savarin - Küss",
    "uid": "13109",
    "latitude": 48.822203,
    "longitude": 2.350422
  },
  {
    "address": "Porte de la Plaine - Lefebvre",
    "uid": "15048",
    "latitude": 48.83056827450558,
    "longitude": 2.292114570736885
  },
  {
    "address": "Guy Môquet - Saint-Ouen",
    "uid": "17001",
    "latitude": 48.892777719023975,
    "longitude": 2.3270335793495183
  },
  {
    "address": "Grand Prieuré - Crussol",
    "uid": "11042",
    "latitude": 48.86450126141778,
    "longitude": 2.3695197701454167
  },
  {
    "address": "Berthier - Place Stuart Merril",
    "uid": "17107",
    "latitude": 48.885927922355044,
    "longitude": 2.2933450341224675
  },
  {
    "address": "Westermeyer-Paul Vaillant-Couturier ",
    "uid": "42004",
    "latitude": 48.81900867199005,
    "longitude": 2.3968300223350525
  },
  {
    "address": "Paris - Laitieres",
    "uid": "43001",
    "latitude": 48.84626743927403,
    "longitude": 2.4207928776741032
  },
  {
    "address": "Gare de l'Est - Faubourg-Saint-Martin",
    "uid": "10161",
    "latitude": 48.87564264747718,
    "longitude": 2.3594588041305546
  },
  {
    "address": "Mairie",
    "uid": "32602",
    "latitude": 48.87944408875351,
    "longitude": 2.416152656078339
  },
  {
    "address": "Gaston Roussel - Commune de Paris",
    "uid": "32303",
    "latitude": 48.89432034553156,
    "longitude": 2.432552274419542
  },
  {
    "address": "Anatole France - Louise Michel",
    "uid": "23009",
    "latitude": 48.88867835897005,
    "longitude": 2.288065105676651
  },
  {
    "address": "Jean Colly - Tolbiac",
    "uid": "13045",
    "latitude": 48.827817291597846,
    "longitude": 2.3705490678548813
  },
  {
    "address": "Place du 14 Juillet",
    "uid": "22408",
    "latitude": 48.81933517370336,
    "longitude": 2.300447523593903
  },
  {
    "address": "Jean Jaurès - Ouvrières Pivéreuses",
    "uid": "35001",
    "latitude": 48.91042343752563,
    "longitude": 2.399576604366303
  },
  {
    "address": "Place Charles Vallin",
    "uid": "15122",
    "latitude": 48.83552381204077,
    "longitude": 2.302598655223847
  },
  {
    "address": "Hôtel de Ville - Pont-d'Arcole",
    "uid": "4111",
    "latitude": 48.85587645183987,
    "longitude": 2.350892163813114
  },
  {
    "address": "Emeriau - Beaugrenelle",
    "uid": "15030",
    "latitude": 48.84791249714942,
    "longitude": 2.2843059897422795
  },
  {
    "address": "Pont-Neuf - Quai du Louvre",
    "uid": "1122",
    "latitude": 48.8582958982873,
    "longitude": 2.3423714563250546
  },
  {
    "address": "Stade Georges Carpentier",
    "uid": "13117",
    "latitude": 48.82048243838136,
    "longitude": 2.366832196712494
  },
  {
    "address": "Place du Lieutenant Henri Karcher",
    "uid": "1026",
    "latitude": 48.863490871724466,
    "longitude": 2.3400671035051346
  },
  {
    "address": "Théophile Gautier - François Millet",
    "uid": "16136",
    "latitude": 48.85034821320644,
    "longitude": 2.273029983043671
  },
  {
    "address": "Candale-Mehul",
    "uid": "35012",
    "latitude": 48.88941346432578,
    "longitude": 2.4141535542600945
  },
  {
    "address": "Place de la Porte de Passy",
    "uid": "16020",
    "latitude": 48.857341095470794,
    "longitude": 2.2644165538827115
  },
  {
    "address": "Félix Ziem - Armand Gauthier",
    "uid": "18111",
    "latitude": 48.88949630486821,
    "longitude": 2.333393658804396
  },
  {
    "address": "Senard - Longchamp",
    "uid": "22103",
    "latitude": 48.857315824799,
    "longitude": 2.221091687679291
  },
  {
    "address": "Berges de Seine - Musée d'Orsay",
    "uid": "7113",
    "latitude": 48.8627885936562,
    "longitude": 2.3064593598246574
  },
  {
    "address": "Charles de Gaulle - Gravier",
    "uid": "22008",
    "latitude": 48.88315175918637,
    "longitude": 2.2648814320564274
  },
  {
    "address": "Charonne - Alexandre Dumas",
    "uid": "20014",
    "latitude": 48.85607741505607,
    "longitude": 2.394869327545166
  },
  {
    "address": "Porte de Saint-Ouen - Bessières",
    "uid": "17044",
    "latitude": 48.89792240854517,
    "longitude": 2.32851451022192
  },
  {
    "address": "Place Fernand Forest",
    "uid": "15072",
    "latitude": 48.849269143835926,
    "longitude": 2.2818631678819656
  },
  {
    "address": "Proudhon - Président Wilson",
    "uid": "32001",
    "latitude": 48.906469,
    "longitude": 2.358442
  },
  {
    "address": "Guichet - Jean Jaurès",
    "uid": "21109",
    "latitude": 48.9051308,
    "longitude": 2.3023773
  },
  {
    "address": "Wilson - Métallurgie",
    "uid": "32003",
    "latitude": 48.909423010092326,
    "longitude": 2.3584435880184174
  },
  {
    "address": "Boutroux - Porte de Vitry",
    "uid": "13047",
    "latitude": 48.822632,
    "longitude": 2.377738
  },
  {
    "address": "Paris - Emile Zola",
    "uid": "31003",
    "latitude": 48.85491133239991,
    "longitude": 2.41877242975671
  },
  {
    "address": "Ivry- Simone Weil",
    "uid": "13039",
    "latitude": 48.823283932030755,
    "longitude": 2.3659095168113713
  },
  {
    "address": "Wilson - Landy",
    "uid": "32004",
    "latitude": 48.91513597458588,
    "longitude": 2.357760965824127
  },
  {
    "address": "Place Mongolfier",
    "uid": "44101",
    "latitude": 48.815160030060376,
    "longitude": 2.4592879414558415
  },
  {
    "address": "Professeur René Leriche-général Leclerc ",
    "uid": "21111",
    "latitude": 48.90782845483013,
    "longitude": 2.307887971401215
  },
  {
    "address": "Pont de Levallois-bécon ",
    "uid": "23002",
    "latitude": 48.8984179,
    "longitude": 2.2791311
  },
  {
    "address": "Mairie",
    "uid": "22013",
    "latitude": 48.885065,
    "longitude": 2.267797
  },
  {
    "address": "Ledru-Rollin - Charonne",
    "uid": "11114",
    "latitude": 48.85345855607243,
    "longitude": 2.3772463581947094
  },
  {
    "address": "Metz - Faubourg Saint-Denis",
    "uid": "10004",
    "latitude": 48.870893,
    "longitude": 2.353521
  },
  {
    "address": "Lucien Sampaix - Récollets",
    "uid": "10015",
    "latitude": 48.8743988,
    "longitude": 2.3620612
  },
  {
    "address": "La Jarry - Place Diderot",
    "uid": "43010",
    "latitude": 48.849797539941804,
    "longitude": 2.4516704678535466
  },
  {
    "address": "Bara-Henri Farman",
    "uid": "21308",
    "latitude": 48.8330538,
    "longitude": 2.2677903
  },
  {
    "address": "Invalides-Duroc",
    "uid": "7001",
    "latitude": 48.8477924732952,
    "longitude": 2.3161920905113225
  },
  {
    "address": "Saint-Denis - Rivoli",
    "uid": "1003",
    "latitude": 48.85905934084166,
    "longitude": 2.3476943001151085
  },
  {
    "address": "Italie - Jardin Joan Miro",
    "uid": "13032",
    "latitude": 48.81990914045756,
    "longitude": 2.3592948541045193
  },
  {
    "address": "Bosquet - Saint-Dominique",
    "uid": "7021",
    "latitude": 48.85865170952645,
    "longitude": 2.303716580234576
  },
  {
    "address": "Place Jacques Bonsergent",
    "uid": "10010",
    "latitude": 48.870948218595146,
    "longitude": 2.3612049221992497
  },
  {
    "address": "Caulaincourt - Place Constantin Pecqueur",
    "uid": "18017",
    "latitude": 48.8896779050281,
    "longitude": 2.33817871442611
  },
  {
    "address": "Perle - Vieille du Temple",
    "uid": "3008",
    "latitude": 48.8601346,
    "longitude": 2.3611206
  },
  {
    "address": "Gaston Tessier - Rosa Parks RER",
    "uid": "19108",
    "latitude": 48.8963765,
    "longitude": 2.3743692
  },
  {
    "address": "Place de Lévis",
    "uid": "17015",
    "latitude": 48.883546851533254,
    "longitude": 2.3131933808326726
  },
  {
    "address": "Clignancourt - Ordener",
    "uid": "18024",
    "latitude": 48.891457726153774,
    "longitude": 2.348636478060487
  },
  {
    "address": "Daumesnil - Picpus",
    "uid": "12010",
    "latitude": 48.8374688,
    "longitude": 2.401765
  },
  {
    "address": "Gare Routière",
    "uid": "22101",
    "latitude": 48.843126,
    "longitude": 2.222189
  },
  {
    "address": "Mont Valérien - Michel Salles",
    "uid": "22102",
    "latitude": 48.8561915,
    "longitude": 2.2152966
  },
  {
    "address": "Bois de Vincennes",
    "uid": "12041",
    "latitude": 48.8338064,
    "longitude": 2.4132221
  },
  {
    "address": "Ville-RER",
    "uid": "92005",
    "latitude": 48.8950295,
    "longitude": 2.1957932
  },
  {
    "address": "André Maurois - Joseph et Marie Hackin",
    "uid": "16003",
    "latitude": 48.8778983,
    "longitude": 2.2789591
  },
  {
    "address": "Président Wilson-Baudin",
    "uid": "23003",
    "latitude": 48.89826887103676,
    "longitude": 2.2849336266517644
  },
  {
    "address": "Square du Bataclan",
    "uid": "11044",
    "latitude": 48.8633762,
    "longitude": 2.3711327
  },
  {
    "address": "Vitruve - Davout",
    "uid": "20122",
    "latitude": 48.86053161154489,
    "longitude": 2.4091592812626543
  },
  {
    "address": "Commandant l'Herminier - Gallieni",
    "uid": "20004",
    "latitude": 48.84704055260307,
    "longitude": 2.416045367717743
  },
  {
    "address": "Square du Théâtre du Garde-Chasse",
    "uid": "32604",
    "latitude": 48.881215,
    "longitude": 2.420209
  },
  {
    "address": "Marcelle - Thalie",
    "uid": "35020",
    "latitude": 48.8835462,
    "longitude": 2.4137883
  },
  {
    "address": "Porte d'Asnières",
    "uid": "17023",
    "latitude": 48.89035817610495,
    "longitude": 2.3036286234855656
  },
  {
    "address": "Square Alban Satragne",
    "uid": "10018",
    "latitude": 48.87534054839558,
    "longitude": 2.35588476061821
  },
  {
    "address": "Saint Georges - d'Aumale",
    "uid": "9021",
    "latitude": 48.87778639599673,
    "longitude": 2.337429821491241
  },
  {
    "address": "Parvis Corentin Celton",
    "uid": "21311",
    "latitude": 48.82747,
    "longitude": 2.278549
  },
  {
    "address": "Hôpital - Campo-Formio",
    "uid": "13011",
    "latitude": 48.835405,
    "longitude": 2.358218
  },
  {
    "address": "Jardin d'Acclimatation",
    "uid": "16121",
    "latitude": 48.878772242510465,
    "longitude": 2.270772233605385
  },
  {
    "address": "Olivier de Serres-Leriche",
    "uid": "15111",
    "latitude": 48.83460221715206,
    "longitude": 2.2957482933998112
  },
  {
    "address": " Place Léon Gambetta",
    "uid": "42023",
    "latitude": 48.81461,
    "longitude": 2.40291
  },
  {
    "address": "Quai de l'Oise - Aisne",
    "uid": "19128",
    "latitude": 48.8906231,
    "longitude": 2.3833701
  },
  {
    "address": "Victor Hugo - Magasins Généraux",
    "uid": "33001",
    "latitude": 48.903281401126755,
    "longitude": 2.368072383105755
  },
  {
    "address": "Gravelle - Route du Bac",
    "uid": "12126",
    "latitude": 48.824407,
    "longitude": 2.418455
  },
  {
    "address": "Lepic - Armée d'Orient",
    "uid": "18113",
    "latitude": 48.887573,
    "longitude": 2.334327
  },
  {
    "address": "Chanzy - Sud-Est",
    "uid": "92003",
    "latitude": 48.8889899,
    "longitude": 2.1956302
  },
  {
    "address": "Ecole Militaire- Place Joffre",
    "uid": "7111",
    "latitude": 48.852060203582795,
    "longitude": 2.301799356937409
  },
  {
    "address": "Gare de l'Est - Place du 11 Novembre 1918",
    "uid": "10165",
    "latitude": 48.876327,
    "longitude": 2.3585464
  },
  {
    "address": "Place du Maréchal Juin - Péreire",
    "uid": "17027",
    "latitude": 48.885263,
    "longitude": 2.298286
  },
  {
    "address": "Gare Arcueil-Cachan",
    "uid": "42301",
    "latitude": 48.799824,
    "longitude": 2.328171
  },
  {
    "address": "Française - Etienne Marcel",
    "uid": "1102",
    "latitude": 48.864053304653766,
    "longitude": 2.347587011754513
  },
  {
    "address": "Rivoli - Musée du Louvre",
    "uid": "1014",
    "latitude": 48.86364636851945,
    "longitude": 2.33409583568573
  },
  {
    "address": "Charles Gide - Fort de Bicêtre ",
    "uid": "42701",
    "latitude": 48.8071937956922,
    "longitude": 2.353984415531159
  },
  {
    "address": "Sebastopol - Rambuteau",
    "uid": "4104",
    "latitude": 48.861818092669864,
    "longitude": 2.3501381278038025
  },
  {
    "address": "Jean Moulin - Paul Doumer",
    "uid": "31017",
    "latitude": 48.862315,
    "longitude": 2.4550668
  },
  {
    "address": "Boulets - Voltaire",
    "uid": "11009",
    "latitude": 48.8522025,
    "longitude": 2.3890928
  },
  {
    "address": "Edouard Vaillant - Galliéni",
    "uid": "31707",
    "latitude": 48.858395,
    "longitude": 2.41486
  },
  {
    "address": "Président Wilson - Jean Jaures",
    "uid": "23007",
    "latitude": 48.89034054052266,
    "longitude": 2.2920683026313786
  },
  {
    "address": "Faidherbe - Dahomey",
    "uid": "11107",
    "latitude": 48.85118127150571,
    "longitude": 2.3837140202522282
  },
  {
    "address": "Place du Maquis du Vercors",
    "uid": "20019",
    "latitude": 48.877321579493575,
    "longitude": 2.4083782732486725
  },
  {
    "address": "Hôtel de Ville ",
    "uid": "23008",
    "latitude": 48.89314451997415,
    "longitude": 2.2888818383216862
  },
  {
    "address": "Saint-Antoine Sévigné ",
    "uid": "4010",
    "latitude": 48.8550222,
    "longitude": 2.3612322
  },
  {
    "address": "Paul Doumer - Stalingrad",
    "uid": "41101",
    "latitude": 48.8063743587237,
    "longitude": 2.3369979858398438
  },
  {
    "address": "Ney - Porte de Montmartre",
    "uid": "18135",
    "latitude": 48.897918,
    "longitude": 2.3364872
  },
  {
    "address": "Bachaumont - Montmartre",
    "uid": "2101",
    "latitude": 48.86617395079137,
    "longitude": 2.3449775576591496
  },
  {
    "address": "Henri Barbusse - Stalingrad",
    "uid": "22409",
    "latitude": 48.814580710238836,
    "longitude": 2.2875300049781804
  },
  {
    "address": "Place Charles de Gaulle ",
    "uid": "24003",
    "latitude": 48.8964947,
    "longitude": 2.2524995
  },
  {
    "address": "Square Louise Michel",
    "uid": "18006",
    "latitude": 48.88468735450907,
    "longitude": 2.3441386967897415
  },
  {
    "address": "Place Jacques et Thérèse Tréfouël",
    "uid": "15005",
    "latitude": 48.84265,
    "longitude": 2.312727
  },
  {
    "address": "Place Jeanne d'Arc",
    "uid": "13044",
    "latitude": 48.829575,
    "longitude": 2.369126
  },
  {
    "address": "Place du Docteur Yersin",
    "uid": "13116",
    "latitude": 48.82010494991975,
    "longitude": 2.3725583776831627
  },
  {
    "address": "Place du Général de Gaulle",
    "uid": "42025",
    "latitude": 48.812708472356654,
    "longitude": 2.3707965016365056
  },
  {
    "address": "Paris - Désiré Préaux",
    "uid": "31007",
    "latitude": 48.857364,
    "longitude": 2.4321581
  },
  {
    "address": "Delphine Seyrig - Général Leclerc",
    "uid": "19043",
    "latitude": 48.893923963057865,
    "longitude": 2.39790290594101
  },
  {
    "address": "Mairie de Quartier du Mont Valérien",
    "uid": "92001",
    "latitude": 48.8799933,
    "longitude": 2.2072437
  },
  {
    "address": "Traktir - Foch",
    "uid": "16103",
    "latitude": 48.87275533461363,
    "longitude": 2.2915497802487077
  },
  {
    "address": "Portugais - Kléber",
    "uid": "16001",
    "latitude": 48.8712148,
    "longitude": 2.2937349
  },
  {
    "address": "Emile Zola - Fondary",
    "uid": "15022",
    "latitude": 48.8470534,
    "longitude": 2.2954377
  },
  {
    "address": "Bessières - Porte de Clichy",
    "uid": "17108",
    "latitude": 48.89611762451474,
    "longitude": 2.31790903955698
  },
  {
    "address": "Saint-Mandé - Fabre-deglantine",
    "uid": "12018",
    "latitude": 48.8453579,
    "longitude": 2.3965022
  },
  {
    "address": "Hautes Bornes - Stalingrad",
    "uid": "42017",
    "latitude": 48.8058918,
    "longitude": 2.3769731
  },
  {
    "address": "Delizy - Louis Nadot",
    "uid": "35006",
    "latitude": 48.896216372284016,
    "longitude": 2.4094739556312565
  },
  {
    "address": "Porte des Lilas - Faidherbe",
    "uid": "32601",
    "latitude": 48.8786326,
    "longitude": 2.4118966
  },
  {
    "address": "Porte d'Italie",
    "uid": "13033",
    "latitude": 48.817161987718954,
    "longitude": 2.360236309468746
  },
  {
    "address": "Croulebarde - Corvisart",
    "uid": "13101",
    "latitude": 48.830981659316855,
    "longitude": 2.3481646925210953
  },
  {
    "address": "Primo Levi - Frigos",
    "uid": "13016",
    "latitude": 48.83094844846017,
    "longitude": 2.37965351592796
  },
  {
    "address": "Place Jean Delay",
    "uid": "13026",
    "latitude": 48.823316,
    "longitude": 2.354336
  },
  {
    "address": "Carnot - Général Lanrezac",
    "uid": "17033",
    "latitude": 48.8754438707356,
    "longitude": 2.2938242935035684
  },
  {
    "address": "Choisy - Tolbiac ",
    "uid": "13035",
    "latitude": 48.82591155587023,
    "longitude": 2.3602393269538884
  },
  {
    "address": "Pierret - Madrid",
    "uid": "22006",
    "latitude": 48.882976576876565,
    "longitude": 2.260125767248034
  },
  {
    "address": "Henri Ste-Claire Déville - Paul Héroult",
    "uid": "25003",
    "latitude": 48.892663,
    "longitude": 2.172411
  },
  {
    "address": "La Motte-Piquet La Tour Maubourg",
    "uid": "7017",
    "latitude": 48.8572316,
    "longitude": 2.3094136
  },
  {
    "address": "Bourse du Travail",
    "uid": "10011",
    "latitude": 48.868976638825686,
    "longitude": 2.362343855202198
  },
  {
    "address": "Mairie du 3ème",
    "uid": "3006",
    "latitude": 48.86446178139141,
    "longitude": 2.361762821674347
  },
  {
    "address": "Beaubourg - Michel-le-Comte",
    "uid": "3010",
    "latitude": 48.862534941805855,
    "longitude": 2.354310303926468
  },
  {
    "address": "Place de la République-Temple",
    "uid": "3004",
    "latitude": 48.8673174,
    "longitude": 2.3638276
  },
  {
    "address": "Centre Georges Pompidou",
    "uid": "4021",
    "latitude": 48.861419,
    "longitude": 2.352581
  },
  {
    "address": "René Coty - Place Denfert-Rochereau",
    "uid": "14005",
    "latitude": 48.83316726722363,
    "longitude": 2.3327158391475677
  },
  {
    "address": "Gare Montparnasse - Arrivée",
    "uid": "15001",
    "latitude": 48.843827994314886,
    "longitude": 2.3225811123847966
  },
  {
    "address": "Place d'Italie - Soeur Rosalie",
    "uid": "13008",
    "latitude": 48.83190852039389,
    "longitude": 2.3546943448098023
  },
  {
    "address": "Ferrus - Saint-Jacques",
    "uid": "14007",
    "latitude": 48.8313878,
    "longitude": 2.3408563
  },
  {
    "address": "Bobillot - Paulin Mery",
    "uid": "13106",
    "latitude": 48.829899841747384,
    "longitude": 2.3543356766748507
  },
  {
    "address": "Place d'Italie - Vincent Auriol",
    "uid": "13010",
    "latitude": 48.83148187446674,
    "longitude": 2.356755029231863
  },
  {
    "address": "Gare de l'Est - Faubourg Saint-Denis",
    "uid": "10022",
    "latitude": 48.876731953103835,
    "longitude": 2.356172762811184
  },
  {
    "address": "La Fouilleuse-Place Nelson Mandela ",
    "uid": "25006",
    "latitude": 48.862090937689715,
    "longitude": 2.196576297283173
  },
  {
    "address": "Général Martial Valin - Pont du Garigliano",
    "uid": "15068",
    "latitude": 48.8359907,
    "longitude": 2.2489514
  },
  {
    "address": "Ney - Porte de Clignancourt ",
    "uid": "18134",
    "latitude": 48.8978867,
    "longitude": 2.3458001
  },
  {
    "address": "Pyramide - Ecole du Breuil ",
    "uid": "12128",
    "latitude": 48.82237977153955,
    "longitude": 2.4586871266365056
  },
  {
    "address": "Tanger - Place du Maroc",
    "uid": "19109",
    "latitude": 48.8862524447104,
    "longitude": 2.3689296841621403
  },
  {
    "address": "Quai des Célestins-Henri IV",
    "uid": "4005",
    "latitude": 48.8512971,
    "longitude": 2.3624535
  },
  {
    "address": "Toudouze - Clauzel",
    "uid": "9020",
    "latitude": 48.87929591733507,
    "longitude": 2.3373600840568547
  },
  {
    "address": "Charonne-Bureau",
    "uid": "11104",
    "latitude": 48.85590755596891,
    "longitude": 2.3925706744194035
  },
  {
    "address": "Tiron-Rivoli ",
    "uid": "4012",
    "latitude": 48.85569225323279,
    "longitude": 2.357905805110932
  },
  {
    "address": "Jean Oestreicher-Porte de Champerret",
    "uid": "17032",
    "latitude": 48.88655580065981,
    "longitude": 2.288640439510346
  },
  {
    "address": "Pitié-Salpêtrière",
    "uid": "13102",
    "latitude": 48.837208062341794,
    "longitude": 2.3646864295005803
  },
  {
    "address": "Pyrénées-Plaine",
    "uid": "20006",
    "latitude": 48.85026702458012,
    "longitude": 2.4062821269035344
  },
  {
    "address": "Porte Pouchet-Bessières",
    "uid": "17008",
    "latitude": 48.8978765,
    "longitude": 2.3230632
  },
  {
    "address": "Georges Bernanos - Port Royal",
    "uid": "5029",
    "latitude": 48.84014521347377,
    "longitude": 2.3373574018478394
  },
  {
    "address": "Montreuil-Gonnet",
    "uid": "11115",
    "latitude": 48.85050056492107,
    "longitude": 2.389085628786445
  },
  {
    "address": "Cité de la Musique ",
    "uid": "19018",
    "latitude": 48.8886668,
    "longitude": 2.3924568
  },
  {
    "address": "Parc Eli Lotar",
    "uid": "33014",
    "latitude": 48.91840652730917,
    "longitude": 2.3727350682020187
  },
  {
    "address": "Jules Guesde-Alsace",
    "uid": "23006",
    "latitude": 48.89066679778823,
    "longitude": 2.294951677322388
  },
  {
    "address": "Richard Lenoir",
    "uid": "11113",
    "latitude": 48.8667175,
    "longitude": 2.3692541
  },
  {
    "address": "Hôpital Européen Georges Pompidou ",
    "uid": "15104",
    "latitude": 48.83769531916335,
    "longitude": 2.275374233722687
  },
  {
    "address": "Château - République",
    "uid": "31706",
    "latitude": 48.8629238005955,
    "longitude": 2.415503561496735
  },
  {
    "address": "Raspail - Chomel",
    "uid": "7101",
    "latitude": 48.8520306,
    "longitude": 2.3264214
  },
  {
    "address": "Place de Barcelone - Mirabeau",
    "uid": "16030",
    "latitude": 48.84762302736417,
    "longitude": 2.2735664248466496
  },
  {
    "address": "Halle Freyssinet-Parvis Alan Turing",
    "uid": "13051",
    "latitude": 48.83260007328244,
    "longitude": 2.3712202906608586
  },
  {
    "address": "Pereire-Ternes",
    "uid": "17040",
    "latitude": 48.87973954830735,
    "longitude": 2.288012132048607
  },
  {
    "address": "Gare de Clamart",
    "uid": "21403",
    "latitude": 48.813939,
    "longitude": 2.272292
  },
  {
    "address": "Chemin Vert - Saint-Maur",
    "uid": "11026",
    "latitude": 48.86112461407359,
    "longitude": 2.381299026310444
  },
  {
    "address": "Joseph Dijon - Ornano",
    "uid": "18030",
    "latitude": 48.89374453351521,
    "longitude": 2.3476678133010864
  },
  {
    "address": "Bagnolet - Orteaux",
    "uid": "20015",
    "latitude": 48.857089057369045,
    "longitude": 2.3982978612184525
  },
  {
    "address": "Place Jean Ferrat",
    "uid": "42015",
    "latitude": 48.81850865003077,
    "longitude": 2.3744389414787297
  },
  {
    "address": "Suffren-Champ de Mars",
    "uid": "15071",
    "latitude": 48.8547194,
    "longitude": 2.2952168
  },
  {
    "address": "George-V-Christophe Colomb",
    "uid": "8549",
    "latitude": 48.8704294,
    "longitude": 2.3011638
  },
  {
    "address": "Batignolles - Rome",
    "uid": "8051",
    "latitude": 48.881823370566806,
    "longitude": 2.3202589899301524
  },
  {
    "address": "Place de la République - Voltaire",
    "uid": "11046",
    "latitude": 48.86683096894219,
    "longitude": 2.3654810339212418
  },
  {
    "address": "Faubourg Du Temple - Republique",
    "uid": "11037",
    "latitude": 48.867872484748744,
    "longitude": 2.3648982158072323
  },
  {
    "address": "Paul Vaillant Couturier-Chaptal",
    "uid": "23001",
    "latitude": 48.89347957618252,
    "longitude": 2.277525365352631
  },
  {
    "address": "Vincent Auriol-Louise Weiss",
    "uid": "13049",
    "latitude": 48.83446803717201,
    "longitude": 2.3699381947517395
  },
  {
    "address": "Saint-Germain-des-Prés",
    "uid": "6024",
    "latitude": 48.85418510815204,
    "longitude": 2.330305874347687
  },
  {
    "address": "Choiseul-Quatre Septembre ",
    "uid": "2012",
    "latitude": 48.8699355331869,
    "longitude": 2.3359975218772893
  },
  {
    "address": "Gare",
    "uid": "24002",
    "latitude": 48.8993867,
    "longitude": 2.2506506
  },
  {
    "address": "Bordier - Félix Faure",
    "uid": "33003",
    "latitude": 48.90399919889405,
    "longitude": 2.382941544055939
  },
  {
    "address": "Edouard Tremblay-Julian Grimau",
    "uid": "44013",
    "latitude": 48.78138382511671,
    "longitude": 2.3723508417606354
  },
  {
    "address": "Vigée-Lebrun -Anselme Payen",
    "uid": "15112",
    "latitude": 48.838683941628375,
    "longitude": 2.3110422492027287
  },
  {
    "address": "Dugommier",
    "uid": "12029",
    "latitude": 48.8388110488154,
    "longitude": 2.38965779542923
  },
  {
    "address": "Fabre d'Eglantine - Place de la Nation",
    "uid": "12157",
    "latitude": 48.8471116,
    "longitude": 2.3956204
  },
  {
    "address": "Jean Marin Naudin - Stalingrad",
    "uid": "22202",
    "latitude": 48.805419,
    "longitude": 2.3205839
  },
  {
    "address": "Boissière-Etienne-Dolet",
    "uid": "31102",
    "latitude": 48.876111,
    "longitude": 2.4677312
  },
  {
    "address": "Anatole France-President Roosevelt",
    "uid": "33017",
    "latitude": 48.9155156,
    "longitude": 2.3814103
  },
  {
    "address": "Marne-Germain Dardan",
    "uid": "21212",
    "latitude": 48.81154270401601,
    "longitude": 2.303070724010468
  },
  {
    "address": "Conservatoire de Musique",
    "uid": "46003",
    "latitude": 48.77107596196057,
    "longitude": 2.4032488837838173
  },
  {
    "address": "Hoche-Jean-Lolive",
    "uid": "35010",
    "latitude": 48.89110591907107,
    "longitude": 2.4028596282005314
  },
  {
    "address": "Villette-Belleville",
    "uid": "19032",
    "latitude": 48.87289171442418,
    "longitude": 2.376256473362446
  },
  {
    "address": "Victoire-Chaussée d'Antin",
    "uid": "9116",
    "latitude": 48.875065791004396,
    "longitude": 2.3316738009452824
  },
  {
    "address": "Gare du Nord-Denain",
    "uid": "10153",
    "latitude": 48.879595787608096,
    "longitude": 2.3544725775718693
  },
  {
    "address": "Morillons - Dantzig",
    "uid": "15047",
    "latitude": 48.83310149953933,
    "longitude": 2.299380004405976
  },
  {
    "address": "Cimetiere",
    "uid": "41209",
    "latitude": 48.8471951,
    "longitude": 2.4788566
  },
  {
    "address": "Lucie Aubrac-Fort",
    "uid": "21312",
    "latitude": 48.8180841,
    "longitude": 2.2710145
  },
  {
    "address": "Concorde-Stalingrad",
    "uid": "44006",
    "latitude": 48.7985844,
    "longitude": 2.3821302
  }
]
